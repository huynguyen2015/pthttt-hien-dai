namespace project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update20170216 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TBL_MERCHANT", "CloseDate", c => c.DateTime());
            AlterColumn("dbo.TBL_MERCHANT", "LastActiveDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TBL_MERCHANT", "LastActiveDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.TBL_MERCHANT", "CloseDate", c => c.DateTime(nullable: false));
        }
    }
}
