namespace Click.Data.Entities
{
    using System;


    public class PaymentTransactionModel
    {
        public int PaymentTransactionId { get; set; }

        public int ProfilePaymentId { get; set; }

        public string TransactionData { get; set; }

        public string TransactionId { get; set; }

        public decimal Amount { get; set; }

        public short CurrencyId { get; set; }

        public DateTimeOffset NextBillingCycle { get; set; }

        public string InvoicePdfUUID { get; set; }

        public bool IsRecurring { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

    }
}