/**
 * @ngdoc overview
 * @name Member controller
 * @description 
 *
 */
(function() {
    function memberSearchController($scope, $uibModal, commonService, $location, httpService, orderType, apiRoutes, $timeout, $q, $log) {

        // Declare variable        
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;

        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 6, pageSize: 20, currentItem: '0-20', totalItems: 120 };
            $scope.filter = {filter: false , keyword: "" };
        }

        resetFilter();

        function filterMembers() {
            httpService.sendGet(apiRoutes.memberFilter, $scope.filter, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.copiedData = $scope.datas;
                    $scope.paging = commonService.preparePagination(response);
                }
            });
        }

       filterMembers();

        $scope.removedAccount = {};

        $scope.sortBy = function (type) {
            $scope.filter.filter = true;
            $scope.filter.name2Sort = type;
            $scope.filter.orderType = $scope.filter.orderType == orderType.ASC ? orderType.DESC : orderType.ASC;

            filterMembers();
        }

        $scope.roles = Role;
        $scope.selectedRole = $scope.roles[0];

        $scope.status = Status;
        $scope.selectedStatus = $scope.status[0];

        $scope.setRole = function (r) {
            $scope.selectedRole = r;
        }

        $scope.setStatus = function (s) {
            $scope.selectedStatus = s;
        }

        $scope.searchPageChange = function (flag, pageNumber) {
            debugger
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1; 
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterMembers();
        };

        function seachMember() {
            httpService.sendGet(apiRoutes.memberSearch, $scope.filter, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.paging = commonService.preparePagination(response);
                }
            });
        }
        $scope.searchClick = function () {
            $scope.filter.Email = $scope.filter.Email ? $scope.filter.Email : "";
            $scope.filter.pageNumber = 1;
            $scope.filter.RoleId = $scope.selectedRole.id;
            $scope.filter.StatusId = $scope.selectedStatus.id;
            seachMember();
        };

        $scope.deleteAccount = function() {

            httpService.sendPost(apiRoutes.memberDelete, $scope.removedAccount).then(function (response) {
                if (response && response.Success === true) {
                    filterMembers();
                }
            });
            return true;
        }

        $scope.removeItem = function(item) {
            $scope.removedAccount = item;
        }

        $scope.showPopup = function(account) {
            commonService.openModal("accountmodal.html", "AccountModificationController", account, filterMembers);
        }

        $scope.parseNameFromId = function (id, array_obj) {
            return parseNameFromId(id, array_obj);
        }
        
        $scope.refresh = function () {
            resetFilter();
            filterMembers();
        }
    }

    angular.module('app').controller('MemberSearchController', ['$scope', '$uibModal', 'commonService', '$location', 'HttpService', 'ORDER_TYPE', 'API_ROUTES', memberSearchController]);
})();
