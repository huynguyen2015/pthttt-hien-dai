﻿using project.Service.Services.Interfaces;
using project.Web.Api.Commons.Helper;

namespace project.Web.Api.Controllers.v1.Interfaces
{
    public interface IAccountControllerDependencyBlock : IControllerDependencyBlock
    {
        IAccountService MemberService { get; }        
        IParseRequestParams ParseRequestParams { get; }
    }
}
