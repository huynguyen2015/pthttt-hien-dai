﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace project.Common
{
    public class ImageHelper:IImage
    {
        public Size ProportionSize(Size sizeOriginal, Size sizeChanged)
        {
            int nWidth;
            int nHeight;
            if (sizeChanged.Width < 1 || sizeChanged.Height < 1 || sizeOriginal.Width < 1 || sizeOriginal.Height < 1)
                return Size.Empty;
            var sMaxRatio = (double)sizeChanged.Width / sizeChanged.Height;
            var sRealRatio = (double)sizeOriginal.Width / sizeOriginal.Height;
            if (sMaxRatio < sRealRatio)
            {
                nWidth = Math.Min(sizeChanged.Width, sizeOriginal.Width);
                nHeight = (int)Math.Round(nWidth / sRealRatio);
            }
            else
            {
                nHeight = Math.Min(sizeChanged.Height, sizeOriginal.Height);
                nWidth = (int)Math.Round(nHeight * sRealRatio);
            }
            return new Size(nWidth, nHeight);
        }

        public Bitmap GetThumbnailBitmap(Stream stream
            , Size sizeChanged)
        {
            using (var img = Image.FromStream(stream, true, false))
            {
                var sizeProportion = ProportionSize(sizeChanged, img.Size);
                return (Bitmap)img.GetThumbnailImage(
                    sizeProportion.Width,
                    sizeProportion.Height,
                    null,
                    IntPtr.Zero);
            }
        }

        public Stream GetThumbnailImageStream(Stream stream
            , Size sizeChanged)
        {
            var bitMap = GetThumbnailBitmap(stream, sizeChanged);
            var newStream = new MemoryStream();
            bitMap.Save(newStream, ImageFormat.Png);
            return newStream;
        }
        public Stream ResizeStream(Stream stream
            , Size sizeChanged)
        {
            var img = Image.FromStream(stream);
            sizeChanged.Height = (int)(sizeChanged.Width * (img.Height / (float)img.Width));
            var bitMap = GetThumbnailBitmap(stream, sizeChanged);
            var newStream = new MemoryStream();
            bitMap.Save(newStream, ImageFormat.Jpeg);
            return newStream;
        }
    }
}