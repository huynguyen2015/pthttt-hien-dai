USE [master]
GO
/****** Object:  Database [CardProcessing]    Script Date: 12/28/2016 10:53:05 PM ******/
CREATE DATABASE [CardProcessing] ON  PRIMARY 
GO
USE [CardProcessing]
GO
/****** Object:  Table [dbo].[INF_MESSAGES]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INF_MESSAGES](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](1024) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[FromAccountId] [bigint] NOT NULL,
	[ToAccountId] [bigint] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[IsDeleted] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
 CONSTRAINT [PK_INF_MESSAGES] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_MERCHANT_DATASUMARY]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPT_MERCHANT_DATASUMARY](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DateType] [int] NOT NULL,
	[TotalAmount] [money] NOT NULL,
	[TotalCount] [bigint] NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[DateReport] [datetime] NULL,
	[PayType] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MERCHANT_DATASUMARY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_MERCHANT_SUMARY]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPT_MERCHANT_SUMARY](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[SaleAmount] [money] NULL,
	[SaleCount] [bigint] NULL,
	[ReturnAmount] [money] NULL,
	[ReturnCount] [bigint] NULL,
	[TransactionAmount] [money] NULL,
	[TransactionCount] [bigint] NULL,
	[KeyAmount] [money] NULL,
	[KeyedCount] [bigint] NULL,
	[ForeignCardAmount] [money] NULL,
	[ForeignCardCount] [bigint] NULL,
	[DebitAmount] [money] NULL,
	[DebitCount] [bigint] NULL,
	[VisaNetAmount] [money] NULL,
	[VisaTransactionCount] [bigint] NULL,
	[MasterCardNetAmount] [money] NULL,
	[MasterCardTransactionCount] [bigint] NULL,
	[AmericanExpressNetAmount] [money] NULL,
	[AmericanExpressTransactionCount] [bigint] NULL,
	[DiscoverNetAmount] [money] NULL,
	[DiscoverNetCount] [bigint] NULL,
	[DebitCardNetAmount] [money] NULL,
	[DebitCardTransactionCount] [bigint] NULL,
	[OtherNetAmount] [money] NULL,
	[OtherTransactionCount] [bigint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MerchantSummary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_TRANSACTION_DETAIL]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TRANSACTION_DETAIL](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[FileSource] [nvarchar](200) NULL,
	[BatchNumber] [bigint] NOT NULL,
	[TerminalNumber] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[TransactionCode] [char](30) NOT NULL,
	[CardTypeCode] [char](30) NULL,
	[TransactionAmount] [bigint] NULL,
	[TransactionDate] [date] NULL,
	[TransactionTime] [time](7) NULL,
	[KeyedEntry] [bit] NOT NULL,
	[AuthorizationNumber] [char](30) NULL,
	[ReportTime] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
	[AccountNumber] [char](30) NULL,
	[FirstTwelveAccountNumber] [nchar](16) NULL,
	[CountryId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_TransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_ACCOUNT]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountType] [int] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Username] [nvarchar](100) NOT NULL,
	[Password] [nvarchar](200) NOT NULL,
	[Salt] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsHash] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ACCOUNT_TYPE]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](200) NOT NULL,
	[Role] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Account_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_LOG]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LOG](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](500) NOT NULL,
	[ReadDate] [datetime] NULL,
	[Status] [int] NULL,
	[Version] [nchar](10) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_LOG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_AGENT]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AGENT](
	[Id] [bigint] NOT NULL,
	[AgentCode] [char](30) NOT NULL,
	[AgentName] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Agent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_ATTRIBUTE]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_ATTRIBUTE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Entity] [int] NOT NULL,
	[KeyNumber] [bigint] NOT NULL,
	[Key] [nvarchar](200) NOT NULL,
	[Value] [nvarchar](200) NOT NULL,
	[IsPrimary] [bit] NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_TBL_ATTRIBUTE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_MERCHANT]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MERCHANT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumber] [char](30) NOT NULL,
	[BackendProcessor] [bigint] NULL,
	[MerchantName] [nvarchar](512) NOT NULL,
	[Status] [int] NOT NULL,
	[Owner] [nchar](10) NOT NULL,
	[Address] [nvarchar](512) NULL,
	[City] [nvarchar](512) NULL,
	[State] [nvarchar](512) NULL,
	[Zip] [char](20) NULL,
	[Phone] [char](20) NULL,
	[Fax] [char](20) NULL,
	[Email] [char](50) NULL,
	[ApprovalDate] [datetime] NULL,
	[CloseDate] [datetime] NULL,
	[BankCardDBA] [char](50) NOT NULL,
	[FistActiveDate] [datetime] NULL,
	[LastActiveDate] [datetime] NULL,
	[AgentId] [bigint] NULL,
	[MerchantTypeId] [int] NOT NULL,
	[RegionId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Mechant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MERCHANT_TYPE]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MERCHANT_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MerchantTypeName] [nvarchar](200) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MerchantType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_REGION]    Script Date: 12/28/2016 10:53:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_REGION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[IsDelete] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
 CONSTRAINT [PK_TBL_REGION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[INF_MESSAGES] ADD  CONSTRAINT [DF_INF_NOTIFYCATION_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[INF_MESSAGES] ADD  CONSTRAINT [DF_INF_MESSAGES_ParentId]  DEFAULT ((0)) FOR [ParentId]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] ADD  CONSTRAINT [DF_RPT_MERCHANT_DATASUMARY_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] ADD  CONSTRAINT [DF_TBL_MERCHANT_DATASUMARY_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] ADD  CONSTRAINT [DF_TBL_MerchantSummary_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] ADD  CONSTRAINT [DF_TBL_MerchantSummary_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_TBL_TransactionDetail_KeyedEntry]  DEFAULT ((0)) FOR [KeyedEntry]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_TBL_TransactionDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_TBL_TransactionDetail_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] ADD  CONSTRAINT [DF_SYS_ACCOUNT_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] ADD  CONSTRAINT [DF_TBL_Account_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] ADD  CONSTRAINT [DF_TBL_Account_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_TYPE] ADD  CONSTRAINT [DF_SYS_Account_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_TYPE] ADD  CONSTRAINT [DF_SYS_Account_Type_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_LOG] ADD  CONSTRAINT [DF_SYS_LOG_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_LOG] ADD  CONSTRAINT [DF_SYS_LOG_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_AGENT] ADD  CONSTRAINT [DF_TBL_Agent_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_AGENT] ADD  CONSTRAINT [DF_TBL_Agent_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_ATTRIBUTE] ADD  CONSTRAINT [DF_TBL_ATTRIBUTE_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[TBL_MERCHANT] ADD  CONSTRAINT [DF_TBL_Merchant_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_MERCHANT] ADD  CONSTRAINT [DF_TBL_Merchant_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_MERCHANT_TYPE] ADD  CONSTRAINT [DF_TBL_MerchantType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_MERCHANT_TYPE] ADD  CONSTRAINT [DF_TBL_MerchantType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_REGION] ADD  CONSTRAINT [DF_TBL_REGION_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[TBL_REGION] ADD  CONSTRAINT [DF_TBL_REGION_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY]  WITH CHECK ADD  CONSTRAINT [FK_TBL_MERCHANT_DATASUMARY_TBL_Merchant] FOREIGN KEY([MerchantNumberId])
REFERENCES [dbo].[TBL_MERCHANT] ([Id])
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] CHECK CONSTRAINT [FK_TBL_MERCHANT_DATASUMARY_TBL_Merchant]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY]  WITH CHECK ADD  CONSTRAINT [FK_TBL_MERCHANT_SUMARY_TBL_Merchant] FOREIGN KEY([MerchantNumberId])
REFERENCES [dbo].[TBL_MERCHANT] ([Id])
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] CHECK CONSTRAINT [FK_TBL_MERCHANT_SUMARY_TBL_Merchant]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_TBL_TRANSACTION_DETAIL_TBL_Merchant] FOREIGN KEY([MerchantNumberId])
REFERENCES [dbo].[TBL_MERCHANT] ([Id])
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] CHECK CONSTRAINT [FK_TBL_TRANSACTION_DETAIL_TBL_Merchant]
GO
ALTER TABLE [dbo].[TBL_MERCHANT]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Merchant_TBL_MERCHANT_TYPE] FOREIGN KEY([MerchantTypeId])
REFERENCES [dbo].[TBL_MERCHANT_TYPE] ([Id])
GO
ALTER TABLE [dbo].[TBL_MERCHANT] CHECK CONSTRAINT [FK_TBL_Merchant_TBL_MERCHANT_TYPE]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: chua doc | 1: da doc' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'INF_MESSAGES', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: bat dau tin nhan | > 0: reply tin nhan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'INF_MESSAGES', @level2type=N'COLUMN',@level2name=N'ParentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Merchant - 1: Agent - 2...n: orther' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ACCOUNT', @level2type=N'COLUMN',@level2name=N'AccountType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'//Id account cua Merchant hoặc Agent, kèm theo AccountType de phan biet' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ACCOUNT', @level2type=N'COLUMN',@level2name=N'AccountId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Salt for encryption password' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ACCOUNT', @level2type=N'COLUMN',@level2name=N'Salt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'true: pass bi ma hoa - false: pass ko bi ma hoa' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ACCOUNT', @level2type=N'COLUMN',@level2name=N'IsHash'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: admin | 1: agent | 2: merchant' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_ACCOUNT_TYPE', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'null or 0 : khong thuoc agent nao | > 0 : co parent' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TBL_MERCHANT', @level2type=N'COLUMN',@level2name=N'AgentId'
GO
USE [master]
GO
ALTER DATABASE [CardProcessing] SET  READ_WRITE 
GO
