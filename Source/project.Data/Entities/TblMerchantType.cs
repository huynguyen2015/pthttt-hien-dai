namespace project.Data.Entities
{

    public class TblMerchantType : BaseEntity
    {
        public virtual int Id { get; set; }
        public string MerchantTypeName { get; set; }     
    }
}
