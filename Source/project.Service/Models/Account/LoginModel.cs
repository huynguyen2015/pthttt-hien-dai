﻿using System.ComponentModel.DataAnnotations;

namespace project.Service.Models
{
    public class LoginModel
    {
        [Required]
        [MaxLength(100)]
        [MinLength(6)]
        public string Email { get; set; }
        [Required]
        [MaxLength(100)]
        [MinLength(6)]
        public string Password { get; set; }
    }
}