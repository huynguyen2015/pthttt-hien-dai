﻿using System;

namespace Click.Data.Entities
{
    public class MemberModel
    {
        public long MemberId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string PasswordSalt { get; set; }

        public short StatusId { get; set; }

        public bool IsLockedOut { get; set; }

        public DateTimeOffset? LastLockoutDate { get; set; }

        public short FailedPasswordAttemptCount { get; set; }

        public string FbAccessToken { get; set; }

        public string FbUserId { get; set; }

        public bool IsSuperAdmin { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short[] Version { get; set; }

        public virtual string HashedPassword { get; set; }
    }
}