﻿namespace project.Data.Entities
{
    public class SysAccountType : BaseEntity
    {
        public virtual int Id { get; set; }
        public string TypeName { get; set; }
        public int Role { get; set; }     
    }
}