﻿using System;

namespace project.Common
{
    public class DateTimeAdapter : IDateTime
    {
        private DateTimeOffset UnixEpoch()
        {
            return new DateTimeOffset(1970, 1, 1, 0, 0, 0, DateTimeOffset.UtcNow.Offset);
        }

        private DateTimeOffset UnixEpoch(TimeSpan timespan)
        {
            return new DateTimeOffset(1970, 1, 1, 0, 0, 0, timespan);
        }

        public DateTimeOffset UtcNow
        {
            get { return DateTimeOffset.UtcNow; }
        }

        public bool IsNullOrMinValue(DateTimeOffset? dateTime)
        {
            return dateTime == null || dateTime.Equals(DateTimeOffset.MinValue);
        }

        public DateTimeOffset MinValue
        {
            get { return DateTimeOffset.MinValue; }
        }

        public DateTimeOffset GetBirthdayByAge(short? age)
        {
            age = age ?? 0;
            return DateTimeOffset.UtcNow.AddYears(-1 * (short)age);
        }

        public void GetBirthdayByAge(short? startAge, short? endAge, out DateTimeOffset startDate, out DateTimeOffset endDate)
        {
            endDate = GetBirthdayByAge(startAge);
            startDate = endAge == startAge
                ? GetBirthdayByAge(endAge).AddYears(-1)
                : GetBirthdayByAge(endAge);
            startDate = GetStartOfDay(startDate);
            endDate = GetEndOfDay(endDate);
        }

        public void GetRangeByBirthday(DateTimeOffset birthday, short rage, out short startAge, out short endAge)
        {
            var age = UtcNow.Year - birthday.Year;
            startAge = (short)(age - rage);
            endAge = (short)(age + rage);
        }
        public short GetAgeByBirthday(DateTimeOffset birthday)
        {
            var age = UtcNow.Year - birthday.Year;
            if (birthday > UtcNow.AddYears(-age))
                --age;
            return (short)age;
        }
        public short GetAgeByBirthday(DateTime birthday)
        {
            var age = UtcNow.Year - birthday.Year;
            if (birthday > UtcNow.AddYears(-age))
                --age;
            return (short)age;
        }

        public DateTimeOffset GetStartOfDay(DateTimeOffset value)
        {
            return new DateTimeOffset(value.Year, value.Month, value.Day, 0, 0, 0, value.Offset);
        }

        public DateTimeOffset GetFirstDayOfWeek(DateTimeOffset value)
        {
            var delta = DayOfWeek.Monday - UtcNow.DayOfWeek;
            return value.AddDays(delta);
        }

        public DateTimeOffset GetFirstDayOfMonth(DateTimeOffset value)
        {
            var firstDayOfMonth = new DateTime(value.Year, value.Month, 1);
            return new DateTimeOffset(firstDayOfMonth);
        }

        public TimeSpan GetUtcOffset()
        {
            return UtcNow.Offset;
        }

        public long GetCurrentUnixTimestampSeconds()
        {
            return (long)(UtcNow - UnixEpoch()).TotalSeconds;
        }

        public long GetCurrentUnixTimestampMillis()
        {
            return (long)(UtcNow - UnixEpoch()).TotalMilliseconds;
        }

        public long UnixTimestampSecondsFromDateTimeOffset(DateTimeOffset value)
        {
            return (long)(value - UnixEpoch(value.Offset)).TotalSeconds;
        }

        public long UnixTimestampMillisFromDateTimeOffset(DateTimeOffset value)
        {
            return (long)(value - UnixEpoch(value.Offset)).TotalMilliseconds;
        }

        public DateTimeOffset DateTimeOffsetFromUnixTimestampMillis(long millis)
        {
            return UnixEpoch().AddMilliseconds(millis);
        }

        public DateTimeOffset DateTimeOffsetFromUnixTimestampSeconds(long seconds)
        {
            return UnixEpoch().AddSeconds(seconds);
        }
        public DateTimeOffset DateTimeOffsetFromString(string value)
        {
            return DateTimeOffset.Parse(value);
        }

        public DateTimeOffset UniversalTimeNow
        {
            get { return UtcNow.ToUniversalTime(); }
        }


        public DateTimeOffset GetUtcFromEtc(string etcTime)
        {
            DateTime result = DateTime.ParseExact(
                etcTime,
                "yyyy-MM-dd HH:mm:ss 'Etc/GMT'",
                System.Globalization.CultureInfo.InvariantCulture,
                System.Globalization.DateTimeStyles.AssumeUniversal);
            return result.ToUniversalTime();
        }

        public DateTimeOffset GetEndOfDay(DateTimeOffset value)
        {
            return new DateTimeOffset(value.Year, value.Month, value.Day, 23, 59, 59, value.Offset);
        }

        public DateTimeOffset GetStartOfToday()
        {
            var today = this.UtcNow;
            return new DateTimeOffset(today.Year, today.Month, today.Day, 0, 0, 0, today.Offset);
        }
        public long UnixTimestampMillisFromMaxId(long? value)
        {
            if (value != null && value != 0)
                return value.Value;
            else
                return UnixTimestampMillisFromDateTimeOffset(this.UtcNow);
        }

    }
}