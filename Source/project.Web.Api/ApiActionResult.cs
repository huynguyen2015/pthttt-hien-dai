﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace project.Web.Api
{
    public class ApiActionResult : IHttpActionResult
    {
        private readonly HttpRequestMessage _requestMessage;
        private readonly HttpResponseMessage _responeMessage;
        public object Result { get; private set; }

        public ApiActionResult(HttpRequestMessage requestMessage, object result)
        {
            _requestMessage = requestMessage;
            Result = result;
        }

        public ApiActionResult(HttpResponseMessage responeMessage, object result)
        {
            _responeMessage = responeMessage;
            Result = result;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        public HttpResponseMessage Execute()
        {
            if (_responeMessage != null)
                return _responeMessage;
            else
                return _requestMessage.CreateResponse(HttpStatusCode.OK, Result);
        }
    }
}