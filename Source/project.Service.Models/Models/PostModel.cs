using System;

namespace Click.Data.Entities
{
    public class PostModel
    {
        public long PostId { get; set; }

        public string Content { get; set; }

        public long? MemberId { get; set; }

        public short StatusId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short[] Version { get; set; }

        public short PostTypeId { get; set; }


    }
}