﻿using System;
using System.Collections.Generic;

namespace project.Common
{
    public class Constants
    {

        public static class CommonParameterNames
        {
            public const string PageNumber = "pageNumber";
            public const string PageSize = "pageSize";
            public const string TotalItemCount = "totalItemCount";
            public const string ParentTypeId = "parentTypeId";
            public const string ParentId = "parentId";
            public const string StartAge = "startAge";
            public const string EndAge = "endAge";
            public const string Lat = "lat";
            public const string Lng = "lng";
            public const string Radius = "radius";             
            public const string LocationTypeId = "locationTypeId";
            public const string MemberId = "memberId";
            public const string StartDate = "startdate";
            public const string EndDate = "enddate";
            public const string Keyword = "keyword";
            public const string MaxId = "maxId";
            public const string From = "from";
            public const string To = "to";            
            public const string OrderId = "orderId";
            public const string CreateAt = "createAt";
            public const string UpdateAt = "updateAt";
        }

        public static class HttpHeaderKeyNames
        {
            public const string ApplicationTokenId = "ApplicationTokenId";
            public const string Password = "Password";
            public const string ClassifyId = "ClassifyId";
            public const string DeviceTokenId = "DeviceTokenId";
            public const string FBUserId = "FBUserId";
            public const string FBAccessToken = "FBAccessToken";
            public const string Authorization = "Authorization";
        }

        public static class RegexValues
        {
            public const string Email =
                @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
        }
        public static class GeocodeValues
        {
            public const double MinLat = -90;
            public const double MaxLat = +90;
            public const double MinLng = -180;
            public const double MaxLng = +180;
            public const string StateComponent = "administrative_area_level_1";
            public const string CountryComponent = "country";
            public const string PoliticalComponent = "political";
        }
    
        public static class MediaTypeNames
        {
            public const string ApplicationXml = "application/xml";
            public const string TextXml = "text/xml";
            public const string ApplicationJson = "application/json";
            public const string TextJson = "text/json";
            public const string ImageJpeg = "image/jpeg";
            public const string ImageJpg = "image/jpg";
            public const string ImagePng = "image/png";
            public const string VideoMp4 = "video/mp4";
            public const string VideoMov = "video/mov";
            public const string VideoQuickTime = "video/quicktime";
            public const string ExtensionImageDefault = ".png";
            public const string ExtensionImageDefaultJpeg = ".jpeg";
        }
        public static class Paging
        {
            public const int MinPageSize = 1;
            public const int MinPageNumber = 1;
            public const int DefaultPageNumber = 1;
            public const int DefaultPageSize = 20;
            public const int MaxPageSize = 50;
            public const int DefaultConversationPageSize = 200;
            public const int MaxConversationPageSize = 1000;

        }

        public static class Integer
        {
            public const int Default = 1;
        }

        public static class AuthenticationTypeNames
        {
            public const string None = "None";
            public const string Basic = "basic";
            public const string Digest = "Digest";
            public const string Tokens = "Tokens";
        }

        public static class ErrorCodeValues
        {
            public const int InternalError = 500;
            public const int BadRequest = 400;
            public const int Unauthorized = 401;
            public const int NotFound = 404;
            public const int BadSessionToken = 999;
            public const int CreateMemberFail = 1000;

        }

        public static class HttpMethodNames
        {
            public const string Post = "POST";
            public const string Get = "GET";
            public const string Put = "PUT";
            public const string Delete = "DELETE";
        }


        public static class RoleNames
        {
            public const string Manager = "Manager";
            public const string SeniorWorker = "SeniorWorker";
            public const string JuniorWorker = "JuniorWorker";
        }

        public static class EmailLength
        {
            public const int Max = 64;
        }
        public static class PasswordLength
        {
            public const int Min = 6;
            public const int Max = 64;
        }
        public static class SessionToken
        {
            public const int Day = 90;
            public const int PasswordExpireDay = 3;
            public const int VerifyEmailExpireDay = 3;
            public const string HeaderSession = "Bearer ";
        }

        public static class RetrievingObjKey
        {
            public const string Model = "model";
            public const string Files = "files";
        }

        #region Travel
        public static class TourCategory
        {
            public const short BacBo = 1;
            public const short NamBo = 2;
            public const short TrungBo = 3;
            public const short DuLichDao = 4;
        }
        #endregion

        #region CONSTANS ENUM

        #region STATUS DEFINE

        public static class Role
        {            
            public const short Admin = 1;
            public const short Agent = 2;            
            public const short Merchant = 3;
            public const string StrAdmin = "Administator";
            public const string StrAgent = "Agent";
            public const string StrMerchant = "Merchant";
        }

        public static class AccountTypeIds
        {            
            public const short Merchant = 1;
            public const short Agent = 2;                    
        }

        public static class StatusIds
        {
            //public const short Default = 1;
            public const short Active = 1;
            public const short Inactive = 2;
            public const short Locked = 3;            
            public const short Deleted = 4;
            public const short Rejected = 5;
            public const short AdminRemoved = 10;
            public const short SelfRemoved = 11;
        }

        #endregion

        public static class DeviceType
        {
            public const short iOS = 1;
            public const short Android = 2;
            public const short Other = 3;
            public const short Web = 4;
        }

        #endregion
        
    }
}