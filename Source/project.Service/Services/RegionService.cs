﻿using project.Common;
using project.Common.Exceptions;
using project.Common.TypeMapping;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Interfaces;
using project.Service.Models;
using project.Service.Models.Region;
using project.Service.Services.Interfaces;
using System;

namespace project.Service.Services
{
    public class RegionService : BaseService, IRegionService
    {
        private IRegionRepository _regionRepository;
        public RegionService(IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper,
            IRegionRepository regionRepository,
            TravelContext context)
            : base(context, autoMapper, dateTime, exceptionHelper)
        {
            _regionRepository = regionRepository;
        }

        public RegionModel GetById(int id)
        {
            var value = _regionRepository.GetById(id);
            return new RegionModel
            {
                CreatedAt = value.CreatedAt,
                UpdatedAt = value.UpdatedAt,             
            };
        }

        public bool Insert(RegionModel value)
        {
            var entity = AutoMapper.Map<TblRegion>(value);
            _regionRepository.Insert(entity);
            return true;
        }
        
        public bool Update(RegionModel model)
        {
            try
            {
                var entity = AutoMapper.Map<TblRegion>(model);
                _regionRepository.Update(entity);
                return true;
            }
            catch
            {
                //TODO: change error code later
                throw WrapException<BusinessException>(204);
            }
        }

        public bool Delete(RegionModel model)
        {
            try
            {
                var entity = AutoMapper.Map<TblRegion>(model);
                _regionRepository.Delete(entity);
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                throw WrapException<BusinessException>(204);
            }
        }

        public PagedDataInquiryResponse<RegionModel> FilterRegion(RegionDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _regionRepository.FilterRegion(dataRequest);
            return WrapPagedResponse<RegionModel,TblRegion>(queryResult, dataRequest);
        }     
    }
}