﻿
namespace project.Web.Common.Security
{
    public class MembershipUserWrapper
    {
        public long MemberId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int LanguageId { get; set; }
        public string SessionToken { get; set; }
    }
}