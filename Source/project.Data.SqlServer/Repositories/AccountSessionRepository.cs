﻿using System;
using project.Common;
using project.Common.Logging;
using project.Data;
using project.Data.Entities;
using project.Repository.Intefaces;

namespace project.Repository.Repositories
{
    public class AccountSessionRepository : EfRepository<SysAccountSession>, IAccountSessionRepository
    {
        public AccountSessionRepository(TravelContext context, IDateTime datetime, ILogManager logManager)
            : base(context, datetime, logManager)
        {
        }
       
    }
}