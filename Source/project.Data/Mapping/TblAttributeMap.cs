﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class TblAttributeMap : BaseEntityMap<TblAttribute>
    {
        public TblAttributeMap(){
            ToTable("TBL_ATTRIBUTE");
            HasKey(o => o.Id);
            Property(o => o.Entity);
            Property(o => o.KeyNumber);
            Property(o => o.Key);
            Property(o => o.Value);
            Property(o => o.IsPrimary);      
        }
    }
}