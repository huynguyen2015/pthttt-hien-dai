﻿using project.Service.Models.Agent;
using project.Service.Models.Region;
using System;

namespace project.Service.Models.Merchant
{
    public class MerchantModel : BaseModel
    {
        public string MerchantNumber { get; set; }
        public long BackendProcessor { get; set; }
        public string MerchantName { get; set; }
        public int Status { get; set; }
        public string Owner { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string BankCardDBA { get; set; }
        public DateTime FistActiveDate { get; set; }
        public DateTime? LastActiveDate { get; set; }
        public long AgentId { get; set; }
        public int MerchantTypeId { get; set; }
        public int RegionId { get; set; }
        public MerchantTypeModel MerchantType { get; set; }
        public RegionModel Region { get; set; }
        public AgentModel Agent { get; set; }
    }
}