﻿using System.Web;
using System.Web.Http.ExceptionHandling;
using project.Common;
using project.Common.Exceptions;

namespace project.Web.Common.ErrorHandling
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var exception = context.Exception;
            var httpException = exception as HttpException;
            if (httpException != null)
            {
                context.Result = new SimpleErrorResult(context.Request, httpException.GetHttpCode(), httpException.Message);
                return;
            }
            if (exception is RootObjectNotFoundException)
            {
                context.Result = new SimpleErrorResult(context.Request, Constants.ErrorCodeValues.NotFound, exception.Message);
                return;
            }
            if (exception is ChildObjectNotFoundException)
            {
                context.Result = new SimpleErrorResult(context.Request, Constants.ErrorCodeValues.BadRequest, exception.Message);
                return;
            }
            if (exception is BusinessRuleViolationException)
            {
                context.Result = new SimpleErrorResult(context.Request, Constants.ErrorCodeValues.BadRequest, exception.Message);
                return;
            }
            if (exception is AuthorizationException)
            {
                context.Result = new SimpleErrorResult(context.Request, Constants.ErrorCodeValues.Unauthorized, exception.Message);
                return;
            }
            if (exception is TokenValidationException)
            {
                context.Result = new SimpleErrorResult(context.Request, Constants.ErrorCodeValues.BadSessionToken, exception.Message);
                return;
            }
            if (exception is BaseException)
            {
                var memException = exception as BaseException;
                context.Result = new SimpleErrorResult(context.Request, memException.ExceptionMessage.ErrorCode, memException.ExceptionMessage.ErrorMessage);
                return;
            }
            context.Result = new SimpleErrorResult(context.Request, Constants.ErrorCodeValues.InternalError, exception.Message);
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }
    }
}