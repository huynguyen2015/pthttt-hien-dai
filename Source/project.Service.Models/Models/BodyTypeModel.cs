namespace Click.Data.Entities
{
    using System;

    public  class BodyTypeModel
    {
      
        public short BodyTypeId { get; set; }

        public string Name { get; set; }

        

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short AboutMeCategoryId { get; set; }


    }
}
