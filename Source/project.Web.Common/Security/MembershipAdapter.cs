﻿using System;
using project.Common;
using project.Common.Logging;
using project.Common.Security;
using project.Data.Entities;
using project.Repository.Intefaces;

namespace project.Web.Common.Security
{
    public class MembershipAdapter : IMembershipInfoProvider
    {
        private readonly IAccountRepository _memberQueryProcessor;
        private readonly ICrypto _crypto;
        private readonly IDateTime _dateTime;

        public MembershipAdapter(IAccountRepository memberQueryProcessor
            , ICrypto crypto
            , IDateTime dateTime
            , ILogManager logManager)
        {
            _memberQueryProcessor = memberQueryProcessor;
            _crypto = crypto;
            _dateTime = dateTime;
        }

        public void ValidateMember(string email, string password)
        {
           
        }

        public MembershipUserWrapper GetMembershipInfoById(long memberId)
        {
            SysAccount member = _memberQueryProcessor.GetById(memberId);
            return CreateMembershipUserWrapper(member, _dateTime.UtcNow);
        }

        public MembershipUserWrapper GetMembershipInfoBySessionToken(string sessionToken)
        {
            return new MembershipUserWrapper();
        }

        public string[] GetRolesForMember(string username)
        {
            //TODO: later
            return new string[0];
        }

        private MembershipUserWrapper CreateMembershipUserWrapper(SysAccount member, DateTimeOffset lastActivity)
        {
            if (member == null)
            {
                return null;
            }

            return new MembershipUserWrapper
            {
                //MemberId = member.MemberId,
                //Email = member.Email,
               // Username = member.UserName,
            };
        }

        public bool IsUserValid(SysAccount member, string password)
        {    
            return IsPasswordValid(member, password);
        }

        private bool IsPasswordValid(SysAccount member, string password)
        {
            if (member.IsHash == false)
                if (member.Password.Equals(password)) return true;
                else
                {
                    return string.Equals(
                   _crypto.EncryptPassword(password, member.Salt),
                   member.Password);
                }
            return false;
        }

        public void Logout(Guid sessionTokenId, long memberId, DateTimeOffset currentTime)
        {
            //var member = _memberQueryProcessor.GetMembershipBySessionToken(sessionTokenId.ToString());
            //if (member == null)
            //    throw new TokenValidationException("Your session has been expired.");
            //_memberQueryProcessor.Logout(sessionTokenId, memberId, currentTime);
        }
    }
}