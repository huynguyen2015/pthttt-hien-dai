IF OBJECT_ID('sp_ViewMerchants', 'P') IS NOT NULL
DROP PROC sp_ViewMerchants
GO

CREATE PROC sp_ViewMerchants
	@regionId int,
	@merchantTypeId int,
	@agentId bigint,
	@minIncome money,
	@maxIncome money,
	@reportYear int
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       mc.[Status] as [Status],
	       mc.[Owner] as [Owner],
	       mc.[Address] as [Address],
	       mc.[City] as [City],
	       mc.[State] as [State],
	       mc.[Zip] as [Zip],
	       mc.[Phone] as [Phone],
	       mc.[Fax] as [Fax],
	       mc.[Email] as [Email],
	       mc.[ApprovalDate] as [ApprovalDate],
	       mc.[CloseDate] as [CloseDate],
	       mc.[BankCardDBA] as [BankCardDBA],
	       mc.[FistActiveDate] as [FistActiveDate],
	       mc.[LastActiveDate] as [LastActiveDate],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       summary.[TotalAmount] as [TotalAmount],
	       summary.[TotalCount] as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
	INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
	LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
	LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	LEFT OUTER JOIN dbo.RPT_MERCHANT_DATASUMARY summary ON summary.MerchantNumberId = mc.MerchantNumber 
	                                                   AND summary.DateType = 3 --Year
	WHERE (CASE WHEN @regionId > 0 THEN @regionId ELSE mc.RegionId END) = mc.RegionId
	AND (CASE WHEN @merchantTypeId > 0 THEN @merchantTypeId ELSE mc.MerchantTypeId END) = mc.MerchantTypeId
	AND (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
	AND (CASE WHEN @minIncome > 0 THEN @minIncome ELSE summary.TotalAmount END) >= summary.TotalAmount
	AND (CASE WHEN @maxIncome > 0 THEN @maxIncome ELSE summary.TotalAmount END) >= summary.TotalAmount
	AND (CASE WHEN @reportYear > 0 THEN @reportYear ELSE DATEPART(year, summary.DateReport) END) = DATEPART(year, summary.DateReport)
	
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]
END