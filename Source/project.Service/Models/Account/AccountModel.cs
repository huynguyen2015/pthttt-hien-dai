﻿namespace project.Service.Models
{
    public class AccountModel: BaseModel
    {
        public int AccountTypeId { get; set; }
        public long AccountId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public bool IsActive { get; set; }
        public bool IsHash { get; set; }
        public short RoleId { get; set; }

        public string SessionToken { get; set; }
    }
}