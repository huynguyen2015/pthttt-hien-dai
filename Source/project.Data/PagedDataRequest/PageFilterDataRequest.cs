﻿using System;

namespace project.Data.PagedDataRequest
{
    public class PageFilterDataRequest : PagedDataRequest
    {
        public PageFilterDataRequest(int pageNumber, int pageSize, DateTimeOffset lastTime, FilterParameter filterParameter)
            : base(pageNumber, pageSize)
        {
            FilterParameter = filterParameter;
            LastTime = lastTime;
        }
        public FilterParameter FilterParameter { get; set; }
    }
}