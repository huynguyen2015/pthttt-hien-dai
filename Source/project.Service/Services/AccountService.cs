﻿using System;
using System.Collections.Generic;
using project.Common;
using project.Common.Exceptions;
using project.Common.Security;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Intefaces;
using project.Service.Models;
using project.Service.Services.Interfaces;
using project.Web.Common.Security;
using System.Text.RegularExpressions;
using project.Data;

namespace project.Service.Services
{
    public class AccountService : BaseService, IAccountService
    {        
        private IAccountRepository _accountRepository;
        private IMembershipInfoProvider _membershipInfoProvider;
        private IFileHelper _fileHelper;
        private IAccountSessionRepository _accountSessionRepository;
        private JwtIssuer _jwtIssuer;
        private ICrypto _crypto;
        public AccountService(IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper
            , TravelContext context
            , IAccountRepository accountRepository
            , IMembershipInfoProvider membershipInfoProvider
            , ICrypto crypto
            , IFileHelper fileHelper
            , IAccountSessionRepository accountSessionRepository
            , JwtIssuer jwtIssuer)
            : base(context, autoMapper, dateTime, exceptionHelper)
        {            
            _accountRepository = accountRepository;
            _membershipInfoProvider = membershipInfoProvider;
            _accountSessionRepository = accountSessionRepository;
            _jwtIssuer = jwtIssuer;
            _crypto = crypto;
            _fileHelper = fileHelper;
        }

        /// <summary>
        /// Khiet Tran, 2016-08-18, Get member By Id
        /// </summary>
        /// <param name="pagedDataRequest"></param>        
        /// <returns></returns>
        public AccountModel GetById(int memberId)
        {
            var member = _accountRepository.GetById(memberId);
            return new AccountModel
            {
                CreatedAt = member.CreatedAt,
                UpdatedAt = member.UpdatedAt,
            };
        }

        /// <summary>
        /// Khiet Tran, 2016-10-24, Insert a member
        /// </summary>
        /// <param name="AccountModel"></param>        
        /// <returns></returns>
        public AccountModel Insert(AccountModel model)
        {
            try { 
                if (string.IsNullOrEmpty(model.Password)
                   || model.Password.Length < Constants.PasswordLength.Min
                   || model.Password.Length > Constants.PasswordLength.Max)
                    throw WrapException<BusinessException>("Độ dài mật khẩu từ 6-64 ký tự");

                if (model.Email.Length > Constants.EmailLength.Max
                    || !Regex.IsMatch(model.Email, Constants.RegexValues.Email, RegexOptions.IgnoreCase))
                    throw WrapException<BusinessException>("Sai email khoặc không đúng định dạng");

                if (_accountRepository.GetAccountByEmail(model.Email) != null)
                    throw WrapException<BusinessException>("Tài khoản email đã tồn tại");

                if (model.RoleId != Constants.Role.Merchant && model.RoleId != Constants.Role.Agent)
                    throw WrapException<BusinessException>("Không hỗ trợ loại tài khoản này");

                var entity = AutoMapper.Map<SysAccount>(model);
                PreInsertProcess(entity, model.Password);
                                        
                _accountRepository.Insert(entity);
               
                // Add AccountSession
                string sessionToken = SaveSessionToken(entity.Id, entity.Email);

                _context.SaveChanges();
                var result = AutoMapper.Map<AccountModel>(entity);
                result.SessionToken = sessionToken;
                return result;
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Khiet Tran, 2016-10-24, Update a member
        /// </summary>
        /// <param name="AccountModel"></param>        
        /// <returns></returns>
        public bool Update(AccountModel model)
        {
            if(model == null)
                throw WrapException<BusinessException>("Sai thông tin tài khoản");

            if (!string.IsNullOrEmpty(model.Password)
               &&(model.Password.Length < Constants.PasswordLength.Min
               || model.Password.Length > Constants.PasswordLength.Max))
                throw WrapException<BusinessException>("Độ dài mật khẩu từ 6-64 ký tự");

            if (model.Email.Length > Constants.EmailLength.Max
                || !Regex.IsMatch(model.Email, Constants.RegexValues.Email, RegexOptions.IgnoreCase))
                throw WrapException<BusinessException>("Sai email khoặc không đúng định dạng");
            var entity = _accountRepository.GetAccountByEmail(model.Email);
            if (entity != null && model.Id != entity.Id)
                throw WrapException<BusinessException>("Địa chỉ email đã có người sử dụng");
          
            _accountRepository.Update(entity);
            _context.SaveChanges();
            return true;
        }

        public bool Delete(AccountModel model)
        {
            var entity = AutoMapper.Map<SysAccount>(model);
            _accountRepository.Delete(entity);
            return true;
        }

        /// <summary>
        /// Khiet Tran, 2016-08-18, Filter member
        /// </summary>
        /// <param name="pagedDataRequest"></param>        
        /// <returns></returns>
        public PagedDataInquiryResponse<AccountModel> FilterMember(PagedDataRequest pagedDataRequest)
        {
            if (pagedDataRequest == null)
                throw WrapException<BusinessException>(3304);            
                       
            var queryResult = _accountRepository.FilterAccount(pagedDataRequest);
            return WrapPagedResponse<AccountModel, SysAccount>(queryResult, pagedDataRequest);
        }

        /// <summary>
        /// Huy Nguyen, 2016-10-26, Login
        /// </summary>        
        /// <param name="model"></param>
        /// <param name="requiredRoles"></param>
        /// <returns></returns>  
        public AccountModel Login(LoginModel model, List<short> requiredRoles)
        {
            if (string.IsNullOrEmpty(model.Password)
                || model.Password.Length < Constants.PasswordLength.Min
                || model.Password.Length > Constants.PasswordLength.Max)
                throw WrapException<BusinessException>("Độ dài mật khẩu từ 6-64 ký tự");
            
            if (model.Email.Length > Constants.EmailLength.Max
                || !Regex.IsMatch(model.Email, Constants.RegexValues.Email, RegexOptions.IgnoreCase))
                throw WrapException<BusinessException>("Sai email khoặc không đúng định dạng");

            var memberEntity = _accountRepository.GetAccountByEmail(model.Email);            
            if (memberEntity == null)
                throw WrapException<BusinessException>("Tài khoản không tồn tại.");
            if (memberEntity.StatusId != Constants.StatusIds.Active)
                throw WrapException<BusinessException>("Tài khoản hiện tại không được kích hoạt.");                        
            if(!_membershipInfoProvider.IsUserValid(memberEntity, model.Password))
                throw WrapException<BusinessException>("Mật khẩu không đúng.");
            var memberModel = AutoMapper.Map<AccountModel>(memberEntity);
            memberModel.SessionToken = SaveSessionToken(memberEntity.Id, memberEntity.Email);
            
            return memberModel;
        }

        /// <summary>
        /// Huy Nguyen, 2016-10-26, Logout
        /// </summary>        
        /// <param name="model"></param>
        /// <returns></returns>  
        public bool Logout(long memberId, long sessionTokenId, short deviceTypeId)
        {           
            //Update session token and set expiration at is now.
            var accountSession = _accountSessionRepository.GetById(sessionTokenId);
            if (accountSession == null) return true;
            accountSession.SessionToken = string.Empty;
            accountSession.ExpirationAt = DateTime.UtcNow.Date;
            _accountSessionRepository.Update(accountSession);
            return true;
        }
       
        public AccountModel GetMemberByEmail(string email)
        {
            var result = _accountRepository.GetAccountByEmail(email);
            var returnedModel = AutoMapper.Map<AccountModel>(result);
            return returnedModel;
        }

        #region PRIVATE MENTHOD
        private void PreInsertProcess(SysAccount entity, string password)
        {
            entity.StatusId = Constants.StatusIds.Active;
            entity.AccountTypeId = entity.AccountTypeId > 0 ? entity.AccountTypeId : Constants.AccountTypeIds.Merchant;
            entity.Salt = _crypto.GenerateSalt();
            entity.Password = _crypto.EncryptPassword(password ?? RandomString(6), entity.Salt);            
        }

        private string SaveSessionToken(long memberId, string email)
        {
            var memberEntity = _accountRepository.GetById(memberId);
            if (memberEntity == null)
                throw WrapException<BusinessException>(2209);
            var sessionToken = new SysAccountSession()
            {
                AccountId = memberEntity.Id,
                SessionToken = string.Empty,
                ExpirationAt = DateTime.UtcNow.Date.AddDays(Constants.SessionToken.Day)
            };
            _accountSessionRepository.Insert(sessionToken);
            _context.SaveChanges();
            var roles = new List<string>(new[] { memberEntity.RoleId == Constants.Role.Admin ? Constants.Role.StrAdmin : (memberEntity.RoleId == Constants.Role.Agent ? Constants.Role.StrAgent : Constants.Role.StrMerchant) });
            var sessionTokenGuid = _jwtIssuer.GenerateJwt(memberId, email, roles, Constants.SessionToken.Day, sessionToken.Id);
            sessionToken.SessionToken = sessionTokenGuid;
            _accountSessionRepository.Update(sessionToken);
            _context.SaveChanges();
            return sessionTokenGuid;
        }

        #endregion
    }
}
