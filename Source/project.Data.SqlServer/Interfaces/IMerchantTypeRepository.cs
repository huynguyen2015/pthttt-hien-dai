﻿using project.Common;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;

namespace project.Repository.Interfaces
{
    public interface IMerchantTypeRepository : IRepository<TblMerchantType>
    {
        QueryResult<TblMerchantType> FilterMerchantType(MerchantTypeDataRequest dataRequest);
    }
}