﻿using project.Common.Logging;
using project.Common.Security;
using project.Web.Common.Security;
using log4net.Config;
using Ninject;
using project.Data;
using System.Configuration;
using project.Data.TransactionHelper;
using System;
using project.Web.Common.TransactionHelper;

namespace project.Service.DIConfiguration
{
    public class NinjectConfigurator
    {
        public void Configure(IKernel container)
        {
            AddBindings(container);
        }

        private void AddBindings(IKernel container)
        {
            ConfigureLog4net(container);
            ConfigureUserSession(container);
            ConfigureEntityFramework(container);
            AutoMappingConfigurator.Configure(container);
            HelperConfigurator.Configure(container);
            ProcessorConfigurator.Configure(container);
        }

        
        /// <summary>
        /// Set up log4net for this application, including putting it in the given containerBuilder.
        /// </summary>
        private void ConfigureLog4net(IKernel container)
        {
            XmlConfigurator.Configure();
            var logManager = new LogManagerAdapter();
            container.Bind<ILogManager>().ToConstant(logManager);
        }
             
        /// <summary>
        /// Used to fetch the current thread's principal as an <see cref="IWebUserSession"/> object.
        /// </summary>
        private void ConfigureUserSession(IKernel container)
        {
            var userSession = new UserSession();
            container.Bind<IUserSession>().ToConstant(userSession).InSingletonScope();
            container.Bind<IWebUserSession>().ToConstant(userSession).InSingletonScope();            
        }

        /// <summary>
        /// Config for entity framework
        /// </summary>
        /// <param name="containerBuilder"></param>
        private void ConfigureEntityFramework(IKernel containerBuilder)
        {
            containerBuilder.Bind<IDbContext>().To<TravelContext>().InSingletonScope().WithConstructorArgument("connectionString", ConfigurationManager.ConnectionStrings["CardProcessingConnectionString"].ConnectionString);
            containerBuilder.Bind<IDbContextScope>().To<DbContextScope>().InSingletonScope();
            containerBuilder.Bind<ITransactionHelper>().To<TransactionHelper>().InSingletonScope();
            containerBuilder.Bind<IDataProvider>().To<DataProvider>().InSingletonScope();
        }
    }
}