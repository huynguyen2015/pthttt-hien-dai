namespace Click.Data.Entities
{
    using System;


    public class NotificationMemberModel
    {
        public long NotificationId { get; set; }

        public bool IsRead { get; set; }

        public long MemberId { get; set; }

        public short StatusId { get; set; }

        public long NotificationMemberId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

    }
}