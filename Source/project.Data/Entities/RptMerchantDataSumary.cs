using System;

namespace project.Data.Entities
{

    public class RptMerchantDataSumary : BaseEntity
    {
        public virtual long Id { get; set; }
        public int DateType { get; set; }
        public decimal TotalAmount { get; set; }
        public long TotalCount { get; set; }
        public long MerchantId { get; set; }
        public DateTime DateReport { get; set; }
        public int PayType { get; set; }
        
        public TblMerchant Merchant { get; set; }
    }
}
