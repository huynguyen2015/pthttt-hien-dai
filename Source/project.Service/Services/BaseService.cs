﻿using System;
using System.Collections.Generic;
using System.Linq;
using project.Common;
using project.Common.Exceptions;
using project.Common.TypeMapping;
using project.Data;
using project.Service.Models;

namespace project.Service.Services
{
    public abstract class BaseService
    {
        // Generic dependency
        protected IAutoMapper AutoMapper;           
        protected IDateTime DateTime;
        protected IExceptionHelper ExceptionHelper;
        protected TravelContext _context;

        // constructor
        protected BaseService(TravelContext context, IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper)
        {
            _context = context;
            AutoMapper = autoMapper;
            DateTime = dateTime;
            ExceptionHelper = exceptionHelper;            
        }

        #region //common methods

        protected List<TDestination> MapEntitiesToModels<TDestination, TSource>(IEnumerable<TSource> sources)
        {
            return sources.Select(x => AutoMapper.Map<TDestination>(x)).ToList();
        }

        /// <summary>
        /// Wrap and throw business exception base on T type
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="errorCode"></param>
        protected void WrapAndThrowException<TException>(int errorCode) where TException : BusinessException
        {
            var exceptionalException = ExceptionHelper
                .ResourceToExceptionMessage(Resources.ErrorCodes.ResourceManager, errorCode.ToString());
            var businessException = Activator.CreateInstance(typeof(TException), exceptionalException);

            throw (TException)businessException;
        }

        protected TException WrapException<TException>(int errorCode) where TException : BusinessException
        {
            var exceptionalException = ExceptionHelper
                .ResourceToExceptionMessage(Resources.ErrorCodes.ResourceManager, errorCode.ToString());
            var businessException = Activator.CreateInstance(typeof(TException), exceptionalException);

            return (TException)businessException;
        }

        protected TException WrapException<TException>(string errorMessage) where TException : BusinessException
        {
            var exceptionalException = new ExceptionMessage();
            exceptionalException.ErrorMessage = errorMessage;                
            var businessException = Activator.CreateInstance(typeof(BusinessException), exceptionalException);

            return (TException)businessException;
        }

        protected PagedDataInquiryResponse<TModel> WrapPagedResponse<TModel, TEntity>
            (QueryResult<TEntity> queryResult, Data.PagedDataRequest.PagedDataRequest pagedDataRequest)
        {
            var response = new PagedDataInquiryResponse<TModel>
            {
                Items = this.MapEntitiesToModels<TModel, TEntity>(queryResult.QueriedItems),
                PageCount = queryResult.TotalPageCount,
                PageNumber = pagedDataRequest.PageNumber,
                PageSize = pagedDataRequest.PageSize,
                TotalItemCount = queryResult.TotalItemCount,
                MaxId = DateTime.UnixTimestampMillisFromDateTimeOffset(pagedDataRequest.LastTime)
            };
            return response;
        }

        protected PagedDataInquiryResponse<TModel> WrapPagedResponse<TModel>
            (List<TModel> items, Data.PagedDataRequest.PagedDataRequest pagedDataRequest, int totalPageCount, int totalItemCount)
        {
            var response = new PagedDataInquiryResponse<TModel>
            {
                Items = items,
                PageCount = totalPageCount,
                PageNumber = pagedDataRequest.PageNumber,
                PageSize = pagedDataRequest.PageSize,
                TotalItemCount = totalItemCount,
                MaxId = DateTime.UnixTimestampMillisFromDateTimeOffset(pagedDataRequest.LastTime)
            };
            return response;
        }
       
        /// <summary>
        /// generate value by and bit with pow 2
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        protected long GenerateValue(IEnumerable<int> items)
        {
            return items.Sum(item => (long)Math.Pow(2, item));
        }

        public string RandomString(int length = 8)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        #endregion

    }
}