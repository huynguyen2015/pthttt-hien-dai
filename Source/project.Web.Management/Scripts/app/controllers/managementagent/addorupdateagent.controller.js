/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function() {
    function addOrUpdateAgentController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params) {
        $scope.agentModel = params;
      
        $scope.closeClick = function() {
            $uibModalInstance.dismiss("cancel");
        }

        $scope.saveClick = function () {            
            if (isEmpty($scope.agentModel.AgentName)) {
                alertify.logPosition("top right").error("Tên đại lý không được để trống");
                return;
            }
            if (isEmpty($scope.agentModel.AgentCode)) {
                alertify.logPosition("top right").error("Mã đại lý không được để trống");
                return;
            }
            var url = params && params.Id > 0 ? apiRoutes.managementAgentUpdate : apiRoutes.managementAgentAdd;
            httpService.sendPost(url, $scope.agentModel).then(function (response) {
                 if (response && response.Success === true) {
                     $uibModalInstance.close("success");
                 }
             });
        }
       
    }

    angular.module('app').controller('AddOrUpdateAgentController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify', 'params', addOrUpdateAgentController]);
})();
