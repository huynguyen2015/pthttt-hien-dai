﻿using System;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using System.Text;
using System.Linq;

namespace project.Common.Security
{
    public class Hash
    {
        /**
         * the function handles generate Salt
         * we temporary using a string with length 10 bytes
         */
        public static byte[] getSaltBytes()
        {
            //Generate a cryptographic with default string okFree
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = Encoding.ASCII.GetBytes("okFree"); ;
            rng.GetBytes(buff);

            return buff;
        }

        public static string getSaltString()
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = Encoding.ASCII.GetBytes("okFree"); ;
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        public static string getPasswordHash(string plainText)
        {
            // hash with SHA256
            HashAlgorithm algorithm = new SHA256Managed();
            byte[] rawPasswordByte = Encoding.ASCII.GetBytes(plainText);
            return System.Text.Encoding.UTF8.GetString(algorithm.ComputeHash(rawPasswordByte)).TrimEnd('\0');
        }

        public static string getPasswordHashSalt(string plainText)
        {
            byte[] salt = getSaltBytes();
            byte[] rawPasswordByte = Encoding.ASCII.GetBytes(plainText);
            byte[] combinePasswordWithSalt = rawPasswordByte.Concat(salt).ToArray();

            // hash with SHA256
            HashAlgorithm algorithm = new SHA256Managed();

            return System.Text.Encoding.UTF8.GetString(algorithm.ComputeHash(combinePasswordWithSalt)).TrimEnd('\0');
        }
    }
}