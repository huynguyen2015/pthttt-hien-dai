using System;

/// FILE TEMP FOR TEST FIRST DEMO
namespace travel.Data.Entities
{

    public class MemberProfile : BaseEntity
    {           
        public long MemberId { get; set; }           

        public string Username { get; set; }

        public string AvatarUrl { get; set; }

        public int StatusId { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime LastOnlineAt { get; set; }     

        public virtual Member Member { get; set; }

    }
}
