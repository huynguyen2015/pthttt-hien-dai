﻿using System;

namespace project.Data.TransactionHelper
{    
    public interface IDbContextScope : IDisposable
    {       
        void Commit();
       
        void Rollback();
    
        void Dispose();       
    }
}
