namespace Click.Data.Entities
{
    using System;
    public class PaymentPackageModel
    {
        public short PaymentPackageId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short SubscriptionTypeId { get; set; }

    }
}