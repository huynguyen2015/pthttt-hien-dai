﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models.Region;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.AutoMappingConfiguration
{
    public class TblRegionToRegionModel : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<TblRegion, RegionModel>()
                .ForMember(dst => dst.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dst => dst.Name, x => x.MapFrom(src => src.Name))
                .ForMember(dst => dst.Description, x => x.MapFrom(src => src.Description));
        }
    }
}