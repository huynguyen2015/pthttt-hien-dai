﻿using System;

namespace project.Data.PagedDataRequest
{
    public class AgentDataRequest : PagedDataRequest
    {
        public AgentDataRequest()         
        {

        }
        public AgentDataRequest(int pageNumber, int pageSize)
            : base(pageNumber, pageSize)
        {
        
        }

        public AgentDataRequest(int pageNumber, int pageSize, DateTime lastTime)
            : base(pageNumber, pageSize, lastTime)
        {
        
        }


        public AgentDataRequest(int pageNumber, int pageSize, int orderId)
            : base(pageNumber, pageSize, orderId)
        {
           
        }

        public DateTime? CreatedFrom { get; set; }
        public DateTime? CreatedTo { get; set; }
        public string AgentName { get; set; }
        public string AgentCode { get; set; }
    }
}