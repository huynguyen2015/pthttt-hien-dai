var translationsVI = {
    app_name: 'Quản Trị Hệ Thống',
    menu: {
        dashboard: 'Trang chủ',
        member: {
            member: 'Thành viên',
            profile: 'Thông tin người dùng',
            role: 'Loại người dùng',
            permission: 'Quyền sử dụng'
        },
        post: {
            post: 'Post'
        },
        place: {
            place: 'Địa điểm',
            category: 'Danh mục',
            detail: 'Chi tiết',
            category: 'Danh mục',
            image: 'Hình ảnh'
        },
        /* TRAN TUNG - Vehicle menu*/
        vehicle: {
            vehicle: 'Phương tiện',
            details: 'Chi tiết',
            category: 'Loại',
            company: 'Công ty'
        }
        /* TRAN TUNG - Vehicle menu*/
    },
    page: {
        dashboard: {
            title: 'Trang chủ'
        },
        member: {
            detail: {
                tabname: {
                    dashboard: 'Dashboard',
                    linked_account: 'Linked Account',
                    account: 'Account',
                    purchase: 'Purchase',
                    photos_video: 'Photos & Video',
                    posts: 'Posts',
                    comments: 'Comments',
                    communication_with_admin: 'Communication with admin',
                    reported: 'Reported'
                }
            },
            search: {
                title: "Tìm kiếm",
                username: "Tên đăng nhập",
                password: 'Mật khẩu đăng nhập',
                placeholderusernameoremail: "Who",
                joinedfrom: "Joined from",
                to: "to",
                searchresults: "Search results: ",
                shortednumber: "No.",
                fullname: "Họ tên",
                id: "Id",
                age: "Age",
                location: "Location",
                id: "Id",
                status: "Trạng thái",
                joined: "Ngày tạo",
                lastlogin: "Last login",
                role: "Loại thành viên",
                checked: "Checked",
                sendmessage: "Gửi tin nhắn",
                email: 'Email sử dụng',
                FailedLoginCount: 'Đăng nhập sai',
                createBy: 'Tạo bởi',
                createAt: 'Tạo ngày',
                updateBy: 'Cập nhật bởi',
                updateAt: 'Cập nhật ngày',
                imageUrl: 'Hình đại diện',
                birthday: 'Ngày sinh',
                ssn: 'Chứng minh thư',
                ssn_short: 'SSN',
                phoneNumber: 'Số điện thoại',
                address: 'Địa chỉ',
                lastOnline: 'Last Online',
                delete: 'Xoá',
                update: 'Cập nhật',
                addorupdate: 'THÔNG TIN NGƯỜI DÙNG',
                refresh: 'Làm mới',
                avatar: 'Hình đại diện',
                account: 'Thông tin tài khoản',
                profile: 'Thông tin chi tiết'
            }
        },
        post: {
            queries: {
                title: "Queries",
                username: "Username",
                shortednumber: "No.",
                type: "Type",
                role: "Role",
                daterecieved: "Date Recieved",
                content: "Content",
                nooftask: "No of Tasks",
                reply: "Reply",
                addtask: "Add tasks",
                completeclose: "Complete & Close"
            }
        }, 
        role: {
            title: "Search",
            name: "Name",
            placeholdername: "Name",
            createdfrom: "Created from",
            to: "to",
            shortednumber: "No.",
            createdat: "Created at",
            updatedat: "Updated at",
            searchresults: "Search results: ",
            status: "Status",
            actions: "Actions",
            addroletitle: "Add Role",
            permissions: "Permissions",
        },
        permission: {
            title: "Search",
            name: "Name",
            placeholdername: "Name",
            createdfrom: "Created from",
            to: "to",
            shortednumber: "No.",
            createdat: "Created at",
            updatedat: "Updated at",
            searchresults: "Search results: ",
            status: "Status",
            actions: "Actions",
        },
        place: {
            search: {
                title: "Tìm kiếm",
                name: "Tên địa điểm",
                placeholdername: "",
                searchresults: "Kết quả tìm kiếm: ",
                shortednumber: "STT",
                id: "Id",
                icon: "Icon",
                createdat: "Created at",
                createdby: "Created by",
                updatedat: "Updated at",
                updatedby: "Updated by",
                action: "Thao tác",
                addorupdate: 'Thông tin địa điểm'
            },
            detail: {
                name: "Tên",
                placeholdername: "",
                searchresults: "Kết quả tìm kiếm: ",
                shortednumber: "STT",
                id: "Id",
                description: "Mô tả",
                rating: "Đánh giá",
                likenumber: "Số lược thích",
                placecategory: "Loại địa điểm",
                createdat: "Thời gian tạo",
                createdby: "Created by",
                updatedat: "Thời gian cập nhật",
                updatedby: "Updated by",
                action: "Thao tác",
                addorupdate: 'Thông tin chi tiết địa điểm'
            },
            image: {
                title: "Tìm kiếm",
                name: "Tên file",
                placeholdername: "Tên địa điểm",
                searchresults: "Kết quả tìm kiếm: ",
                shortednumber: "STT",
                id: "Id",
                image: "Hình ảnh",
                place: "Địa điểm",
                createdat: "Thời gian tạo",
                createdby: "Created by",
                updatedat: "Thời gian cập nhật",
                updatedby: "Updated by",
                action: "Thao tác",
                addorupdate: 'Thông tin chi tiết địa điểm'
            }
        },
        /* TRAN TUNG - Vehicle menu*/
        vehicle: {
            search: 'Tìm kiếm',
            searchresults: "Kết quả tìm kiếm: ",
            title: 'Phương tiện',
            sortnumber: 'STT',
            createdat: 'Ngày tạo',
            createdby: 'Người tạo',
            updatedat: 'Ngày cập nhật',
            updatedby: 'Người cập nhật',
            update: 'Cập nhật',
            delete: 'Xóa',
            detail: {
                title: 'Chi tiết',
                name: 'Phương tiện',
                description: "Mô tả",
                addorupdate: "Chi tiết Phương tiện"
            },
            category: {
                title: 'Loại',
                name: 'Tên loại',
                iconbase64: "Biểu tượng",
                addorupdate: "Chi tiết loại phương tiện"
            },
            company: {
                title: 'Công ty',
                search: {
                    addorupdate: 'Thông Tin Công Ty',
                    name: 'Tên công ty',
                    phone: 'Số điện thoại',
                    address: 'Địa chỉ',
                    rating: 'Đánh giá',
                    likenumber: 'Lượt thích'
                }
            }
        },
        /* TRAN TUNG - Vehicle menu*/
        paging: {
            itemperpage: "showing {{pagesize}} of {{total}} items"
        }
    },
    btn: {
        search: "Tìm kiếm",
        cancel: "Huỷ bỏ",
        save: "Lưu lại",
        ok: "Đồng ý",
        addrole: "Add Role",
        addpermission: "Add Permission",
        addaccount: 'Add Account',
        addprofile: 'Add Profile',
        refresh: 'Làm mới'
    },
    tour: {
        tour: 'Chuyến đi',
        title: 'Tên chuyến đi',
        dayscount: 'Số ngày',
        description: 'Mô tả',
        likenumber: 'Số người thích',
        rating: 'Lượt bình chọn',
        plantour: 'Hoạt động',
        tourtitle: 'Quản lý chuyến đi',
        cruise: 'Hành trình',
        cruiseDetail: 'Chi tiết hành trình',
        official: 'Chuyến đi hiện tại',
        comment: 'Ý kiến về chuyến đi',
        tourpopup: 'Thông Tin Chuyến Đi',

        // official
        dateStart: 'Ngày bắt đầu',
        priceAdult: 'Giá cho người lớn',
        priceChild: 'Giá cho trẻ em',
        priceInfant: 'Giá cho baby',
        promotionRate: 'Tỉ lệ giảm giá',
        bookNumber: 'Mã chuyến đi',
        totalSeat: 'Số lượng chỗ',
        viewNumber: 'Số lượng người xem',


        // comment
        personComment: 'Người bình luận',
        commentContent: 'Nội dung',
        likecomment: 'Số lượng người thích',

        //cruise
        numberOfDate: 'Số ngày',
        dateActive: 'Ngày',
        detail: 'Chi tiết',

        typeMoney: 'VNĐ',
        cuiseTitle: 'Tiêu đề'
    },
    common: {
        upload: 'Tải lên',
        percentage: '%'
    }
};