﻿
namespace project.Service.Models.Region
{
    public class RegionModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }    
    }
}