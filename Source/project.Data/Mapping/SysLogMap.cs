﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class SysLogMap : BaseEntityMap<SysLog>
    {
        public SysLogMap(){
            ToTable("SYS_LOG");
            HasKey(o => o.Id);
            Property(o => o.FileName);
            Property(o => o.ReadDate);
            Property(o => o.Status);
            Property(o => o.Version);
        }
    }
}