﻿using project.Common;
using project.Common.Exceptions;
using project.Common.TypeMapping;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Interfaces;
using project.Service.Models;
using project.Service.Models.Merchant;
using project.Service.Services.Interfaces;
using System;

namespace project.Service.Services
{
    public class MerchantTypeService : BaseService, IMerchantTypeService
    {
        private IMerchantTypeRepository _merchantTypeRepository;
        public MerchantTypeService(IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper,
            IMerchantTypeRepository merchantTypeRepository,
            TravelContext context)
            : base(context, autoMapper, dateTime, exceptionHelper)
        {
            _merchantTypeRepository = merchantTypeRepository;
        }

        public MerchantTypeModel GetById(int id)
        {
            var value = _merchantTypeRepository.GetById(id);
            return new MerchantTypeModel
            {
                CreatedAt = value.CreatedAt,
                UpdatedAt = value.UpdatedAt,             
            };
        }

        public bool Insert(MerchantTypeModel value)
        {
            var entity = AutoMapper.Map<TblMerchantType>(value);
            _merchantTypeRepository.Insert(entity);
            return true;
        }
        
        public bool Update(MerchantTypeModel model)
        {
            try
            {
                var entity = AutoMapper.Map<TblMerchantType>(model);
                _merchantTypeRepository.Update(entity);
                return true;
            }
            catch
            {
                //TODO: change error code later
                throw WrapException<BusinessException>(204);
            }
        }

        public bool Delete(MerchantTypeModel model)
        {
            try
            {
                var entity = AutoMapper.Map<TblMerchantType>(model);
                _merchantTypeRepository.Delete(entity);
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                throw WrapException<BusinessException>(204);
            }
        }

        public PagedDataInquiryResponse<MerchantTypeModel> FilterMerchantType(MerchantTypeDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _merchantTypeRepository.FilterMerchantType(dataRequest);
            return WrapPagedResponse<MerchantTypeModel,TblMerchantType>(queryResult, dataRequest);
        }   
    }
}