/**
 * @ngdoc overview
 * @name Tour controller
 * @description 
 * @author khiettran
 *
 */

(function() {
    'use strict';
    angular.module('app').controller('TourModalController', tourModalController);

    tourModalController.$inject = ['$scope', '$uibModalInstance', 'commonService'];

    function tourModalController($scope, $uibModalInstance, commonService) {
        
        $scope.activeDays = true;
        $scope.dayCount = 0;
        $scope.days = [];

        $scope.init = function () {
            console.log("aaaa");
        }

        $scope.closeClick = function() {
            $uibModalInstance.dismiss("cancel");
        }

        $scope.numberousDayChange = function () {
            if ($scope.dayCount > 0) {
                $scope.activeDays = true;
                for (var i = 1; i <= $scope.dayCount; i ++) {
                    $scope.days.push(i);
                }
            } else {
                $scope.activeDays = false;
            }
        }


    }
})();
