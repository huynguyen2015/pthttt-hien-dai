/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function() {
    function accountModificationController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params) {
        $scope.roles = Role;
        $scope.roleModel = $scope.roles[0];

        $scope.status = Status;
        $scope.statusModel = $scope.status[0];

        $scope.files = [];

        function processDataRoleShown() {
            if (!params) return;
            for (var i = 0; i < $scope.roles.length; i++) {
                var rm = $scope.roles[i];
                if (params.RoleId == rm.id) {
                    $scope.roleModel = rm;
                    break;
                }
            }
        }
        $scope.data = {
            model: {
                Id: -1,
                Email: '',
                Password: '',
                FailedPasswordCount: 0,
                StatusId: '',
                RoleId: '',
                CreatedBy: -1,
                CreatedAt: new Date(),
                UpdatedBy: -1,
                UpdatedAt: new Date(),
                AccountProfile: {
                    Id: -1,
                    AvatarUrl: '',
                    AccountId: '',
                    Username: '',
                    FullName: '',
                    Birthday: new Date(),
                    Ssn: 0,
                    PhoneNumber: 0,
                    Address: ''
                }
            }, files: []};
      
        $scope.init = function () {
            if (!params) {
                return;
            }

            $scope.data.model.Id = params.Id;
            $scope.data.model.Email = params.Email;
            $scope.data.model.FailedPasswordCount = params.FailedPasswordCount;
            $scope.data.model.Password = params.Password;
            $scope.data.model.CreatedAt = params.CreatedAt;
            $scope.data.model.CreatedBy = params.CreatedBy;
            $scope.data.model.UpdatedAt = params.UpdatedAt;
            $scope.data.model.UpdatedBy = params.UpdatedBy;      
            $scope.data.model.AccountProfile = params.AccountProfile ? params.AccountProfile : $scope.data.model.AccountProfile;

            if ($scope.data.model.AccountProfile) {
                $scope.data.model.AccountProfile.Ssn = parseInt($scope.data.model.AccountProfile.Ssn);
                $scope.data.model.AccountProfile.PhoneNumber = parseInt($scope.data.model.AccountProfile.PhoneNumber);
                

                //$scope.data.model.AccountProfile.Birthday = new Date($scope.data.model.AccountProfile.Birthday);
            }

            $scope.roleModel = Role.filter(function (value) { return value.id == params.RoleId })[0];
            $scope.statusModel = Status.filter(function (value) { return value.id == params.StatusId })[0];
        }
        $scope.selectionChange = function (column, model) {    
            if (column == 'status') {
                $scope.statusModel = model;
            } else if (column == 'role') {
                $scope.roleModel = model;
            }
        }

        $scope.closeClick = function() {
            $uibModalInstance.dismiss("cancel");
        }

        $scope.saveClick = function () {
            var result = validateDataBeforeSubmit();

             if (!isEmpty(result)) {
                 alertify.logPosition("top right").error(result);
                 return;
             }
             var url = params ? apiRoutes.memberUpdate : apiRoutes.memberAdd;

             $scope.data.model.RoleId = $scope.roleModel.id;
             $scope.data.model.StatusId = $scope.statusModel.id;
             console.log($scope.data)
            httpService.sendPostMedia(url, $scope.data).then(function (response) {
                 if (response && response.Success === true) {
                     $uibModalInstance.close("success");
                 }
             });
        }

        function validateDataBeforeSubmit() {
            var errorMessage = '';
            var account = $scope.data.model;
            //validate email
            if (!isEmail(account.Email)) {
                errorMessage = 'Email is not valid';
            
            //set password default to 0 if invalid
            } else if (!isNumber(account.FailedPasswordCount + '')) {
                account.FailedPasswordCount = 0;

            //check password
            } else if (!isValidPassword) {
                errorMessage = 'Password must have at least one number, one lowercase, \n one uppercase letter and at least 8 characters \n';
                errorMessage += ' that are letters, numbers or the underscore';
            }

            return errorMessage;
        }

        $scope.uploadImage = function () {
            var input = $("#avatar")[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#avatar-img')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $scope.hidePassword = function () {
            if (!params) {
                return;
            }

            $("#content-password").hide();
        }

        $scope.showImage = function () {
            var image = $('#avatar-img')
                    .attr('src', $scope.data.model.AccountProfile.AvatarUrl).width(150).height(200);;
        }
    }

    angular.module('app').controller('AccountModificationController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify', 'params', accountModificationController]);
})();
