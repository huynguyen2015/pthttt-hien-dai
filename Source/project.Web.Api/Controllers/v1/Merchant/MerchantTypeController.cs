﻿using project.Service.Models.Merchant;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using System.Net.Http;
using System.Web.Http;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("merchanttype")]
    [UserAudit]
    public class MerchantTypeController : ApiControllerBase<IMerchantTypeControllerDependencyBlock>
    {
        public MerchantTypeController(IMerchantTypeControllerDependencyBlock merchantTypeControllerDependency)
            : base(merchantTypeControllerDependency)
        {
        }

        [Route("filter", Name = "FilterMerchantTypeRoute")]
        [HttpGet]
        public IHttpActionResult FilterMerchantType(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateMerchantTypeDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.MerchantTypeService.FilterMerchantType(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);

            //var result = _controllerDependency.PlaceService.FilterPlace(new PlaceDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("add", Name = "AddMerchantTypeRoute")]
        [HttpPost]
        public HttpResponseMessage AddMerchantType([FromBody] MerchantTypeModel Place)
        {
            var result = _controllerDependency.MerchantTypeService.Insert(Place);
            var returnObj = new
            {
                Place = Place,
                Success = result
            };
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("update", Name = "UpdateMerchantTypeRoute")]
        [HttpPost]
        public HttpResponseMessage UpdateMerchantType([FromBody] MerchantTypeModel Place)
        {
            var result = _controllerDependency.MerchantTypeService.Update(Place);
            var returnObj = new
            {
                Place = Place,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("delete", Name = "DeleteMerchantTypeRoute")]
        [HttpPost]
        public HttpResponseMessage DeleteMerchantType([FromBody] MerchantTypeModel removedPlace)
        {
            var result = _controllerDependency.MerchantTypeService.Delete(removedPlace);
            var returnObj = new
            {
                Place = removedPlace,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }
    }
}