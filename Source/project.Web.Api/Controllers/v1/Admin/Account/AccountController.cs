﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using project.Common.Serialization;
using project.Service.Models;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using project.Common;
using System.Net.Http.Headers;
using project.Web.Api.Filter;
using travel.Service.Models;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("account")]    
    [UserAudit]
    public class AccountController : ApiControllerBase<IAccountControllerDependencyBlock>
    {
        public AccountController(IAccountControllerDependencyBlock membersControllerDependency)
            : base(membersControllerDependency)
        {
            _controllerDependency = membersControllerDependency;
        }

        [Route("add", Name = "AddMemberRoute")]
        [HttpPost]
        //[ValidateMultiPartForm]      
        public async Task<IHttpActionResult> AddMember(HttpRequestMessage requestMessage, AccountModel model)
        {
            //var files = new List<UploadFileStream>();
            //AccountModel model = await GetModel<AccountModel>(requestMessage, files);
            //if (files.Count > 0)            
            //    model.AccountProfile.AvatarUrl = SaveUploadedFile(files[0], "PersonalPhoto");
            
            var result = _controllerDependency.MemberService.Insert(model);
           return WrapApiActionResult(Request, true, result);
        }

        [Route("update", Name ="UpdateMemberRoute")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateMember(HttpRequestMessage requestMessage)
        {
            SerializationHelper _serialization = new SerializationHelper();
            var model = default(AccountModel);
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            var files = new List<UploadFileStream>();
            foreach (var content in provider.Contents)
            {
                if (content.Headers.ContentType == null ||
                    (!content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.ImageJpeg)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.ImageJpg)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.ImagePng)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.VideoMp4)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.VideoMov)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.VideoQuickTime)
                    && !content.Headers.ContentType.MediaType.Equals(
                         Constants.MediaTypeNames.ApplicationJson)))
                    throw new UnsupportedMediaTypeException(
                        "Only support image/jpeg, image/jpg, image/png, video/mp4, video/mov, video/quicktime, application/json",
                        new MediaTypeHeaderValue("multipart/form-data"));
                if (content.Headers.ContentType.MediaType == Constants.MediaTypeNames.ImageJpeg
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.ImageJpg
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.ImagePng
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.VideoMp4
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.VideoMov
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.VideoQuickTime)
                {
                    var fileName = content.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    files.Add(new UploadFileStream
                    {
                        FileStream = await content.ReadAsStreamAsync(),
                        FileName = fileName,
                        ContentType = content.Headers.ContentType.MediaType,
                        Extention = Path.GetExtension(fileName)
                    });
                    continue;
                }
                model =
                    _serialization.DeserializeJsonToObject<AccountModel>(
                        content.ReadAsStringAsync().Result);
            }
            
            //if (null == model.AccountProfile)
            //{
            //    model.AccountProfile = new AccountProfileModel();
            //}
            //try
            //{
            //    if (files.Count > 0)
            //    {
            //        model.AccountProfile.AvatarUrl = SaveUploadedFile(files[0], "PersonalPhoto", model.Email.Replace('@', '_'));
            //    }
            //}
            //catch (System.Exception e) {
            //    throw e;
            //}

            var result = _controllerDependency.MemberService.Update(model);
            return WrapApiActionResult(Request, true, result);
        }

        [Route("delete", Name = "DeleteMemberRoute")]
        [HttpPost]
        public HttpResponseMessage DeleteMember([FromBody] AccountModel removedMember)
        {
            var result = _controllerDependency.MemberService.Delete(removedMember);
            var returnObj = new
            {
                Member = removedMember,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("filter", Name = "FilterMemberRoute")]
        [HttpGet]
        public IHttpActionResult FilterMember(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateMemberFilterDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.MemberService.FilterMember(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("search", Name = "SearchMemberRoute")]
        [HttpGet]
        public IHttpActionResult SearchMember(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateMemberSeachingDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.MemberService.FilterMember(pagedDataRequest);
            return WrapApiActionResult(Request, true, result);
        }

        #region PRIVATE METHOD
        /**
         * Common function for parsing model from request in case using method POST as stream
         */
        //private async Task<AccountModel> ParseModelFromRequest(HttpRequestMessage requestMessage)
        //{
        //    var contentType = Request.Content.Headers.ContentType;
        //    SerializationHelper _serialization = new SerializationHelper();
        //    var model = default(AccountModel);

        //    // in case no file to upload
        //    if (null != contentType && contentType.MediaType == Constants.MediaTypeNames.ApplicationJson && contentType.Parameters.Count == 1)
        //    {
        //        model = _serialization.DeserializeJsonToObject<AccountModel>(Request.Content.ReadAsStringAsync().Result);
        //    }
        //    else
        //    {
        //        var provider = new MultipartMemoryStreamProvider();

        //        var requestStream = Request.Content.ReadAsStreamAsync().Result;
        //        // Read the form data and return an async task.
        //        await ConvertStreamToStreamContent(requestStream).ReadAsMultipartAsync(provider);
        //        Stream file = null;

        //        foreach (var content in provider.Contents)
        //        {

        //            if ("file".Equals(content.Headers.ContentDisposition.Name.Replace("\"", string.Empty)))
        //            {
        //                file = await content.ReadAsStreamAsync();
        //            }

        //            if ("model".Equals(content.Headers.ContentDisposition.Name.Replace("\"", string.Empty)))
        //            {
        //                model =
        //                _serialization.DeserializeJsonToObject<AccountModel>(
        //                    content.ReadAsStringAsync().Result);
        //            }
        //        }

        //        if (file != null)
        //        {
        //            // using email as uploaded file unique
        //            model.AccountProfile.AvatarUrl = "Avatar/" + model.Email.Replace('@', '_').Replace('.', '_');

        //            //_controllerDependency.MemberService.SaveUploadedFile(file, model.AccountProfile.AvatarUrl);
        //        }
        //    }

        //    return model;
        //}

        #endregion
    }
}