﻿using System.Collections.Generic;

namespace project.Service.Models
{
    public class PagedDataInquiryResponse<T>
    {
        private List<T> _items;

        public List<T> Items
        {
            get { return _items ?? (_items = new List<T>()); }
            set { _items = value; }
        }

        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public int TotalItemCount { get; set; }
        public long MaxId { get; set; } //usually this is timestamp or similar
    }
}