﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace project.Web.Common
{
    public class HttpHelper : IHttpHelper
    {
        public string GetFirstHeaderValue(HttpRequestMessage requestMessage, string keyName)
        {
            IEnumerable<string> headerValues;
            if (requestMessage.Headers.TryGetValues(keyName, out headerValues))
                return headerValues == null ? string.Empty : headerValues.FirstOrDefault().ToString();
            return string.Empty;

            
        }
    }
}