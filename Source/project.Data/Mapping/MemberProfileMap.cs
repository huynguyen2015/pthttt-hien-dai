using travel.Data.Entities;

/// FILE TEMP FOR TEST FIRST DEMO
namespace travel.Data.Mapping
{
    public class MemberProfileMap : BaseEntityMap<MemberProfile>
    {
        public MemberProfileMap()
        {
            ToTable("MemberProfile");
            HasKey(o => o.Id);
            Property(o => o.Username).HasMaxLength(50);
            Property(o => o.AvatarUrl);
            Property(o => o.StatusId).IsRequired();
            Property(o => o.Birthday);
            Property(o => o.LastOnlineAt);            

            HasRequired(s => s.Member)
                .WithOptional(ad => ad.MemberProfile);

        }


    }
}
