﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Web.Common.ActionHelper
{
    public class DataResult
    {
        public object Items { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int PageCount { get; set; }
        public int TotalItemCount { get; set; }
        public long MaxId { get; set; }
    }
}