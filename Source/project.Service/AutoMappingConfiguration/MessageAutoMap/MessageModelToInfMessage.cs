﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models.Agent;
using project.Service.Models.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.AutoMappingConfiguration
{
    public class MessageModelToInfMessage : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<MessageModel, InfMessage>()
                .ForMember(dst => dst.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dst => dst.FromAccount, x => x.Ignore())
                .ForMember(dst => dst.ToAccount, x => x.Ignore());
        }
    }
}