﻿using System;

namespace Click.Data.Entities
{
    public class StatusModel
    {
        public short StatusId { get; set; }

        public string Name { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short[] Version { get; set; }
    }
}