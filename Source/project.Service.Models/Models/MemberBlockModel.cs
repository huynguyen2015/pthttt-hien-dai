namespace Click.Data.Entities
{
    using System;
    
    public  class MemberBlockModel
    {
        public int MemberBlockId { get; set; }

        public long MemberId { get; set; }

        public long BlockedMemberId { get; set; }

        public short StatusId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

    }
}
