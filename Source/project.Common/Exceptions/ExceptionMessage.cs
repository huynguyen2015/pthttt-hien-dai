﻿
namespace project.Common.Exceptions
{
    public class ExceptionMessage
    {
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}