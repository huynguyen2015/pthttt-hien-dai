﻿
using project.Data.Entities;

namespace project.Web.Common.Security
{
    public interface IMembershipInfoProvider
    {
        void ValidateMember(string username, string password);

        MembershipUserWrapper GetMembershipInfoBySessionToken(string sessionToken);
        string[] GetRolesForMember(string username);
        bool IsUserValid(SysAccount member, string password);


    }
}
