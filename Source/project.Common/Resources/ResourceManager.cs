﻿using System;

namespace project.Common.Resources
{
    public class ResourceManager : IResourceManager
    {
        public string GetResouce(System.Resources.ResourceManager manager, string name)
        {
            if (manager == null)
                throw new NullReferenceException("ResourceManager is not found");
            if (string.IsNullOrEmpty(name))
                throw new NullReferenceException("Resource is not found");
            return manager.GetString(name);
        }
    }
}