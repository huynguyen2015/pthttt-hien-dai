﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using project.Common;

namespace project.Web.Common.Extensions
{
    public static class HttpRequestMessageExtension
    {
        public static T GetRequestParam<T>(this HttpRequestMessage requestMessage, string paramName)
        {
            var paramValue = default(T);
            
            var paramCollection = requestMessage.RequestUri.ParseQueryString();
            paramValue = PrimitiveTypeParser.Parse<T>(paramCollection[paramName]);

            return paramValue;
        }

        public static IList<T> GetCollectionParam<T>(this HttpRequestMessage requestMessage, string paramName)
        {
            var paramCollection = requestMessage.RequestUri.ParseQueryString();
            var paramValue = requestMessage.GetRequestParam<string>(paramName);
            if (string.IsNullOrWhiteSpace(paramValue))
                return new List<T>();

            string pattern = @"[{}()]";
            Regex rgx = new Regex(pattern);
            paramValue = rgx.Replace(paramValue, string.Empty);

            return paramValue.Split(',').Select(PrimitiveTypeParser.Parse<T>).ToList();
            
        }
    }
}