using System;
using System.ComponentModel;

namespace project.Data.Entities
{
    public class BaseEntity
    {
        public BaseEntity() {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
        
        public virtual DateTime CreatedAt { get; set; }
        public virtual long CreatedUser { get; set; }
        public virtual DateTime UpdatedAt { get; set; }
        public virtual long UpdatedUser { get; set; }

        [DefaultValue(1)]
        public virtual short StatusId { get; set; }
    }
}
