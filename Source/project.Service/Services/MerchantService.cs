﻿using project.Common;
using project.Common.Exceptions;
using project.Common.TypeMapping;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Interfaces;
using project.Service.Models;
using project.Service.Models.Merchant;
using project.Service.Services.Interfaces;
using System;

namespace project.Service.Services
{
    public class MerchantService : BaseService, IMerchantService
    {
        private IMerchantRepository _merchantRepository;
        public MerchantService(IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper,
            IMerchantRepository merchantRepository,
            TravelContext context)
            : base(context, autoMapper, dateTime, exceptionHelper)
        {
            _merchantRepository = merchantRepository;
        }

        public MerchantModel GetById(int id)
        {
            var value = _merchantRepository.GetById(id);
            return new MerchantModel
            {
                CreatedAt = value.CreatedAt,
                UpdatedAt = value.UpdatedAt,             
            };
        }

        public bool Insert(MerchantModel value)
        {
            var entity = AutoMapper.Map<TblMerchant>(value);
            _merchantRepository.Insert(entity);
            return true;
        }
        
        public bool Update(MerchantModel model)
        {
            try
            {
                //var entity = AutoMapper.Map<TblMerchant>(model);
                //_merchantRepository.Update(entity);
                var entity = _merchantRepository.GetById(model.Id);
                if (entity == null)
                    throw WrapException<BusinessException>("Merchant không tồn tại");

                entity.StatusId = model.StatusId;
                entity.MerchantTypeId = model.MerchantTypeId;
                entity.RegionId = model.RegionId;
                entity.AgentId = model.AgentId;
                entity.MerchantNumber = model.MerchantNumber;
                entity.MerchantName = model.MerchantName;
                entity.Owner = model.Owner;
                entity.Address = model.Address;
                entity.City = model.City;
                entity.State = model.State;
                entity.Phone = model.Phone;
                entity.Zip = model.Zip;
                entity.Email = model.Email;
                entity.BankCardDBA = model.BankCardDBA;
                entity.ApprovalDate = model.ApprovalDate;
                entity.FistActiveDate = model.FistActiveDate;
                entity.CloseDate = model.CloseDate;
                entity.LastActiveDate = model.LastActiveDate;
                entity.CreatedAt = model.CreatedAt;
                entity.CreatedUser = model.CreatedUser;
                entity.UpdatedAt = model.UpdatedAt;
                entity.UpdatedUser = model.UpdatedUser;

                _merchantRepository.Update(entity);
                _context.SaveChanges();  
                return true;
            }
            catch
            {
                //TODO: change error code later
                throw WrapException<BusinessException>(204);
            }
        }

        public bool Delete(MerchantModel model)
        {
            try
            {
                var entity = AutoMapper.Map<TblMerchant>(model);
                _merchantRepository.Delete(entity);
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                throw WrapException<BusinessException>(204);
            }
        }

        public PagedDataInquiryResponse<MerchantModel> FilterMerchant(MerchantDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _merchantRepository.FilterMerchant(dataRequest);
            return WrapPagedResponse<MerchantModel,TblMerchant>(queryResult, dataRequest);
        }       
    }
}