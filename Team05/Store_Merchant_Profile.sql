/*
Ngay			Phien ban		Tac gia						Mo ta
27/12/2016		1.0				Phan Thi Thu Thuy			Tao store lay profile user
29/12/2016		1.1				Phan Thi Thu Thuy			Viet tiep store
16/02/2017		1.2				Phan Thi Thu Thuy			Xoa Agent
*/

CREATE PROC sp_MerchantProfile
	@userName NVARCHAR(100)
AS
BEGIN
	DECLARE @accountType INT
	DECLARE @id BIGINT

	SELECT @accountType = [dbo].[SYS_ACCOUNT].[AccountType], @id = [dbo].[SYS_ACCOUNT].[AccountId]
	FROM [dbo].[SYS_ACCOUNT] 
	WHERE [dbo].[SYS_ACCOUNT].[Username] = @userName

	-- [AccountType] = 0 => Account of Master
	-- [AccountType] = 1 => Account of Agent
	-- [AccountType] = 2 => Account of merchant

	IF(@accountType = 1)
	BEGIN
		SELECT [Id]
			  ,[AgentCode]
			  ,[AgentName]
			  ,[CreatedDate]
			  ,[CreatedUser]
			  ,[UpdatedDate]
			  ,[UpdatedUser]
			  ,[IsDeleted]
		  FROM [dbo].[TBL_AGENT]
		  WHERE [dbo].[TBL_AGENT].[Id] = @id
	END 
	ELSE
		IF (@accountType = 2)
		BEGIN
			SELECT [Id]
				  ,[MerchantNumber]
				  ,[BackendProcessor]
				  ,[MerchantName]
				  ,[Status]
				  ,[Owner]
				  ,[Address]
				  ,[City]
				  ,[State]
				  ,[Zip]
				  ,[Phone]
				  ,[Fax]
				  ,[Email]
				  ,[ApprovalDate]
				  ,[CloseDate]
				  ,[BankCardDBA]
				  ,[FistActiveDate]
				  ,[LastActiveDate]
				  ,[AgentId]
				  ,[MerchantTypeId]
				  ,[RegionId]
				  ,[CreatedDate]
				  ,[CreatedUser]
				  ,[UpdatedDate]
				  ,[UpdatedUser]
				  ,[IsDeleted]
			FROM [dbo].[TBL_MERCHANT]
			WHERE [dbo].[TBL_MERCHANT].[Id] = @id
		END
END