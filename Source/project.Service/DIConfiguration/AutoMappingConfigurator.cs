﻿using project.Common.TypeMapping;
using Ninject;
using okFree.Service.AutoMappingConfiguration.TourAutoMap;
using project.Service.AutoMappingConfiguration.MerchantAutoMap;
using project.Service.AutoMappingConfiguration;


namespace project.Service.DIConfiguration
{
    public static class AutoMappingConfigurator
    {
        public static void Configure(IKernel containerBuilder)
        {
            containerBuilder.Bind<IAutoMapper>().To<AutoMapperAdapter>().InSingletonScope();
            // Member - Map
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<AccountModelModelToSysAccount>().InSingletonScope();
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<SysAccountToAccountModel>().InSingletonScope();
          
            //Merchant - Map
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<MerchantModelToTblMerchant>().InSingletonScope();
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<TblMerchantToMerchantModel>().InSingletonScope();

            //Merchant type - Map
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<MerchantTypeModelToTblMerchantType>().InSingletonScope();
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<TblMerchantTypeToMerchantTypeModel>().InSingletonScope();

            //Agent
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<AgentModelToTblAgent>().InSingletonScope();
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<TblAgentToAgentModel>().InSingletonScope();

            //Region
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<RegionModelToTblRegion>().InSingletonScope();
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<TblRegionToRegionModel>().InSingletonScope();

            //Message
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<MessageModelToInfMessage>().InSingletonScope();
            containerBuilder.Bind<IAutoMapperTypeConfigurator>().To<InfMessageToMessageModel>().InSingletonScope();
        }
    }
}

