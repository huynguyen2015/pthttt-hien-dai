﻿using project.Common.Exceptions;
using project.Common.Security;
using project.Common.Serialization;
using project.Web.Common;
using project.Web.Common.ActionHelper;

namespace project.Web.Api.Controllers.v1.Interfaces
{
    public interface IControllerDependencyBlock
    {
        IActionResultHelper ActionResultHelper { get; }
        IHttpHelper HttpHelper { get; }
        IUserSession UserSession { get; }
        IExceptionHelper ExceptionHelper { get; }
        ISerialization Serialization { get; }
    }
}
