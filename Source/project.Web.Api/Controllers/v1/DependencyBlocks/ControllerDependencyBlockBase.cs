﻿using project.Common.Exceptions;
using project.Common.Security;
using project.Common.Serialization;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common;
using project.Web.Common.ActionHelper;

namespace project.Web.Api.Controllers.v1.DependencyBlocks
{
    public abstract class ControllerDependencyBlockBase : IControllerDependencyBlock
    {
        #region //public properties
        public IActionResultHelper ActionResultHelper { get; protected set; }

        public IHttpHelper HttpHelper { get; protected set; }

        public IUserSession UserSession { get; protected set; }

        public IExceptionHelper ExceptionHelper { get; protected set; }

        public ISerialization Serialization { get; protected set; }

        #endregion

        #region //constructor

        protected ControllerDependencyBlockBase()
        {
        }

        protected ControllerDependencyBlockBase(IActionResultHelper actionResultHelper)
        {
            ActionResultHelper = actionResultHelper;
        }
       
        protected ControllerDependencyBlockBase(IActionResultHelper actionResultHelper, IUserSession userSession, ISerialization serialization)
        {
            ActionResultHelper = actionResultHelper;
            UserSession = userSession;
            Serialization = serialization;
        }    

        #endregion
    }
}