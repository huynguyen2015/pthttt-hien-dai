/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function() {
    function placeDetailModificationController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params, NgMap) {
        $scope.data = {
            Id: -1,
            Title: '',
            Description: '',
            Latitude: 10.7797838,
            Longitude: 106.69899480000004,
            Rating: 0.0,
            LikeNumber: 0,
            PlaceCategoryId: -1,
            CreatedBy: '',
            CreatedAt: '',
            UpdatedBy:'',
            UpdatedAt: '',
            PlaceCategory: {
                Id: -1,
                Name: '',
                IconBase64: '',
                CreatedBy: '',
                CreatedAt: '',
                UpdatedBy: '',
                UpdatedAt: ''
            }           
        }

        $scope.center = {
            lat: 10.7797838,
            lng: 106.69899480000004
        };
            
        $scope.placeCategory = {};
        var oldPlaceCategoryId = -1;       
        $scope.render = true;
        $scope.googleMapsUrl = "https://maps.google.com/maps/api/js?key=AIzaSyB2xONcCM5lw4HqsC8tjztvHzkI8IBTU_4&libraries=placeses,visualization,drawing,geometry,places";
        NgMap.getMap().then(function (map) {
            console.log(map.getCenter());
            console.log('markers', map.markers[0].position);
            console.log('shapes', map.shapes);
        }); 
       
        //$scope.vm = {
        //    address: '',
        //    type: "['establishment']",
        //    placeChanged: null,
        //    map: null,
        //    place: null
        //};
        
        //$scope.vm.placeChanged = function () {
        //    $scope.vm.place = this.getPlace();
        //    console.log('location', $scope.vm.place.geometry.location);
        //    $scope.vm.map.setCenter($scope.vm.place.geometry.location);
        //}
        //ngMap.getMap().then(function (map) {
        //    $scope.vm.map = map;
        //});

        $scope.vm = this;
        $scope.vm.types = "['establishment']";
        
        $scope.vm.placeChanged = function () {
            
            $scope.vm.place = this.getPlace();

            console.log('location', $scope.vm.place);
            $scope.vm.map.setCenter($scope.vm.place.geometry.location);
        }


        NgMap.getMap().then(function (map) {
            $scope.vm.map = map;
        });

        console.log($scope.vm);
        function GetPlaceCategory() {
            var filter = { orderId: 1, name: "" };
            httpService.sendGet(apiRoutes.placeCategoryFilter, filter).then(function (response) {
                console.log(response);
                if (response && response.Success === true) {
                    $scope.placeCategory = response.Data ? response.Data.Items : [];     
                }
            });
        };

        $scope.init = function () {
            GetPlaceCategory();
        
            if (!params) {
                return;
            }
          
            $scope.data = params;
            oldPlaceCategoryId = $scope.data.PlaceCategoryId;
            $scope.center.lat = $scope.data.Latitude;
            $scope.center.lng = $scope.data.Longitude;
            console.log($scope.center);
        }
        

        $scope.closeClick = function() {
            $uibModalInstance.dismiss("cancel");
            $scope.data.PlaceCategoryId = oldPlaceCategoryId;
        }

        $scope.saveClick = function () {
            var result = validateDataBeforeSubmit();
            
            if (!isEmpty(result)) {
                alertify.logPosition("top right").error(result);                
                return;
            }
            var url = params ? apiRoutes.placeUpdate : apiRoutes.placeAdd;

            if (params) {
                Updated();
            } else {
                Inserted();
            }         

            console.log($scope.data);
            httpService.sendPost(url, $scope.data).then(function (response) {
                if (response && response.Success === true) {
                    $uibModalInstance.close("success");
                }
            });
        }

        function validateDataBeforeSubmit() {
            var errorMessage = '';
            var place = $scope.data;
            if (isEmpty(place.Title))
                errorMessage = "Title is not empty";
            else if (isEmpty(place.Description))
                errorMessage = "Description is not empty";
            return errorMessage;
        }

        function Updated() {
            $scope.data.UpdatedAt = new Date();
            $scope.data.UpdatedBy = 1;
        }

        function Inserted() {
            $scope.data.CreatedAt = $scope.data.UpdatedAt = new Date();
            ////TODO: change logged in account id
            $scope.data.CreatedBy = $scope.data.UpdatedBy = 1;
        }       

        
    }

    angular.module('app').controller('PlaceDetailModificationController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify', 'params', 'NgMap', placeDetailModificationController]);
})();
