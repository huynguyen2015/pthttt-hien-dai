/**
 * @ngdoc overview
 * @name Permission controller
 * @description 
 *
 */
(function () {
    function permissionSearchController($scope, $uibModal, $filter, commonService, $location, httpService, orderPermission, apiRoutes, alertify) {
        // Declare variable        
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;
        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 6, pageSize: 20, currentItem: '0-20', totalItems: 120 };
            $scope.filter = { orderId: orderPermission.nameAsc, keyword: "" };
        }

        resetFilter();

        function filterPermissions() {
            httpService.sendGet(apiRoutes.permissionFilter, $scope.filter, $scope.setContentLoading).then(function (response) {                
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.paging = commonService.preparePagination(response);
                }
            });
        }

        filterPermissions();

        $scope.sortBy = function (type) {
            switch (type) {
                case "name":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderPermission.nameAsc ? orderPermission.nameDesc : orderPermission.nameAsc;
                        break;
                    }
                case "status":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderPermission.statusAsc ? orderPermission.statusDesc : orderPermission.statusAsc;
                        break;
                    }
                case "createdat":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderPermission.createdatAsc ? orderPermission.createdatDesc : orderPermission.createdatAsc;
                        break;
                    }
                case "updatedat":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderPermission.updatedatAsc ? orderPermission.updatedatDesc : orderPermission.updatedatAsc;
                        break;
                    }
            }
            filterPermissions();
        }
        $scope.getSortClass = function (type) {
            switch (type) {
                case "name":
                    {
                        return $scope.filter.orderId === orderPermission.fullnameAsc ? "sorting_asc" : ($scope.filter.orderId === orderPermission.fullnameDesc ? "sorting_desc" : "sorting");
                    }
                case "status":
                    {
                        return $scope.filter.orderId === orderPermission.idAsc ? 'sorting_asc' : ($scope.filter.orderId === orderPermission.idDesc ? "sorting_desc" : "sorting");
                    }
                case "createdat":
                    {
                        return $scope.filter.orderId === orderPermission.createdatAsc ? 'sorting_asc' : ($scope.filter.orderId === orderPermission.createdatDesc ? "sorting_desc" : "sorting");
                    }
                case "updatedat":
                    {
                        return $scope.filter.orderId === orderPermission.updatedatAsc ? 'sorting_asc' : ($scope.filter.orderId === orderPermission.updatedatDesc ? "sorting_desc" : "sorting");
                    }
                default:
                    return '';
            }
        }
        $scope.searchPageChange = function (flag, pageNumber) {
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1;
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterPermissions();
        };
        $scope.searchClick = function () {
            $scope.filter.pageNumber = 1;
            filterPermissions();
        }
        $scope.createOrEditClick = function (role) {
            commonService.openModal("_addpermissionpopup.html", "PermissionAddPopupController", role, filterPermissions);
        }
        $scope.removeClick = function (item) {
            var confirmMessage = $filter('translate')('messages.confirmMessage');
            alertify.confirm(confirmMessage, function () {
                httpService.sendPost(apiRoutes.permissionRemove, item, $scope.setContentLoading).then(function (response) {
                    if (response && response.Success === true) {
                        filterPermissions();
                    }
                });
            }, function () {

            });
        }
    }

    angular.module('app').controller('PermissionSearchController', ['$scope', '$uibModal', '$filter', 'commonService', '$location', 'HttpService', 'ORDER_PERMISSION'
        , 'API_ROUTES', 'alertify', permissionSearchController]);
})();
