﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using project.Common;
using project.Service.Models;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using travel.Service.Models;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("access")]
    [UserAudit]
    public class AccessController : ApiControllerBase<IAccountControllerDependencyBlock>
    {
        public AccessController(IAccountControllerDependencyBlock memberControllerDependencyBlock)
            : base(memberControllerDependencyBlock)
        {
        }

        /// <summary>
        /// Huy Nguyen, 2016-11-17, Login for admin panel
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("loginadmin", Name = "LoginAdminRoute")]
        [HttpPost]
        public IHttpActionResult Login(HttpRequestMessage requestMessage, [FromBody] LoginModel model)
        {            
            var result = _controllerDependency.MemberService.Login(model, new List<short> {Constants.Role.Admin});
            return WrapApiActionResult(requestMessage, true, result);
        }        

        /// <summary>
        /// Huy Nguyen, 2016-11-17, logout
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("logout", Name = "LogoutRoute")]
        [HttpPost]
        public IHttpActionResult Logout(HttpRequestMessage requestMessage, [FromBody] LoginModel model)
        {
            var sessionTokenId = _controllerDependency.UserSession.SessionTokenId;
            var memberId = _controllerDependency.UserSession.MemberId;
            var isLogout = _controllerDependency.MemberService.Logout((long)memberId, sessionTokenId, Constants.DeviceType.Web);
            return WrapApiActionResult(requestMessage, true, isLogout);
        }

        /// <summary>
        /// Huy Nguyen, 2016-11-17, signup for user app/web
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        [Route("signup", Name = "SignUpRoute")]
        [HttpPost]
        public async Task<IHttpActionResult> AddMember(HttpRequestMessage requestMessage)
        {
            var files = new List<UploadFileStream>();
            AccountModel model = await GetModel<AccountModel>(requestMessage, files);
            //if (files.Count > 0)
            //    model.AccountProfile.AvatarUrl = SaveUploadedFile(files[0], "PersonalPhoto");
            
            var result = _controllerDependency.MemberService.Insert(model);
            return WrapApiActionResult(Request, true, result);
        }
        
        [Route("signupfb", Name = "UserSignUpFBRoute")]
        [HttpPost]
        public IHttpActionResult UserSignUpFacebook(HttpRequestMessage requestMessage, [FromBody] AccountModel model)
        {
            //model.RoleId = Constants.Role.a;
            //var result = _controllerDependency.MemberService.InsertFB(model);
            return WrapApiActionResult(Request, true, true);
        }
    }
}