using System;

namespace Click.Data.Entities
{
    public class MemberFavoriteModel
    {
        public long MemberId { get; set; }

        public long MemberFavoritedId { get; set; }

        public short StatusId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public long MemberFavoriteId { get; set; }

    }
}