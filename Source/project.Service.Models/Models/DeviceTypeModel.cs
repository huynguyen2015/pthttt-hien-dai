namespace Click.Data.Entities
{
    using System;

    public  class DeviceTypeModel
    {
        public short DeviceTypeId { get; set; }
        
        public string Name { get; set; }
        
        public string Description { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        
    }
}
