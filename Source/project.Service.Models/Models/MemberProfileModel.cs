namespace Click.Data.Entities
{
    using System;
    public  class MemberProfileModel
    {

        public long MemberId { get; set; }

        public DateTimeOffset Birthday { get; set; }

        public int? SuburbId { get; set; }

        public short StatusId { get; set; }

        public int? MaritalStatusId { get; set; }

        public int? IdentityId { get; set; }

        public string About { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public bool IsSocialConnected { get; set; }

        public bool IsOnline { get; set; }

        public int? HereForId { get; set; }

        public long? AboutMeId { get; set; }

        public long PostCount { get; set; }
    }
}
