namespace Click.Data.Entities
{
    using System;


    public class NotificationModel
    {
        public long NotificationId { get; set; }

        public short NotificationTypeId { get; set; }

        public long ParentId { get; set; }

        public long? ParentIdOther { get; set; }

        public long CreatedBy { get; set; }

        public string Message { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

    }
}