namespace Click.Data.Entities
{
    public class PostTypeModel
    {
        public short PostTypeId { get; set; }

        public string Name { get; set; }

    }
}