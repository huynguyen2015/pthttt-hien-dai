﻿using travel.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// FILE TEMP FOR TEST FIRST DEMO
namespace travel.Data.Entities
{
    public class Member : BaseEntity
    {    
        [Required]   
        [MaxLength(50)]
        public string Email { get; set; }

        public string PasswordSalt { get; set; }

        public string HashedPassword { get; set; }

        public short FailedPasswordCount { get; set; }
        
        public int StatusId { get; set; }// = Constants.StatusIds.Default;
        public int RoleId { get; set; }// = Constants.Role.Default;
        public virtual Role Role { get; set; }
        public virtual MemberProfile MemberProfile { get; set; }
       
    }
}