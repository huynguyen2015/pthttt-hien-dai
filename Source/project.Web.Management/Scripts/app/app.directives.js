/**
 * @ngdoc function
 * @name app.directives
 * @description
 */

(function() {
    'use strict';

    function initActiveLink($location) {
        return {
            restrict: 'A',
            link: function ($scope, $element, $attrs) {
                var activeClass = $attrs.ngActiveLink;
                var path = $attrs.href;                
                $scope.location = $location;
                $scope.$watch('location.path()', function (newPath) {
                    
                    if (path === newPath) {
                        $element.addClass(activeClass);                       
                    } else {
                        $element.removeClass(activeClass);
                    }
                });
            }
        };
    }

    angular.module('app').directive('ngActiveLink', ["$location", function ($location) {
	        return initActiveLink($location);
	    }
    ]);  

})();
