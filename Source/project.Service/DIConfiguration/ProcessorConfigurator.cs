﻿using project.Repository.Intefaces;
using project.Repository.Repositories;
using project.Web.Common.Security;
using Ninject;
using Ninject.Web.Common;
using project.Service.Services;
using project.Service.Services.Interfaces;
using project.Repository.Interfaces;

namespace project.Service.DIConfiguration
{
    public static class ProcessorConfigurator
    {
        public static void Configure(IKernel containerBuilder)
        {
            #region ACCOUNT

            containerBuilder.Bind<IAccountService>().To<AccountService>().InRequestScope();
            containerBuilder.Bind<IAccountRepository>().To<AccountRepository>().InRequestScope();
            containerBuilder.Bind<IMembershipInfoProvider>().To<MembershipAdapter>().InRequestScope();
            #endregion       

            #region ACOUNTSESSION
            containerBuilder.Bind<IAccountSessionRepository>().To<AccountSessionRepository>().InRequestScope();

            #endregion

            #region MERCHANT
            containerBuilder.Bind<IMerchantRepository>().To<MerchantRepository>().InRequestScope();
            containerBuilder.Bind<IMerchantService>().To<MerchantService>().InRequestScope();
            #endregion

            #region REGION
            containerBuilder.Bind<IRegionRepository>().To<RegionRepository>().InRequestScope();
            containerBuilder.Bind<IRegionService>().To<RegionService>().InRequestScope();
            #endregion

            #region MERCHANT TYPE
            containerBuilder.Bind<IMerchantTypeRepository>().To<MerchantTypeRepository>().InRequestScope();
            containerBuilder.Bind<IMerchantTypeService>().To<MerchantTypeService>().InRequestScope();
            #endregion

            #region AGENT
            containerBuilder.Bind<IAgentRepository>().To<AgentRepository>().InRequestScope();
            containerBuilder.Bind<IAgentService>().To<AgentService>().InRequestScope();
            #endregion

            #region MESSAGE
            containerBuilder.Bind<IInfMessageRepository>().To<InfMessageRepository>().InRequestScope();
            containerBuilder.Bind<IMessageService>().To<MessageService>().InRequestScope();
            #endregion
        }
    }
}
