﻿using System.Collections.Generic;

namespace project.Web.Common.Security
{
    public interface IJwtIssuer
    {
        string GenerateJwt(long memberId, string email, IEnumerable<string> roles, int lifeDay, long SessionTokenId);
       
    }
}
