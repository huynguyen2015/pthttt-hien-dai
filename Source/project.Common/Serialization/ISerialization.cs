﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace project.Common.Serialization
{
    public interface ISerialization
    {
        T DeserializeJsonToObject<T>(string jsonString);
        T ToObject<T>(object obj);
        string SerializeObjectToJson(object obj);

        int SerializeArrayToInteger(IEnumerable<byte> list, short range, short maxValue);
        IEnumerable<byte> DeserializeIntegerToArray(int value);

        string SerializeIEnumerableToJson(IEnumerable<object> list);

        IEnumerable<T> DeserializeJArrayToIEnumerable<T>(JArray obj);

        IEnumerable<T> DeserializeJsonToIEnumerable<T>(string json);

        dynamic SerializeToDynamic<T>(T obj);
    }
}