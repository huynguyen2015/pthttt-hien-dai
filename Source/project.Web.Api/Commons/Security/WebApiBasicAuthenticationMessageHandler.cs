﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using project.Common;
using project.Common.Logging;
using project.Web.Common;
using project.Web.Common.ActionHelper;
using log4net;
using System.Security.Claims;

namespace project.Web.Api.Security
{
    public class WebApiBasicAuthenticationMessageHandler : DelegatingHandler
    {
        private readonly ILog _log;
        private readonly IHttpHelper _httpHelper;
        private readonly IBasicSecurityService _basicSecurityService;

        public WebApiBasicAuthenticationMessageHandler(ILogManager logManager,
            IBasicSecurityService basicSecurityService,
            IHttpHelper httpHelper)
        {
            _basicSecurityService = basicSecurityService;
            _log = logManager.GetLog(typeof(WebApiBasicAuthenticationMessageHandler));
            _httpHelper = httpHelper;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {            
            if (IsAllowAnomynous(request))
                return await base.SendAsync(request, cancellationToken);
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                try
                {
                    var isAuthenticated = Authenticate(request);
                    if (!isAuthenticated)
                        return CreateUnauthorizedResponse(request);
                }
                catch (Exception e)
                {
                    _log.Error("Failure in auth processing", e);
                    return CreateUnauthorizedResponse(request);
                }
                _log.Debug("Already authenticated; passing on to next handler...");
                return await base.SendAsync(request, cancellationToken);
            }

            if (!CanHandleAuthentication(request))
            {
                _log.Debug("Not a basic authentication request ");
                return CreateUnauthorizedResponse(request);
            }
            return CreateUnauthorizedResponse(request);
        }

        public bool Authenticate(HttpRequestMessage request)
        {                       
            try
            {
                long sessionTokenId = long.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("SessionTokenId").Value);
                return _basicSecurityService.IsValidSessionToken(sessionTokenId);
            }
            catch
            {
                return false;
            }            
        }

        private HttpResponseMessage CreateUnauthorizedResponse(HttpRequestMessage request)
        {
            return request.CreateResponse(HttpStatusCode.Unauthorized
                 , new ActionResult
                 {
                     ErrorCode = Constants.ErrorCodeValues.Unauthorized,
                     Success = false,
                     Message = "You're not authentication"
                 });
        }
        private bool CanHandleAuthentication(HttpRequestMessage request)
        {
            //string applicationTokenId = _httpHelper.GetFirstHeaderValue(request, Constants.HttpHeaderKeyNames.ApplicationTokenId);
            return (request.Headers != null
                && request.Headers.Authorization != null
                && request.Headers.Authorization.Scheme.ToLowerInvariant() == Constants.AuthenticationTypeNames.Basic);
            //&& !string.IsNullOrEmpty(applicationTokenId));

        }
        private bool IsAllowAnomynous(HttpRequestMessage request)
        {
            //if (IsFaceBookLogin(request)
            //    || IsFaceBookRegister(request)
            //    || IsLogin(request)
            //    || IsSignup(request)
            //    || IsForgetPassword(request)
            //    || IsLoginByEmail(request)
            //    || IsFaceBookSignup(request)
            //    || IsTermsCondition(request)
            //    || IsResetPassword(request)
            //    || IsReNewPassword(request)
            //    || IsVerifyEmail(request)
            //    || IsExistEmail(request)
            //    || IsUploadPhoto(request)
            //    || IsGetEmoji(request)
            //    || IsMemberFilter(request)
            //    || IsMemberAdd(request)
            //    || TestFilterAccount(request)
            //    || IsAddMessage(request)
            //    )
            //    return true;
            //return false;

            return true;
        }

        private bool IsFaceBookLogin(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "access/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "fblogin");
        }
        private bool TestFilterAccount(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "account/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "filter");
        }

        private bool IsMemberFilter(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "filter");
        }

        private bool IsMemberAdd(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "add");
        }

        private bool IsFaceBookRegister(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "access/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "fbmember");
        }
        private bool IsLogin(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "access/"
                && (request.RequestUri.Segments[4].ToLowerInvariant() == "loginadmin"
                    || request.RequestUri.Segments[4].ToLowerInvariant() == "login"));
        }
        private bool IsSignup(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "account/"
                && (request.RequestUri.Segments[4].ToLowerInvariant() == "add"));
        }

        private bool IsForgetPassword(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "passwordforget");
        }

        private bool IsLoginByEmail(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 6
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "manuallogin/"
                && request.RequestUri.Segments[5].ToLowerInvariant() == "email");
        }

        private bool IsFaceBookSignup(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "signupfb");
        }

        private bool IsTermsCondition(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "termsconditions/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "termsandcondition");
        }

        private bool IsResetPassword(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "resetpw");
        }
        private bool IsVerifyEmail(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "verifyemail");
        }
        private bool IsReNewPassword(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "renewpw");
        }
        private bool IsUploadPhoto(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "photos/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "upload");
        }
        private bool IsExistEmail(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "members/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "isexistemail");
        }

        private bool IsGetEmoji(HttpRequestMessage request)
        {
            return (request.RequestUri != null && request.RequestUri.Segments.Length == 5
                && request.RequestUri.Segments[3].ToLowerInvariant() == "xmpps/"
                && request.RequestUri.Segments[4].ToLowerInvariant() == "emoji");
        }
    }
}