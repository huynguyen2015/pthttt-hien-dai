﻿using System.IO;
using System.Web;

namespace project.Common
{
    public class FileHelper : IFileHelper
    {
        private HttpServerUtility server = System.Web.HttpContext.Current.Server;
        private string folderUpload = string.Empty;

        public FileHelper()
        {
            folderUpload = "~/App_Data/Upload/";
        }

        public System.IO.FileStream Open(string path, System.IO.FileMode mode)
        {
            return File.Open(path, mode);
        }
    }
}