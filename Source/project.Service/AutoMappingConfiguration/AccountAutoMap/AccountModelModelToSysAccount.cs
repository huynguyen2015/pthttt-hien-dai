﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models;

namespace okFree.Service.AutoMappingConfiguration.TourAutoMap
{
    public class AccountModelModelToSysAccount : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<AccountModel, SysAccount>()
                .ForMember(dst => dst.AccountType, x => x.Ignore());

        }


    }
}