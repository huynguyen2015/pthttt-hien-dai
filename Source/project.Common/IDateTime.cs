﻿using System;

namespace project.Common
{
    public interface IDateTime
    {
        DateTimeOffset UtcNow { get; }

        DateTimeOffset UniversalTimeNow { get; }

        bool IsNullOrMinValue(DateTimeOffset? dateTime);

        DateTimeOffset MinValue { get; }

        DateTimeOffset GetBirthdayByAge(short? age);

        short GetAgeByBirthday(DateTimeOffset birthday);

        void GetBirthdayByAge(short? startAge, short? endAge, out DateTimeOffset startDate, out DateTimeOffset endDate);

        void GetRangeByBirthday(DateTimeOffset birthday, short rage, out short startAge, out short endAge);

        DateTimeOffset GetStartOfDay(DateTimeOffset value);

        DateTimeOffset GetEndOfDay(DateTimeOffset value);

        DateTimeOffset GetFirstDayOfWeek(DateTimeOffset value);

        DateTimeOffset GetFirstDayOfMonth(DateTimeOffset value);

        DateTimeOffset GetStartOfToday();

        TimeSpan GetUtcOffset();

        long GetCurrentUnixTimestampSeconds();

        long GetCurrentUnixTimestampMillis();

        long UnixTimestampSecondsFromDateTimeOffset(DateTimeOffset value);

        long UnixTimestampMillisFromDateTimeOffset(DateTimeOffset value);

        DateTimeOffset DateTimeOffsetFromUnixTimestampMillis(long millis);

        DateTimeOffset DateTimeOffsetFromUnixTimestampSeconds(long seconds);

        DateTimeOffset DateTimeOffsetFromString(string value);

        DateTimeOffset GetUtcFromEtc(string etcTime);

        long UnixTimestampMillisFromMaxId(long? maxId);
    }
}
