﻿
namespace project.Web.Common.ActionHelper
{
    public class ActionResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public int ErrorCode { get; set; }
    }
}