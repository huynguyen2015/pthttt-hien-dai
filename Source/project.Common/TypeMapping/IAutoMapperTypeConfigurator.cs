﻿namespace project.Common.TypeMapping
{
    public interface IAutoMapperTypeConfigurator
    {
        void Configure();
    }
}
