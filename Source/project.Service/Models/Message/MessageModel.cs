﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.Models.Message
{
    public class MessageModel : BaseModel
    {
        public string Subject { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; }
        public long FromAccountId { get; set; }
        public long ToAccountId { get; set; }
        public long ParentId { get; set; }
        public int Type { get; set; }
        //public string FromName { get; set; }
        //public string ToName { get; set; }
    }
}