﻿using System;
using project.Data.PagedDataRequest;

namespace project.Web.Api.Commons.Helper
{
    public interface IParseRequestParams
    {
        PagedDataRequest CreatePagedDataRequest(Uri requestUri);        
        PageFilterDataRequest CreateFilterDataRequest(Uri requestUri);        
        PagedDataRequest CreateMemberFilterDataRequest(Uri requestUri);
        PagedDataRequest CreateMemberSeachingDataRequest(Uri uri);
        MerchantDataRequest CreateMerchantDataRequest(Uri requestUri);
        RegionDataRequest CreateRegionDataRequest(Uri requestUri);
        MerchantTypeDataRequest CreateMerchantTypeDataRequest(Uri requestUri);
        AgentDataRequest CreateAgentDataRequest(Uri requestUri);
        AgentDataRequest CreateAgentNoParamsDataRequest(Uri requestUri);
        MessageDataRequest CreateMessageDataRequest(Uri requestUri);
        MessageDataRequest CreateMessageNoParamsDataRequest(Uri requestUri);
    }
}