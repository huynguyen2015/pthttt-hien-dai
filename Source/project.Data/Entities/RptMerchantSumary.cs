using System;

namespace project.Data.Entities
{

    public class RptMerchantSumary : BaseEntity
    {
        public virtual long Id { get; set; }
        public DateTime ReportDate { get; set; }
        public long MerchantId { get; set; }
        public decimal SaleAmount { get; set; }
        public long SaleCount { get; set; }
        public decimal ReturnAmount { get; set; }
        public long ReturnCount { get; set; }
        public decimal TransactionAmount { get; set; }
        public long TransactionCount { get; set; }
        public decimal KeyAmount { get; set; }
        public long KeyedCount { get; set; }
        public decimal ForeignCardAmount { get; set; }
        public long ForeignCardCount { get; set; }
        public decimal DebitAmount { get; set; }
        public long DebitCount { get; set; }
        public decimal VisaNetAmount { get; set; }
        public long VisaTransactionCount { get; set; }
        public decimal MasterCardNetAmount { get; set; }
        public long MasterCardTransactionCount { get; set; }
        public decimal AmericanExpressNetAmount { get; set; }
        public long AmericanExpressTransactionCount { get; set; }
        public decimal DiscoverNetAmount { get; set; }
        public long DiscoverNetCount { get; set; }
        public decimal DebitCardNetAmount { get; set; }
        public long DebitCardTransactionCount { get; set; }
        public decimal OtherNetAmount { get; set; }
        public long OtherTransactionCount { get; set; }

        public TblMerchant Merchant { get; set; }
    }
}
