﻿using System;

namespace project.Service.Models
{
    public class BaseModel
    {
        public BaseModel ()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
        public long Id { get; set; }

        public DateTime CreatedAt { get; set; }
        public long CreatedUser { get; set; }
        public DateTime UpdatedAt { get; set; }
        public long UpdatedUser { get; set; }
        public short StatusId { get; set; }
    }
}