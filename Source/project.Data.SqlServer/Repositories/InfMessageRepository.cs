﻿using project.Common.Logging;
using project.Common;
using project.Data.Entities;
using project.Repository.Intefaces;
using project.Data;
using System.Linq;
using System;
using project.Data.PagedDataRequest;
using project.Common.Exceptions;

namespace project.Repository.Repositories
{
    public class InfMessageRepository : EfRepository<InfMessage>, IInfMessageRepository
    {

        public InfMessageRepository(TravelContext context, IDateTime datetime, ILogManager logManager)
            : base(context, datetime, logManager)
        {

        }

        public QueryResult<InfMessage> FilterMessage(MessageDataRequest dataRequest)
        {
            try
            {
                // Todo: defined Error code
                var startIndex = ResultsPagingUtility.CalculateStartIndex(
                dataRequest.PageNumber, dataRequest.PageSize);

                //Parameter for get list
                var query = Table.Where(obj => obj.StatusId == Constants.StatusIds.Active);

                if (dataRequest.orderId == 1)
                    query = query.OrderBy(c => c.Id);
                else if (dataRequest.orderId == 2)
                    query = query.OrderByDescending(c => c.Id);

                var totalCount = query.Any() ? query.Count() : 0;
                var results = query.Skip(startIndex).Take(dataRequest.PageSize).ToList();
                return new QueryResult<InfMessage>(results, totalCount, dataRequest.PageSize);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not filter member. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, ex.ToString());
            }
        }

        public QueryResult<InfMessage> FilterMessage_NoParams(MessageDataRequest dataRequest)
        {
            try
            {
                // Todo: defined Error code
                var startIndex = ResultsPagingUtility.CalculateStartIndex(
                dataRequest.PageNumber, dataRequest.PageSize);

                //Parameter for get list
                var query = Table.Where(obj => obj.StatusId == Constants.StatusIds.Active);

                if (dataRequest.orderId == 1)
                    query = query.OrderBy(c => c.Id);
                else if (dataRequest.orderId == 2)
                    query = query.OrderByDescending(c => c.Id);

                var totalCount = query.Any() ? query.Count() : 0;
                var results = query.Skip(startIndex).Take(dataRequest.PageSize).ToList();
                return new QueryResult<InfMessage>(results, totalCount, dataRequest.PageSize);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not filter member. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, ex.ToString());
            }
        }

    }
}