﻿using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Web;
using log4net;
using project.Common.Security;
using project.Common;
using project.Common.Logging;
using project.Data.Entities;

namespace project.Web.Api.Security
{
    public class BasicSecurityService : IBasicSecurityService
    {
        private readonly ILog _log;
        private readonly ICrypto _crypto;
        private readonly IDateTime _dateTime;

        public BasicSecurityService(ILogManager logManager, ICrypto crypto, IDateTime dateTime)
        {
            _log = logManager.GetLog(typeof(BasicSecurityService));
            _crypto = crypto;
            _dateTime = dateTime;
        }

        public bool SetPrincipal(string email, string password)
        {
            //var member = GetUser(email);
            //if (!IsUserValid(member, password)) return false;

            //IPrincipal principal = null;
            //if (member == null || (principal = GetPrincipal(member)) == null)
            //{
            //    _log.DebugFormat("System could not validate user {0}", member);
            //    return false;
            //}
            //Thread.CurrentPrincipal = principal;
            //if (HttpContext.Current != null)
            //{
            //    HttpContext.Current.User = principal;
            //}
            return true;
        }

        private bool IsUserValid(SysAccount member, string password)
        {
            return IsPasswordValid(member, password);
        }

        private bool IsPasswordValid(SysAccount member, string password)
        {
            if (member.IsHash == false)
                if (member.Password.Equals(password)) return true;
                else
                {
                    return string.Equals(
                   _crypto.EncryptPassword(password, member.Salt),
                   member.Password);
                }
            return false;
        }

        public virtual IPrincipal GetPrincipal(SysAccount member)
        {
            var identity = new GenericIdentity(member.Email, Constants.AuthenticationTypeNames.Basic);
            identity.AddClaim(new Claim(ClaimTypes.Sid, member.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Email, member.Email));
            var email = member.Email.ToLowerInvariant();
            switch (email)
            {
                case "tt3@g.com":
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.Manager));
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.SeniorWorker));
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.JuniorWorker));
                    break;
                case "tt2@g.com":
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.SeniorWorker));
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.JuniorWorker));
                    break;
                case "tt1@g.com":
                    identity.AddClaim(new Claim(ClaimTypes.Role, Constants.RoleNames.JuniorWorker));
                    break;
                default:
                    return null;
            }
            return new ClaimsPrincipal(identity);
        }

        public virtual SysAccount GetUser(string email)
        {
            return new SysAccount();
                //Session.QueryOver<Member>().Where(x => x.Email == email).SingleOrDefault();
        }

        public bool IsValidSessionToken(long sessionTokenId)
        {
            return true;//Session.QueryOver<SessionToken>()
            //.Where(x => x.SessionTokenId == sessionTokenId && x.TokenId != string.Empty && x.ExpirationAt >= _dateTime.UtcNow)
            //.RowCount() > 0;
        }
    }
}