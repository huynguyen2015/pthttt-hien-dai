1)	PM chỉ được công bố ngày họp trước ít nhất 1 ngày, trong trường hợp khẩn cấp, bắt buộc ngày mai phải họp thì tất cả mọi người không cần thiết phải lên, coi như đó là ngày PM làm việc, ai muốn ủng hộ thì lên.

2)	Sau khi công bố họp, nếu muốn dời thì phải nhắn ngay PM càng sớm càng tốt, quá trễ coi như vô hiệu.

3)	Nếu quá bận vẫn được phép xin nghỉ nhưng phải nhắn với PM hoặc Team leader.


4)	Đối với những trường hợp họp không xin phép, PM sẽ hỏi trực tiếp tới Team leader của người đó và tìm hướng giải quyết.
