/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
(function() {
    'use strict';
    angular
      .module('app', [
        'ngAnimate',
        // 'ngMaterial',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ngStorage',
        'ngStore',
        // 'ngMessages',
        // 'ngMdIcons',
        'jkAngularRatingStars',
        'ui.router',
        'ui.utils',
        'ui.load',
        'ui.jp',
        'oc.lazyLoad',
        'pascalprecht.translate',
        'blockUI',
        'ui.bootstrap',
        'datetimepicker',
        'ngAlertify',
        'checklist-model',
        'summernote',
        'angular-echarts'
      ]);
})();
