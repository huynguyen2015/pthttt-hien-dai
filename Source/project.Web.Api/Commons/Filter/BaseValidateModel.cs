﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;
using project.Common;
using project.Common.Exceptions;
using project.Common.Serialization;
using project.Service.Models;

namespace project.Web.Api.Filter
{
    public class BaseValidateModel : ActionFilterAttribute
    {
        #region <Variables>

        private readonly Func<Dictionary<string, object>, bool> _validate;
        private readonly ISerialization _serialization;
        protected IExceptionHelper ExceptionHelper;

        #endregion

        #region <Constructors>

        protected BaseValidateModel(IExceptionHelper exceptionHelper, ISerialization serialization)
            : this(s =>
                s.ContainsValue(null))
        {
            ExceptionHelper = exceptionHelper;
            _serialization = serialization;
        }

        protected BaseValidateModel(Func<Dictionary<string, object>, bool> checkCondition)
        {
            _validate = checkCondition;
        }

        #endregion

        #region <Override Methods>

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {
                foreach(ModelState modelState in actionContext.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        throw new BusinessException(error.ErrorMessage);
                    }
                }
            }
           
        }

        #endregion

        #region <Public Methods>

        protected T WrapException<T>(int errorCode) where T : BusinessException
        {
            return (T)
                Activator.CreateInstance(typeof (T),
                    ExceptionHelper.ResourceToExceptionMessage(Service.Resources.ErrorCodes.ResourceManager, errorCode.ToString()));
        }       
        #endregion
    }
}