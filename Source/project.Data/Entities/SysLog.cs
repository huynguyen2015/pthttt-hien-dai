﻿using System;

namespace project.Data.Entities
{
    public class SysLog : BaseEntity
    {
        public virtual long Id { get; set; }
        public string FileName { get; set; }
        public DateTime ReadDate { get; set; }
        public int Status { get; set; }
        public string Version { get; set; }                
    }
}