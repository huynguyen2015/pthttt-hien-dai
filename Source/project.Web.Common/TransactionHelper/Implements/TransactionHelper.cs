﻿using System.Linq;
using project.Data;

namespace project.Web.Common.TransactionHelper
{
    public class TransactionHelper: ITransactionHelper
    {
        private readonly TravelContext _context;
        
        public TransactionHelper(TravelContext context)
        {
            _context = context;
        }


        public void Commit()
        {
            _context.SaveChanges();            
        }
        public void Rollback()
        {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}