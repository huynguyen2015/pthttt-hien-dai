﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class TblMerchantTypeMap : BaseEntityMap<TblMerchantType>
    {
        public TblMerchantTypeMap() {
            ToTable("TBL_MERCHANT_TYPE");
            HasKey(o => o.Id);
            Property(o => o.MerchantTypeName);               
      
            //HasRequired(s => s.Account).WithMany(o => o.SessionTokens).HasForeignKey(o => o.AccountId);
        }
    }
}