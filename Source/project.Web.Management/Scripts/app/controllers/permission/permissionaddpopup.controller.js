/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function () {
    function permissionAddPopupController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params) {
        // Declare variable        
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;
        $scope.model = params ? params : {};
        function getPermissions() {
            var permissionFilter = { pageSize: 1000 };
            httpService.sendGet(apiRoutes.permissionFilter, permissionFilter, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    $scope.permissions = response.Data ? response.Data.Items : [];
                }
            });
        }

        getPermissions();

        $scope.closeClick = function () {
            $uibModalInstance.dismiss("cancel");
        }

        $scope.saveClick = function () {
            if (isEmpty($scope.model.Name)) {
                alertify.logPosition("top right").error($filter('translate')('errorMessages.permissionNameCantBeEmpty'));
                return;
            }
                       
            var url = params ? apiRoutes.permissionUpdate : apiRoutes.permissionAdd;
            httpService.sendPost(url, $scope.model).then(function (response) {
                if (response && response.Success === true) {
                    $uibModalInstance.close("success");
                }
            });
        }
    }

    angular.module('app').controller('PermissionAddPopupController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify'
        , 'params', permissionAddPopupController]);
})();
