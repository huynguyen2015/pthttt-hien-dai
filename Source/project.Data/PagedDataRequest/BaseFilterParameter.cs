﻿namespace project.Data.PagedDataRequest
{
    public class BaseFilterParameter
    {

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public long CurrentMemberId { get; set; }
        public long? MaxId { get; set; }

    }
}