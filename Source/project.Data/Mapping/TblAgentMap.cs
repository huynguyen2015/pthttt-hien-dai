﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class TblAgentMap : BaseEntityMap<TblAgent>
    {
        public TblAgentMap(){
            ToTable("TBL_AGENT");
            HasKey(o => o.Id);
            Property(o => o.AgentCode);
            Property(o => o.AgentName);
        }
    }
}