﻿
namespace project.Service.Models.Merchant
{
    public class MerchantTypeModel : BaseModel
    {
        public string MerchantTypeName { get; set; }
    }
}