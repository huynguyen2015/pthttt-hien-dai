using System;

namespace Click.Data.Entities
{
    public class EyeTypeModel
    {
        public short EyeTypeId { get; set; }
        public string Name { get; set; }

        public DateTimeOffset CreateAt { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short AboutMeCategoryId { get; set; }

        
    }
}