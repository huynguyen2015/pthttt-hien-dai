using System;

namespace Click.Data.Entities
{
    public class LikeModel
    {
        public long MemberId { get; set; }

        public short ParentTypeId { get; set; }

        public long ParentId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public long LikeId { get; set; }

    }
}