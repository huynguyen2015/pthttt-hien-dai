using System;

namespace Click.Data.Entities
{
    public class MemberIgnoreModel
    {
        public int MemberIgnoreId { get; set; }

        public long MemberId { get; set; }

        public long IgnoredMemberId { get; set; }

        public short StatusId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        
    }
}