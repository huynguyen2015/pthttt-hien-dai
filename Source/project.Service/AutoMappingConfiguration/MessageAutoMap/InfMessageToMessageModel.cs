﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models.Agent;
using project.Service.Models.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.AutoMappingConfiguration
{
    public class InfMessageToMessageModel : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<InfMessage, MessageModel>()
                .ForMember(dst => dst.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dst => dst.Type, x => x.Ignore());
        }
    }
}