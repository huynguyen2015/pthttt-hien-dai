﻿using project.Common;
using project.Common.Exceptions;
using project.Common.TypeMapping;
using project.Data;
using project.Data.Entities;
using project.Data.Entities.StoreModel;
using project.Data.PagedDataRequest;
using project.Repository.Intefaces;
using project.Service.Models;
using project.Service.Models.Message;
using project.Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace project.Service.Services
{
    public class MessageService : BaseService, IMessageService
    {
        IInfMessageRepository _messageRespository;
        private readonly IDataProvider _dataProvider;
        private readonly IDbContext _dbContext;

        public MessageService(IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper,
            IInfMessageRepository messageRespository,
            IDataProvider dataProvider,
            IDbContext dbContext,
            TravelContext context)
            : base(context, autoMapper, dateTime, exceptionHelper)
        {
            _messageRespository = messageRespository;
            _dataProvider = dataProvider;
            _dbContext = dbContext;
        }

        public int InsertMessageService(MessageModel value)
        {
            //var entity = AutoMapper.Map<InfMessage>(value);
            //entity.StatusId = Constants.StatusIds.Active;
            //_messageRespository.Insert(entity);
            //_context.SaveChanges();

            //var result = AutoMapper.Map<MessageModel>(entity);
            //return result;     

            var pSubject = _dataProvider.GetParameter();
            pSubject.ParameterName = "Subject";
            pSubject.Value = value.Subject;
            pSubject.DbType = DbType.String;

            var pContent = _dataProvider.GetParameter();
            pContent.ParameterName = "Content";
            pContent.Value = value.Content;
            pContent.DbType = DbType.String;

            var pFromAccountId = _dataProvider.GetParameter();
            pFromAccountId.ParameterName = "FromAccountId";
            pFromAccountId.Value = value.FromAccountId;
            pFromAccountId.DbType = DbType.Int64;

            var pToAccountId = _dataProvider.GetParameter();
            pToAccountId.ParameterName = "ToAccountId";
            pToAccountId.Value = value.ToAccountId;
            pToAccountId.DbType = DbType.Int64;

            var pParentId = _dataProvider.GetParameter();
            pParentId.ParameterName = "ParentId";
            pParentId.Value = value.ParentId;
            pParentId.DbType = DbType.Int32;

            var pCreatedUser = _dataProvider.GetParameter();
            pCreatedUser.ParameterName = "CreatedUser";
            pCreatedUser.Value = value.CreatedUser;
            pCreatedUser.DbType = DbType.Int32;

            var rs = _dbContext.ExecuteStoredProcedure("dbo.Ins_INF_MESSAGE_Insert",
                pSubject,
                pContent,
                pFromAccountId,
                pToAccountId,
                pParentId,
                pCreatedUser);

            return rs;
        }

        public int DeleteMessageService(MessageModel value)
        {
            var pUserId = _dataProvider.GetParameter();
            pUserId.ParameterName = "userId";
            pUserId.Value = value.ToAccountId;
            pUserId.DbType = DbType.Int32;

            var pEmailId = _dataProvider.GetParameter();
            pEmailId.ParameterName = "emailId";
            pEmailId.Value = value.Id;
            pEmailId.DbType = DbType.Int32;

            var rs = _dbContext.ExecuteStoredProcedure("dbo.Ins_INF_MESSAGE_Delete",
                pUserId,
                pEmailId);

            return rs;
        }

        public List<EmailModelReturn> GetListEmail(MessageModel value)
        {
            var pUserId = _dataProvider.GetParameter();
            pUserId.ParameterName = "userId";
            pUserId.Value = value.ToAccountId;
            pUserId.DbType = DbType.Int32;

            var pType = _dataProvider.GetParameter();
            pType.ParameterName = "type";
            pType.Value = value.Id;
            pType.DbType = DbType.Int32;

            var rs = _dbContext.ExecuteStoredProcedureList<EmailModel>("Ins_INF_MESSAGE_GetListEmail", pUserId, pType).ToList();

            var list = new List<EmailModelReturn>();
            foreach (var item in rs)
            {
                var model = new EmailModelReturn();
                model.subject = item.Subject;
                model.content = item.Content;
                model.id = item.Id;
                model.from = item.FromName;
                model.to = new ToEmailModel() { name = item.ToName, email = item.ToName };
                model.date = string.Format("{0:dd/MM/yyyy}", item.CreateAt);

                list.Add(model);
            }

            return list;
        }

        public PagedDataInquiryResponse<MessageModel> FilterMessage(MessageDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _messageRespository.FilterMessage(dataRequest);
            return WrapPagedResponse<MessageModel, InfMessage>(queryResult, dataRequest);
        }

        public PagedDataInquiryResponse<MessageModel> FilterMessage_NoParams(MessageDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _messageRespository.FilterMessage_NoParams(dataRequest);
            return WrapPagedResponse<MessageModel, InfMessage>(queryResult, dataRequest);
        }
    }
}