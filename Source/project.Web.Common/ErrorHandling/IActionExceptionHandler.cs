﻿using System.Web.Http.Filters;

namespace project.Web.Common.ErrorHandling
{
    public interface IActionExceptionHandler
    {
        void HandleException(HttpActionExecutedContext filterContext);
    }
}
