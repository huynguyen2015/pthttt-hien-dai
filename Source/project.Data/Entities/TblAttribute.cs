using System;
using System.Collections.Generic;

namespace project.Data.Entities
{

    public class TblAttribute : BaseEntity
    {
        public virtual long Id { get; set; }
        public int Entity { get; set; }
        public long KeyNumber { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public bool IsPrimary { get; set; }          
    }
}
