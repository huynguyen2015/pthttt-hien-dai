﻿
namespace project.Common.Exceptions
{
    public class InternalException : BaseException
    {

        public InternalException(int errorCode, string message)
            : base(errorCode, message)
        {

        }

        public InternalException(ExceptionMessage exceptionMessage)
            : base(exceptionMessage)
        {
        }
    }
}