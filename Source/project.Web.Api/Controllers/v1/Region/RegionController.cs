﻿using project.Service.Models.Region;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("region")]
    [UserAudit]
    public class RegionController : ApiControllerBase<IRegionControllerDependencyBlock>
    {
        public RegionController(IRegionControllerDependencyBlock regionControllerDependency)
            : base(regionControllerDependency)
        {
        }

        [Route("filter", Name = "FilterRegionRoute")]
        [HttpGet]
        public IHttpActionResult FilterRegion(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateRegionDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.RegionService.FilterRegion(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);

            //var result = _controllerDependency.PlaceService.FilterPlace(new PlaceDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("add", Name = "AddRegionRoute")]
        [HttpPost]
        public HttpResponseMessage AddRegion([FromBody] RegionModel Place)
        {
            var result = _controllerDependency.RegionService.Insert(Place);
            var returnObj = new
            {
                Place = Place,
                Success = result
            };
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("update", Name = "UpdateRegionRoute")]
        [HttpPost]
        public HttpResponseMessage UpdateRegion([FromBody] RegionModel Place)
        {
            var result = _controllerDependency.RegionService.Update(Place);
            var returnObj = new
            {
                Place = Place,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("delete", Name = "DeleteRegionRoute")]
        [HttpPost]
        public HttpResponseMessage DeleteRegion([FromBody] RegionModel removedPlace)
        {
            var result = _controllerDependency.RegionService.Delete(removedPlace);
            var returnObj = new
            {
                Place = removedPlace,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }
    }
}