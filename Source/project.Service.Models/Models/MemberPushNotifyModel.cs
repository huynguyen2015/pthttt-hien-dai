using System;

namespace Click.Data.Entities
{
    public class MemberPushNotifyModel
    {
        public long MemberPushNotifyId { get; set; }

        public long MemberId { get; set; }

        public short NotifyOptionId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; } 
    }
}