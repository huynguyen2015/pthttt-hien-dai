namespace Click.Data.Entities
{
    using System;
    public class PaymentStatusModel
    {
        public short PaymentStatusId { get; set; }

        public string Name { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        
    }
}