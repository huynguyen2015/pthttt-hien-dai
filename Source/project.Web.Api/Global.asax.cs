﻿using System.Web.Http;
using project.Common.Logging;
using project.Common.TypeMapping;
using project.Web.Api.Security;
using project.Web.Common;
using JwtAuthForWebAPI;

namespace project.Web.Api
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //AutofacConfigurator.Initialize();
            GlobalConfiguration.Configure(WebApiConfig.Register);

            new AutoMapperConfigurator().Configure(
                WebContainerManager.GetAll<IAutoMapperTypeConfigurator>());

            RegisterHandlers();      
        }



        private void RegisterHandlers()
        {

            var logManager = WebContainerManager.Get<ILogManager>();           

            var builder = new SecurityTokenBuilder();
            var reader = new ConfigurationReader();
            var jwtAuthenticationMessageHandler = new JwtAuthenticationMessageHandler
                                                    {
                                                        AllowedAudience = reader.AllowedAudience,
                                                        Issuer = reader.Issuer,
                                                        SigningToken = builder.CreateFromKey(reader.SymmetricKey)
                                                    };
            GlobalConfiguration.Configuration.MessageHandlers.Add(jwtAuthenticationMessageHandler);

            var httpHelper = WebContainerManager.Get<IHttpHelper>();
            var basicSecurityService = WebContainerManager.Get<IBasicSecurityService>();
            GlobalConfiguration.Configuration.MessageHandlers.Add(
                new WebApiBasicAuthenticationMessageHandler(logManager, basicSecurityService, httpHelper));

        }

        protected void Application_Error()
        {
            var exception = Server.GetLastError();

            if (exception != null)
            {
                var log = WebContainerManager.Get<ILogManager>().GetLog(typeof(WebApiApplication));
                log.Error("Unhandled exception.", exception);
            }
        }
    }
}