﻿using System;

namespace project.Data.PagedDataRequest
{
    public class RegionDataRequest : PagedDataRequest
    {
        public RegionDataRequest()         
        {

        }
        public RegionDataRequest(int pageNumber, int pageSize)
            : base(pageNumber, pageSize)
        {
        
        }

        public RegionDataRequest(int pageNumber, int pageSize, DateTime lastTime)
            : base(pageNumber, pageSize, lastTime)
        {
        
        }


        public RegionDataRequest(int pageNumber, int pageSize, int orderId)
            : base(pageNumber, pageSize, orderId)
        {
           
        }

        public int OrderId { get; set; }
    }
}