﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using project.Data;
using project.Web.Common.TransactionHelper;

namespace project.Web.Common.Filter
{
    public class DbContextScope : ActionFilterAttribute
    {       
        private TravelContext _context { get; set; }
               
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            _context = (TravelContext)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(TravelContext));
            _context.SaveChanges();            
        }
    }
}