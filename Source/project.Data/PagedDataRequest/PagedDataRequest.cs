﻿using System;

namespace project.Data.PagedDataRequest
{
    public class PagedDataRequest
    {
        public PagedDataRequest()
        {
            PageNumber = 1;
            PageSize = 20;
        }
        public PagedDataRequest(int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
        }
        public PagedDataRequest(int pageNumber, int pageSize, DateTimeOffset lastTime)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            LastTime = lastTime;
        }

        public PagedDataRequest(int pageNumber, int pageSize, DateTimeOffset lastTime, string keyword)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            LastTime = lastTime;
            Keyword = keyword;
        }

        public PagedDataRequest(int pageNumber, int pageSize, int orderId)
        {
            PageNumber = pageNumber;
            PageSize = pageSize;
            this.orderId = orderId;
        }

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public DateTimeOffset LastTime { get; set; }        
        public string Keyword { get; set; }
		public int orderId { get; set; }
        public string OrderType { get; set; }
        public string NameToSort { get; set; }
    }
}