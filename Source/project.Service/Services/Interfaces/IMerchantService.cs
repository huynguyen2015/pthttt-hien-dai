﻿using project.Data.PagedDataRequest;
using project.Service.Models;
using project.Service.Models.Merchant;

namespace project.Service.Services.Interfaces
{
    public interface IMerchantService
    {
        MerchantModel GetById(int memberId);
        bool Insert(MerchantModel model);
        bool Update(MerchantModel model);
        bool Delete(MerchantModel model);
        PagedDataInquiryResponse<MerchantModel> FilterMerchant(MerchantDataRequest dataRequest);
    }
}