// <auto-generated />
namespace project.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Update20170216 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Update20170216));
        
        string IMigrationMetadata.Id
        {
            get { return "201702161339473_Update-2017-02-16"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
