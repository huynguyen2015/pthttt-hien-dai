﻿using System;

namespace project.Data.Entities
{
    public class SysAccountSession : BaseEntity
    {
        public virtual long Id { get; set; }

        public virtual string SessionToken { get; set; }        

        public virtual DateTime ExpirationAt { get; set; }

        public virtual long AccountId { get; set; }

        public virtual SysAccount Account { get; set; }
    }
}