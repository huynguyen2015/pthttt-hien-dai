﻿using log4net;
using System.Collections.Specialized;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using project.Common.Logging;
using project.Web.Common;

namespace project.Web.Api.Filter
{
    public class ValidateMultiPartForm : ActionFilterAttribute
    {
        protected readonly ILog _log;
        public ValidateMultiPartForm()
            : this(WebContainerManager.Get<ILogManager>())
        {
        }
        public ValidateMultiPartForm(ILogManager logManager)
        {
            _log = logManager.GetLog(typeof(ValidateMultiPartForm));
        }
        public override bool AllowMultiple
        {
            get { return false; }
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Content.IsMimeMultipartContent())
                throw new HttpRequestException("Request not is Mime Multipart");
            base.OnActionExecuting(actionContext);
        }
        protected NameValueCollection ValueCollection { get; private set; }        
    }
}