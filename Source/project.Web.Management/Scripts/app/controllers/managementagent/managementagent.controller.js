/**
 * @ngdoc overview
 * @name Member controller
 * @description 
 *
 */
(function() {
    function managementAgentController($scope, $uibModal, commonService, $location, httpService, apiRoutes, alertify) {
        // Declare variable        
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;

        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 6, pageSize: 20, currentItem: '0-20', totalItems: 120 };
            $scope.filter = {orderId: 1 , keyword: "" };
        }

        resetFilter();

        function filterAgents() {
            httpService.sendGet(apiRoutes.managementAgentFilter, $scope.filter, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];                    
                    $scope.paging = commonService.preparePagination(response);
                    console.log(response);
                }
            });
        }

        filterAgents();
      
        $scope.sortBy = function (type) {
            $scope.filter.filter = true;
            $scope.filter.name2Sort = type;
            $scope.filter.orderType = $scope.filter.orderType == orderType.ASC ? orderType.DESC : orderType.ASC;

            filterAgents();
        }      

        $scope.searchPageChange = function (flag, pageNumber) {          
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1; 
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterAgents();
        };
      
        $scope.searchClick = function () {            
            filterAgents();
        };

        $scope.deleteClick = function (agentId) {
            alertify.confirm("Bạn thực sự muốn xóa đại lý này?", function () {
                httpService.sendPost(apiRoutes.managementAgentDelete.replace('{agentId}', agentId), null, $scope.setContentLoading).then(function (response) {
                    filterAgents();
                });
            }, function () {

            });           
        }

        $scope.addClick = function () {
            commonService.openModal("_addagent.html", "AddOrUpdateAgentController", null, filterAgents);
        }
        $scope.updateClick = function (agent) {
            commonService.openModal("_addagent.html", "AddOrUpdateAgentController", agent, filterAgents);
        }
    }

    angular.module('app').controller('ManagementAgentController', ['$scope', '$uibModal', 'commonService', '$location', 'HttpService', 'API_ROUTES', 'alertify', managementAgentController]);
})();
