﻿
using System;

namespace travel.Service.Models
{
    public class BaseModel
    {
        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }
    }
}