namespace Click.Data.Entities
{
    using System;


    public class NotificationTypeModel
    {
        public short NotificationTypeId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        
    }
}