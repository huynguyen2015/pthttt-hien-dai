﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class SysAccountMap : BaseEntityMap<SysAccount>
    {
        public SysAccountMap() {
            ToTable("SYS_ACCOUNT");
            HasKey(o => o.Id);
            Property(o => o.AccountTypeId);
            Property(o => o.AccountId);
            Property(o => o.Username);
            Property(o => o.Email);            
            Property(o => o.Password);
            Property(o => o.Salt);
            Property(o => o.IsActive);
            Property(o => o.IsHash);
            Property(o => o.RoleId);

            HasRequired(o => o.AccountType).WithMany().WillCascadeOnDelete(false);
        }
    }
}