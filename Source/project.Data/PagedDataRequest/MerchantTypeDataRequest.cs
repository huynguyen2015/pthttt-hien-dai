﻿using System;

namespace project.Data.PagedDataRequest
{
    public class MerchantTypeDataRequest : PagedDataRequest
    {
        public MerchantTypeDataRequest()         
        {

        }
        public MerchantTypeDataRequest(int pageNumber, int pageSize)
            : base(pageNumber, pageSize)
        {
        
        }

        public MerchantTypeDataRequest(int pageNumber, int pageSize, DateTime lastTime)
            : base(pageNumber, pageSize, lastTime)
        {
        
        }


        public MerchantTypeDataRequest(int pageNumber, int pageSize, int orderId)
            : base(pageNumber, pageSize, orderId)
        {
           
        }

        public int OrderId { get; set; }
    }
}