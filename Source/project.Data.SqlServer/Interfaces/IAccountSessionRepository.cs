﻿using project.Data;
using project.Data.Entities;

namespace project.Repository.Intefaces
{
    public interface IAccountSessionRepository : IRepository<SysAccountSession>
    {        

    }
}