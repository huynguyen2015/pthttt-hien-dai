namespace Click.Data.Entities
{
    using System;


    public  class CurrencyModel
    {
      
        public short CurrencyId { get; set; }

        public string CurrencyName { get; set; }

        public string Symbol { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

    }
}
