/**
 * @ngdoc overview
 * @name Member controller
 * @description 
 *
 */
(function() {     
    var myApp = angular.module('app')
    myApp.controller('PlaceImageSearchController', ['$scope', '$uibModal', 'commonService', '$location', 'HttpService', 'API_ROUTES', 'ORDER_PLACEIMAGE',
    function ($scope, $uibModal, commonService, $location, httpService, apiRoutes, ORDER_PLACEIMAGE) {
        // Declare variable
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;
        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 0, pageSize: 0, currentItem: '0-20', totalItems: 0 };
            $scope.filter = { orderId: ORDER_PLACEIMAGE.idDesc, placeId: -1 };
        }

        $scope.options = {
            height: 150
        };

        resetFilter();

        $scope.place = {};

        function GetPlace() {
            var filter = { orderId: 1, name: "" };
            httpService.sendGet(apiRoutes.placeFilter, filter).then(function (response) {
                console.log(response);
                if (response && response.Success === true) {
                    $scope.place = response.Data ? response.Data.Items : [];
                }
            });
        };

        GetPlace();

        function filterPlacesImage() {
            console.log("a");
            httpService.sendGet(apiRoutes.placeImageFilter, $scope.filter, $scope.setContentLoading).then(function (response) {
                console.log(response);
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.paging = commonService.preparePagination(response);
                }
            });
        }

        filterPlacesImage();
        $scope.removedPlace = {};

        $scope.sortBy = function (type) {
            switch (type) {
                case "place":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACEIMAGE.placeAsc ? ORDER_PLACEIMAGE.placeDesc : ORDER_PLACEIMAGE.placeAsc;
                        break;
                    }
                case "id":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACEIMAGE.idAsc ? ORDER_PLACEIMAGE.idDesc : ORDER_PLACEIMAGE.idAsc;
                        break;
                    }
                case "createdat":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACEIMAGE.createdatAsc ? ORDER_PLACEIMAGE.createdatDesc : ORDER_PLACEIMAGE.createdatAsc;
                        break;
                    }               
            }
            filterPlacesImage();
        }
        $scope.getSortClass = function (type) {
            switch (type) {
                case "place":
                    {
                        return $scope.filter.orderId === ORDER_PLACEIMAGE.placeAsc ? "sorting_asc" : ($scope.filter.orderId === ORDER_PLACEIMAGE.placeDesc ? "sorting_desc" : "sorting");
                    }
                case "id":
                    {
                        return $scope.filter.orderId === ORDER_PLACEIMAGE.idAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACEIMAGE.idDesc ? "sorting_desc" : "sorting");
                    }
                case "createdat":
                    {
                        return $scope.filter.orderId === ORDER_PLACEIMAGE.createdatAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACEIMAGE.createdatDesc ? "sorting_desc" : "sorting");
                    }              
                default:
                    return '';
            }
        }

        $scope.searchPageChange = function (flag, pageNumber) {
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1;
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterPlacesImage();
        };

        $scope.searchClick = function () {
            //$scope.filter.pageNumber = 1;
            console.log($scope.filter.placeId);
            filterPlacesImage();
        }

        $scope.showPopup = function (place) {
            commonService.openModal("placeimagemodal.html", "PlaceImageModificationController", place, filterPlacesImage);
        }

        $scope.removeItem = function (item) {
            $scope.removedPlace = item;
        }

        $scope.deletePlace = function () {
            console.log("Delete place image with url:" + $scope.removedPlace.Url);

            httpService.sendPost(apiRoutes.placeImageDelete, $scope.removedPlace, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    filterPlacesImage();
                }
            });
            return true;
        }
    }]);
})();
