﻿using project.Data.PagedDataRequest;
using project.Service.Models;
using System.Collections.Generic;

namespace project.Service.Services.Interfaces
{
    public interface IAccountService
    {
        /// <summary>
        /// Huy Nguyen, 2016-10-24, Get member by id
        /// </summary>        
        /// <param name="memberId"></param>
        /// <returns></returns>  
        AccountModel GetById(int memberId);

        /// <summary>
        /// Huy Nguyen, 2016-10-24, insert a member
        /// </summary>
        /// <param name="model"></param>        
        /// <returns></returns>
        AccountModel Insert(AccountModel model);

        /// <summary>
        /// Huy Nguyen, 2016-10-24, update a member
        /// </summary>
        /// <param name="model"></param>        
        /// <returns></returns>
        bool Update(AccountModel model);

        /// <summary>
        /// Huy Nguyen, 2016-10-24, delete a member
        /// </summary>
        /// <param name="pagedDataRequest"></param>
        /// <returns></returns>
        bool Delete(AccountModel model);

        /// <summary>
        /// Huy Nguyen, 2016-10-24, Filter member
        /// </summary>
        /// <param name="pagedDataRequest"></param>        
        /// <returns></returns>
        PagedDataInquiryResponse<AccountModel> FilterMember(PagedDataRequest pagedDataRequest);        

        /// <summary>
        /// Huy Nguyen, 2016-10-26, Login
        /// </summary>        
        /// <param name="model"></param>
        /// <param name="requiredRoles"></param>
        /// <returns></returns>  
        AccountModel Login(LoginModel model, List<short> requiredRoles);

        /// <summary>
        /// Huy Nguyen, 2016-10-26, Logout
        /// </summary>        
        /// <param name="model"></param>
        /// <returns></returns>  
        bool Logout(long memberId, long sessionTokenId, short deviceTypeId);     
    }
}
