﻿using project.Service.Services.Interfaces;
using project.Web.Api.Commons.Helper;

namespace project.Web.Api.Controllers.v1.Interfaces
{
    public interface IAgentControllerDependencyBlock : IControllerDependencyBlock
    {
        IAgentService AgentService { get; }
        IParseRequestParams ParseRequestParams { get; }
    }
}