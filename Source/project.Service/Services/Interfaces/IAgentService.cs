﻿using project.Data.PagedDataRequest;
using project.Service.Models;
using project.Service.Models.Agent;

namespace project.Service.Services.Interfaces
{
    public interface IAgentService
    {
        AgentModel GetById(int memberId);
        AgentModel Insert(AgentModel model);
        bool Update(AgentModel model);       
        bool Delete(long agentId);
        PagedDataInquiryResponse<AgentModel> FilterAgent(AgentDataRequest dataRequest);
        PagedDataInquiryResponse<AgentModel> FilterAgent_NoParams(AgentDataRequest dataRequest);
    }
}