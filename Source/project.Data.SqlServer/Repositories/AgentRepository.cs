﻿using project.Common;
using project.Common.Exceptions;
using project.Common.Logging;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace project.Repository.Repositories
{
    public class AgentRepository : EfRepository<TblAgent>, IAgentRepository
    {
        public AgentRepository(TravelContext context, IDateTime datetime, ILogManager logManager)
            : base(context, datetime, logManager)
        {
        }

        public QueryResult<TblAgent> FilterAgent(AgentDataRequest dataRequest)
        {
            try
            {
                // Todo: defined Error code
                var startIndex = ResultsPagingUtility.CalculateStartIndex(
                dataRequest.PageNumber, dataRequest.PageSize);

                //Parameter for get list
                var query = Table.Where(obj => obj.StatusId == Constants.StatusIds.Active);

                if (!string.IsNullOrEmpty(dataRequest.AgentName))
                    query = query.Where(obj => obj.AgentName.Contains(dataRequest.AgentName));

                if (!string.IsNullOrEmpty(dataRequest.AgentCode))
                    query = query.Where(obj => obj.AgentCode.Contains(dataRequest.AgentCode));

                if (dataRequest.CreatedFrom != null && DateTime.MinValue != dataRequest.CreatedFrom)
                    query = query.Where(obj => DbFunctions.TruncateTime(obj.CreatedAt) >= DbFunctions.TruncateTime(dataRequest.CreatedFrom));

                if (dataRequest.CreatedFrom != null && DateTime.MinValue != dataRequest.CreatedFrom)
                    query = query.Where(obj => DbFunctions.TruncateTime(obj.CreatedAt) >= DbFunctions.TruncateTime(dataRequest.CreatedFrom));

                if (dataRequest.CreatedTo != null && DateTime.MinValue != dataRequest.CreatedTo)
                    query = query.Where(obj => DbFunctions.TruncateTime(obj.CreatedAt) <= DbFunctions.TruncateTime(dataRequest.CreatedTo));

                if (dataRequest.orderId == 1)
                    query = query.OrderBy(c => c.Id);
                else if (dataRequest.orderId == 2)
                    query = query.OrderByDescending(c => c.Id);
                       
                var totalCount = query.Any() ? query.Count() : 0;
                var results = query.Skip(startIndex).Take(dataRequest.PageSize).ToList();
                return new QueryResult<TblAgent>(results, totalCount, dataRequest.PageSize);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not filter member. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, ex.ToString());
            }
        }

        public QueryResult<TblAgent> FilterAgent_NoParams(AgentDataRequest dataRequest)
        {
            try
            {
                // Todo: defined Error code
                var startIndex = ResultsPagingUtility.CalculateStartIndex(
                dataRequest.PageNumber, dataRequest.PageSize);

                //Parameter for get list
                var query = Table.Where(obj => obj.StatusId == Constants.StatusIds.Active);                

                if (dataRequest.orderId == 1)
                    query = query.OrderBy(c => c.Id);
                else if (dataRequest.orderId == 2)
                    query = query.OrderByDescending(c => c.Id);

                var totalCount = query.Any() ? query.Count() : 0;
                var results = query.Skip(startIndex).Take(dataRequest.PageSize).ToList();
                return new QueryResult<TblAgent>(results, totalCount, dataRequest.PageSize);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not filter member. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, ex.ToString());
            }
        }      
    }
}