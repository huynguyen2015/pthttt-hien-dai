﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace project.Common.Serialization
{
    public class SerializationHelper : ISerialization
    {
        public T DeserializeJsonToObject<T>(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
                throw new ArgumentNullException("Can not deserialize " + jsonString);

            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        public string SerializeObjectToJson(object obj)
        {
            if (obj == null)
                throw new ArgumentNullException("Can not serialize obj");
            return JsonConvert.SerializeObject(obj);
            //return JObject.FromObject(obj).ToString();
        }

        public T ToObject<T>(object obj)
        {
            return JObject.FromObject(obj).ToObject<T>();
        }

        public int SerializeArrayToInteger(IEnumerable<byte> list, short range, short maxValue)
        {
            if (!list.Any())
                return 0;
            if (list.Count() > range)
                throw new ArgumentOutOfRangeException(string.Format("The list over {0}", range));
            var result = 0;
            foreach (var x in list)
            {
                if (x > maxValue)
                    throw new ArgumentOutOfRangeException(string.Format("value over {0}", maxValue));
                result = result + (int)Math.Pow(2, x - 1);
            }
            return result;
            
        }

        public IEnumerable<byte> DeserializeIntegerToArray(int value)
        {
            if (value == 0)
                return new byte[0];
            var list = Convert.ToString(value, 2).ToList(); //Convert to binary in a string
            var result = new List<byte>();
            var totalCount = (byte)list.Count();
            for (var i = totalCount; i >= 1; i--)
            {
                if (list[i - 1] == '1')
                {
                    result.Add((byte)(totalCount - i + 1));
                }
            }
            return result;
        }

        public IEnumerable<T> DeserializeJArrayToIEnumerable<T>(JArray obj)
        {
            return obj.ToObject<IEnumerable<T>>();
        }

        public string SerializeIEnumerableToJson(IEnumerable<object> list)
        {
           
            return JsonConvert.SerializeObject(list);
        }

        public IEnumerable<T> DeserializeJsonToIEnumerable<T>(string json)
        {
            return JsonConvert.DeserializeObject<IEnumerable<T>>(json);
        }

        public dynamic SerializeToDynamic<T>(T obj)
        {
            IDictionary<string, Object> expando = new ExpandoObject();
            foreach (var property in obj.GetType().GetProperties())
            {
                expando.Add(property.Name.ToUpper(), property.GetValue(obj));
            }
            return (dynamic)expando;
        }

    }
}