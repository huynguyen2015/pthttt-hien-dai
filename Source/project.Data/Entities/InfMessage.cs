namespace project.Data.Entities
{
    public class InfMessage : BaseEntity
    {
        public virtual long Id { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public bool IsRead { get; set; }
        public long FromAccountId { get; set; }
        public long ToAccountId { get; set; }
        public long ParentId { get; set; }
        
        public virtual SysAccount FromAccount { get; set; }
        public virtual SysAccount ToAccount { get; set; }
    }
}
