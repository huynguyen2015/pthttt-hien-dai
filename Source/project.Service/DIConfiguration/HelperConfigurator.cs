﻿using project.Common;
using project.Common.Exceptions;
using project.Common.Resources;
using project.Common.Security;
using project.Common.Serialization;
using project.Web.Common;
using project.Web.Common.ActionHelper;
using project.Web.Common.ErrorHandling;
using project.Web.Common.Logging;
using project.Web.Common.Security;
using Ninject;

namespace project.Service.DIConfiguration
{
    public static class HelperConfigurator
    {
        public static void Configure(IKernel containerBuilder)
        {
            containerBuilder.Bind<IDateTime>().To<DateTimeAdapter>().InSingletonScope();
            containerBuilder.Bind<IDatabaseValueParser>().To<DatabaseValueParser>().InSingletonScope();
            containerBuilder.Bind<ICrypto>().To<Crypto>().InSingletonScope();            
            containerBuilder.Bind<IActionLogHelper>().To<ActionLogHelper>().InSingletonScope(); ;
            containerBuilder.Bind<IExceptionMessageFormatter>().To<ExceptionMessageFormatter>().InSingletonScope();
            containerBuilder.Bind<IActionExceptionHandler>().To<ActionExceptionHandler>().InSingletonScope();
            containerBuilder.Bind<IHttpHelper>().To<HttpHelper>().InSingletonScope();
            containerBuilder.Bind<IActionResultHelper>().To<ActionResultHelper>().InSingletonScope();
            containerBuilder.Bind<ISerialization>().To<SerializationHelper>().InSingletonScope();            
            containerBuilder.Bind<IExceptionHelper>().To<ExceptionHelper>().InSingletonScope();
            containerBuilder.Bind<IResourceManager>().To<ResourceManager>().InSingletonScope();
            containerBuilder.Bind<IFileHelper>().To<FileHelper>().InSingletonScope();
            containerBuilder.Bind<IClientRequestHelper>().To<ClientRequestHelper>().InSingletonScope();
            containerBuilder.Bind<IJwtIssuer>().To<JwtIssuer>().InSingletonScope();
            containerBuilder.Bind<IImage>().To<ImageHelper>().InSingletonScope();            
        }
    }
}
