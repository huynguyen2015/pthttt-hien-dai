﻿/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function() {
    function placeModificationController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params) {        
        $scope.data = {
            Id: -1,
            StatusId: -1,
            ApprovalDate: '',
            CloseDate: '',
            FistActiveDate: '',
            LastActiveDate:'',
            MerchantNumber: '',
            BackendProcessor: 1,
            MerchantName: '',
            Status: 1,
            Owner: '',
            Address: '',
            City: '',
            State: '',
            Zip: '',
            Phone: '',
            Fax: '',
            Email: '',
            BankCardDBA: '',
            AgentId: -1,
            MerchantTypeId: -1,
            RegionId: -1,
            CreatedAt: '',
            CreatedUser: '',
            UpdatedAt: '',
            UpdatedUser: ''
        }               

        $scope.status = [
            {
                Id: 1,
                Name: 'Hoạt động'
            },
            {
                Id: 2,
                Name: 'Khóa'
            }
        ];

        $scope.region = {};
        $scope.merchantType = {};
        $scope.agent = {};
        var oldAgentId = -1;
        var oldMerchantTypeId = -1;
        var oldRegionId = -1;
        var oldStatusId = -1;

        //Get data region and merchant type
        function GetCombobox() {
            var filter = { orderId: 1 };
            httpService.sendGet("api/v1/region/filter", filter).then(function (response) {
                if (response && response.Success === true) {
                    $scope.region = response.Data ? response.Data.Items : [];
                }
            });

            httpService.sendGet("api/v1/merchanttype/filter", filter).then(function (response) {
                if (response && response.Success === true) {
                    $scope.merchantType = response.Data ? response.Data.Items : [];
                }
            });

            httpService.sendGet("api/v1/agent/filternoparams", filter).then(function (response) {
                if (response && response.Success === true) {
                    $scope.agent = response.Data ? response.Data.Items : [];
                    console.log($scope.agent);
                }
            });
        };

        GetCombobox();

        $scope.init = function () {
            if (!params) {
                return;
            }
          
            $scope.data = params;
            oldAgentId = $scope.data.AgentId;
            oldMerchantTypeId = $scope.data.MerchantTypeId;
            oldRegionId = $scope.data.RegionId;
            oldStatusId = $scope.data.StatusId;
            console.log(params);
        }
        
        $scope.changedStatus = function ()
        {
            if ($scope.data.StatusId == 1)
                $scope.data.CloseDate = "";
            else
                $scope.data.LastActiveDate = "";
        };

        $scope.closeClick = function () {
            $scope.data = params;
            $uibModalInstance.dismiss("cancel");
            //$scope.data.AgentId = oldAgentId;
            //$scope.data.MerchantTypeId = oldMerchantTypeId;
            //$scope.data.RegionId = oldRegionId;
            //$scope.data.StatusId = oldStatusId;
            
        }

        $scope.saveClick = function () {
            var result = validateDataBeforeSubmit();
            if (!isEmpty(result)) {
                alertify.logPosition("top right").error(result);                
                return;
            }
            var url = params ? "api/v1/merchant/update" : "api/v1/merchant/add"; // sao phai chia ra nhu vay?
            
            if (params) {
                Updated();
            } else {
                Inserted();
            }         
            console.log($scope.data.CloseDate);
            
            httpService.sendPost(url, $scope.data).then(function (response) {
                if (response && response.Success === true) {
                    $uibModalInstance.close("success");
                    console.log('aaaaaaaaaaaa');
                }
            });
        }

        function validateDataBeforeSubmit() {
            var errorMessage = '';
            var merchant = $scope.data;
            if (isEmpty(merchant.MerchantName))
                errorMessage = "Tên khách hàng không được bỏ trống";
            else if (isEmpty(merchant.MerchantNumber))
                errorMessage = "Mã số không được bỏ trống";
            else if (isEmpty(merchant.Owner))
                errorMessage = "Người chủ không được bỏ trống";
            else if (isEmpty(merchant.Address))
                errorMessage = "Địa chỉ không được bỏ trống";
            else if (isEmpty(merchant.City))
                errorMessage = "Thành phố không được bỏ trống";
            else if (isEmpty(merchant.State))
                errorMessage = "State không được bỏ trống";
            else if (isEmpty(merchant.Phone))
                errorMessage = "Điện thoại không được bỏ trống";
            else if (isEmpty(merchant.Zip))
                errorMessage = "Mã vùng không được bỏ trống";
            else if (isEmpty(merchant.Email))
                errorMessage = "Email không được bỏ trống";
            else if (isEmpty(merchant.BankCardDBA))
                errorMessage = "BankCardDBA không được bỏ trống";
            return errorMessage;
        }

        function Updated() {
            $scope.data.UpdatedAt = new Date();
            $scope.data.UpdatedUser = 1;
            if ($scope.data.StatusId == 1)
                $scope.data.CloseDate = "";
        }

        function Inserted() {
            $scope.data.model.CreatedAt = $scope.data.model.UpdatedAt = new Date();
            ////TODO: change logged in account id
            $scope.data.model.CreatedBy = $scope.data.model.UpdatedBy = 1;
        }
    }

    angular.module('app').controller('PlaceModificationController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify', 'params', placeModificationController]);
})();
