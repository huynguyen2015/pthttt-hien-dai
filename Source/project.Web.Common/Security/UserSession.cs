﻿using project.Common.Exceptions;
using System;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace project.Web.Common.Security
{
    public class UserSession : IWebUserSession
    {

        public long? MemberId
        {
            get
            {
                var temp = ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Sid);
                return temp == null ? null : (long?)Int64.Parse(temp.Value);
            }
        }

        public string Username
        {
            get
            {
                return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Name).Value;
            }
        }

        public string Email
        {
            get
            {
                return ((ClaimsPrincipal)HttpContext.Current.User).FindFirst(ClaimTypes.Email).Value;
            }
        }


        public long SessionTokenId
        {
            get
            {
                try
                {
                    return Int32.Parse(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("SessionTokenId").Value);
                }
                catch
                {
                    throw new AuthorizationException("Invalid session token");
                }
            }
        }

        public bool IsInRole(string roleName)
        {
            return HttpContext.Current.User.IsInRole(roleName);
        }

        public Uri RequestUri
        {
            get { return HttpContext.Current.Request.Url; }
        }

        public string HttpRequestMethod
        {
            get { return HttpContext.Current.Request.HttpMethod; }
        }

        public string ApiVersionInUse
        {
            get
            {
                const int versionIndex = 2;
                if (HttpContext.Current.Request.Url.Segments.Count() < versionIndex + 1)
                {
                    return string.Empty;
                }
                var apiVersionInUse = HttpContext.Current.Request.Url.Segments[versionIndex].Replace("/", string.Empty);
                return apiVersionInUse;
            }
        }


        public int LanguageId
        {
            get
            {
                return Convert.ToInt32(((ClaimsPrincipal)HttpContext.Current.User).FindFirst("LanguageId").Value);
            }
        }
    }
}