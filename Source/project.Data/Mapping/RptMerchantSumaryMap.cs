﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class RptMerchantSumaryMap : BaseEntityMap<RptMerchantSumary>
    {
        public RptMerchantSumaryMap() {
            ToTable("RPT_MERCHANT_SUMARY");
            HasKey(o => o.Id);
            Property(o => o.ReportDate);
            Property(o => o.MerchantId);
            Property(o => o.SaleAmount);            
            Property(o => o.SaleCount);

            Property(o => o.ReturnAmount);
            Property(o => o.ReturnCount);
            Property(o => o.TransactionAmount);
            Property(o => o.TransactionCount);
            Property(o => o.KeyAmount);
            Property(o => o.KeyedCount);
            Property(o => o.ForeignCardAmount);
            Property(o => o.ForeignCardCount);
            Property(o => o.DebitAmount);
            Property(o => o.DebitCount);
            Property(o => o.VisaNetAmount);
            Property(o => o.VisaTransactionCount);
            Property(o => o.MasterCardNetAmount);
            Property(o => o.MasterCardTransactionCount);
            Property(o => o.AmericanExpressNetAmount);
            Property(o => o.AmericanExpressTransactionCount);
            Property(o => o.DiscoverNetAmount);
            Property(o => o.DiscoverNetCount);
            Property(o => o.DebitCardNetAmount);
            Property(o => o.DebitCardTransactionCount);
            Property(o => o.OtherNetAmount);
            Property(o => o.OtherTransactionCount);

            HasRequired(o => o.Merchant).WithMany().WillCascadeOnDelete(false);
        }
    }
}