namespace Click.Data.Entities
{
    using System;
    
    public class DeviceModel
    {
        public long DeviceId { get; set; }

        public long MemberId { get; set; }

        public short DeviceTypeId { get; set; }

        public string DeviceTokenId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short StatusId { get; set; }

        public string DeviceCode { get; set; }

    }
}
