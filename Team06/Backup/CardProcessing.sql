USE [master]
GO
/****** Object:  Database [CardProcessing]    Script Date: 11/21/2016 1:35:35 PM ******/
CREATE DATABASE [CardProcessing]
 
GO
USE [CardProcessing]
GO
/****** Object:  Table [dbo].[SYS_Account]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_Account](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[UserType] [int] NOT NULL,
	[Username] [char](100) NULL,
	[Password] [nvarchar](200) NULL,
	[AccountTypeId] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_Account_Type]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Account_Type](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](200) NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Account_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_Role]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](100) NULL,
	[Description] [nvarchar](512) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_Role_Detail]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_Role_Detail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleDetailName] [nvarchar](200) NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Role_Detail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_Agent]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Agent](
	[Id] [bigint] NOT NULL,
	[AgentCode] [char](30) NOT NULL,
	[AgentName] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Agent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_Merchant]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_Merchant](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumber] [char](30) NOT NULL,
	[BackendProcessor] [bigint] NULL,
	[MerchantName] [nvarchar](512) NOT NULL,
	[Status] [int] NOT NULL,
	[Owner] [nchar](10) NOT NULL,
	[Address1] [nvarchar](512) NULL,
	[Address2] [nvarchar](512) NULL,
	[Address3] [nvarchar](512) NULL,
	[City] [nvarchar](512) NULL,
	[State] [nvarchar](512) NULL,
	[Zip] [char](20) NULL,
	[Phone] [char](20) NULL,
	[Fax] [char](20) NULL,
	[Email] [char](50) NULL,
	[ApprovalDate] [datetime] NULL,
	[CloseDate] [datetime] NULL,
	[BankCardDBA] [char](50) NOT NULL,
	[FistActiveDate] [datetime] NULL,
	[LastActiveDate] [datetime] NULL,
	[AgentId] [bigint] NULL,
	[MerchantTypeId] [bigint] NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Mechant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MerchantSummary]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MerchantSummary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[MerchantNumber] [bigint] NOT NULL,
	[SaleAmount] [decimal](18, 4) NULL,
	[SalseCount] [decimal](18, 4) NULL,
	[ReturnAmount] [decimal](18, 4) NULL,
	[ReturnCount] [decimal](18, 4) NULL,
	[TransactionCount] [decimal](18, 4) NULL,
	[KeyAmount] [decimal](18, 4) NULL,
	[KeyedCount] [decimal](18, 4) NULL,
	[ForeignCardAmount] [decimal](18, 4) NULL,
	[ForeignCardCount] [decimal](18, 4) NULL,
	[DebitAmount] [decimal](18, 4) NULL,
	[DebitCount] [decimal](18, 4) NULL,
	[VisaNetAmount] [decimal](18, 4) NULL,
	[VisaTransactionCount] [decimal](18, 4) NULL,
	[MasterCardNetAmount] [decimal](18, 4) NULL,
	[MasterCardTransactionCount] [decimal](18, 4) NULL,
	[AmericanExpressNetAmount] [decimal](18, 4) NULL,
	[AmericanExpressTransactionCount] [decimal](18, 4) NULL,
	[DiscoverNetAmount] [decimal](18, 4) NULL,
	[DebitCardNetAmount] [decimal](18, 4) NULL,
	[DebitCardTransactionCount] [decimal](18, 4) NULL,
	[OtherNetAmount] [decimal](18, 4) NULL,
	[OtherTransactionCount] [decimal](18, 4) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MerchantSummary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_MerchantType]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MerchantType](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantTypeName] [nvarchar](200) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MerchantType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_TransactionDetail]    Script Date: 11/21/2016 1:35:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TransactionDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[FileSource] [nvarchar](200) NULL,
	[BatchNumber] [bigint] NOT NULL,
	[TerminalNumber] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[TransactionCode] [char](30) NOT NULL,
	[CardTypeCode] [char](30) NULL,
	[TransactionAmount] [bigint] NULL,
	[TransactionDate] [date] NULL,
	[TransactionTime] [time](7) NULL,
	[KeyedEntry] [bit] NOT NULL,
	[AuthorizationNumber] [char](30) NULL,
	[ReportTime] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
	[AccountNumber] [char](30) NULL,
	[FirstTwelveAccountNumber] [nchar](16) NULL,
	[CountryCode] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_TransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SYS_Account] ADD  CONSTRAINT [DF_TBL_Account_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_Account] ADD  CONSTRAINT [DF_TBL_Account_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_Account_Type] ADD  CONSTRAINT [DF_SYS_Account_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_Account_Type] ADD  CONSTRAINT [DF_SYS_Account_Type_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_Role] ADD  CONSTRAINT [DF_SYS_Role_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_Role] ADD  CONSTRAINT [DF_SYS_Role_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_Role_Detail] ADD  CONSTRAINT [DF_SYS_Role_Detail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_Role_Detail] ADD  CONSTRAINT [DF_SYS_Role_Detail_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_Agent] ADD  CONSTRAINT [DF_TBL_Agent_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TBL_Agent] ADD  CONSTRAINT [DF_TBL_Agent_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_Agent] ADD  CONSTRAINT [DF_TBL_Agent_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_Merchant] ADD  CONSTRAINT [DF_TBL_Merchant_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TBL_Merchant] ADD  CONSTRAINT [DF_TBL_Merchant_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_Merchant] ADD  CONSTRAINT [DF_TBL_Merchant_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_MerchantSummary] ADD  CONSTRAINT [DF_TBL_MerchantSummary_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_MerchantSummary] ADD  CONSTRAINT [DF_TBL_MerchantSummary_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_MerchantType] ADD  CONSTRAINT [DF_TBL_MerchantType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_MerchantType] ADD  CONSTRAINT [DF_TBL_MerchantType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_TransactionDetail] ADD  CONSTRAINT [DF_TBL_TransactionDetail_KeyedEntry]  DEFAULT ((0)) FOR [KeyedEntry]
GO
ALTER TABLE [dbo].[TBL_TransactionDetail] ADD  CONSTRAINT [DF_TBL_TransactionDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_TransactionDetail] ADD  CONSTRAINT [DF_TBL_TransactionDetail_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
USE [master]
GO
ALTER DATABASE [CardProcessing] SET  READ_WRITE 
GO
