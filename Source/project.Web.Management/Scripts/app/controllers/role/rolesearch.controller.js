/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function () {
    function roleSearchController($scope, $uibModal, $filter, commonService, $location, httpService, orderRole, apiRoutes, alertify) {
        // Declare variable        
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;
        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 6, pageSize: 20, currentItem: '0-20', totalItems: 120 };
            $scope.filter = { orderId: orderRole.nameAsc, keyword: "" };
        }

        resetFilter();

        function filterRoles() {
            debugger
            httpService.sendGet(apiRoutes.roleFilter, $scope.filter, $scope.setContentLoading).then(function (response) {                
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.paging = commonService.preparePagination(response);
                }
            });
        }
       
        filterRoles();        

        $scope.sortBy = function (type) {
            switch (type) {
                case "name":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderRole.nameAsc ? orderRole.nameDesc : orderRole.nameAsc;
                        break;
                    }
                case "status":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderRole.statusAsc ? orderRole.statusDesc : orderRole.statusAsc;
                        break;
                    }
                case "createdat":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderRole.createdatAsc ? orderRole.createdatDesc : orderRole.createdatAsc;
                        break;
                    }
                case "updatedat":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === orderRole.updatedatAsc ? orderRole.updatedatDesc : orderRole.updatedatAsc;
                        break;
                    }                
            }
            filterRoles();
        }
        $scope.getSortClass = function (type) {
            switch (type) {
                case "name":
                    {
                        return $scope.filter.orderId === orderRole.fullnameAsc ? "sorting_asc" : ($scope.filter.orderId === orderRole.fullnameDesc ? "sorting_desc" : "sorting");
                    }
                case "status":
                    {
                        return $scope.filter.orderId === orderRole.idAsc ? 'sorting_asc' : ($scope.filter.orderId === orderRole.idDesc ? "sorting_desc" : "sorting");
                    }
                case "createdat":
                    {
                        return $scope.filter.orderId === orderRole.createdatAsc ? 'sorting_asc' : ($scope.filter.orderId === orderRole.createdatDesc ? "sorting_desc" : "sorting");
                    }
                case "updatedat":
                    {
                        return $scope.filter.orderId === orderRole.updatedatAsc ? 'sorting_asc' : ($scope.filter.orderId === orderRole.updatedatDesc ? "sorting_desc" : "sorting");
                    }                
                default:
                    return '';
            }
        }       
        $scope.searchPageChange = function (flag, pageNumber) {
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1;
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterRoles();
        };
        $scope.searchClick = function () {
            $scope.filter.pageNumber = 1;
            filterRoles();
        }
        $scope.createOrEditClick = function (role) {
            commonService.openModal("_addrolepopup.html", "RoleAddPopupController", role, filterRoles);
        }
        $scope.removeClick = function (item) {
            var confirmMessage = $filter('translate')('messages.confirmMessage');
            alertify.confirm(confirmMessage, function () {
                httpService.sendPost(apiRoutes.roleRemove, item, $scope.setContentLoading).then(function (response) {
                    if (response && response.Success === true) {
                        filterRoles();
                    }
                });
            }, function () {

            });           
        }
    }

    angular.module('app').controller('RoleSearchController', ['$scope', '$uibModal', '$filter', 'commonService', '$location', 'HttpService', 'ORDER_ROLE', 'API_ROUTES'
        , 'alertify', roleSearchController]);
})();
