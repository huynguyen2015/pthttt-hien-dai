PTHTTT Hien Dai

Note: Moi nguoi checkout master ve, tao rieng cho ca nhan 1 nhanh va lam tren nhanh 
ca nhan. Lam xong phan nao thi merge vao master phan do

Vi du: minh ten Huy -> tao nhanh Huy-Branch
1- git checkout -b Huy-Branch

---- Thuc hien code tren Huy-Branch, xong dc 1 chuc nang, thuc hien update code len bitbuc
2- git add .
3- git commit [comment ve chuc nang vua xong]
4- git push -f origin Huy-Branch

---- Thuc hien merge code tu nhanh Huy-Branch sang master. Nham muc dich cho cac ban khac
---- lay code ve 
5- git checkout master
6- git pull origin master    
7- git merge Huy-Branch 		*** Luu y de bi conflict code, phai resolve 
					*** truoc khi push code len master

---- Thuc hien commit code len nhanh master bitbuck
8- git add .
9- git commit [comment]
10- git push origin master
