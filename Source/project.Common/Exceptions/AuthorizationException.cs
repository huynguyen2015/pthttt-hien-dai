﻿using System;

namespace project.Common.Exceptions
{
    [Serializable]
    public class AuthorizationException : Exception
    {
        public AuthorizationException(string message)
            : base(message)
        {
        }
    }
}