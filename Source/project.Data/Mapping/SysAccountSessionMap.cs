﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class SysAccountSessionMap : BaseEntityMap<SysAccountSession>
    {
        public SysAccountSessionMap(){
            ToTable("SYS_ACCOUNT_SESSION");
            HasKey(o => o.Id);
            Property(o => o.SessionToken);
            Property(o => o.ExpirationAt);
            Property(o => o.AccountId);

            HasRequired(o => o.Account).WithMany().WillCascadeOnDelete(false);
        }
    }
}