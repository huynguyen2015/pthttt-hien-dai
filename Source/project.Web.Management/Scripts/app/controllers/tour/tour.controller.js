/**
 * @ngdoc overview
 * @name Tour controller
 * @description 
 * @author khiettran
 *
 */

(function() {
    'use strict';
    angular.module('app').controller('TourController', tourcontroller);

    tourcontroller.$inject = ['$scope', '$uibModal', 'commonService'];

    function tourcontroller($scope, $uibModal, commonService) {

		var tables = ['tb-tour', 'tb-official', 'tb-cruise', 'tb-comment'];
        $scope.selectedButton = {
            el: '',
            placeholder:''
        }

        // show hide tables
    	$scope.showTable = function(id) {
            var el = $('#' + id);
            el.removeClass('hide');
            el.show()

            for (var i = tables.length - 1; i >= 0; i--) {
                if (tables[i] != id) 
                    $('#' + tables[i]).hide();
            }
    	}

        $scope.showPopup = function(tour) {
            commonService.openModal("/Views/tour/tourmodal.html", "TourModalController", {}, function () {});
        }
    }
})();
