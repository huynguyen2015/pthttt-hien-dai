﻿
namespace project.Web.Common.ActionHelper
{
    public interface IActionResultHelper
    {
        ActionResult WrapActionResult(bool isSuccess, string message, object data);
    }
}
