﻿using project.Data.Entities.StoreModel;
using project.Data.PagedDataRequest;
using project.Service.Models;
using project.Service.Models.Message;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.Services.Interfaces
{
    public interface IMessageService
    {
        int InsertMessageService(MessageModel value);

        int DeleteMessageService(MessageModel value);

        List<EmailModelReturn> GetListEmail(MessageModel value);

        PagedDataInquiryResponse<MessageModel> FilterMessage(MessageDataRequest dataRequest);
        PagedDataInquiryResponse<MessageModel> FilterMessage_NoParams(MessageDataRequest dataRequest);
    }
}