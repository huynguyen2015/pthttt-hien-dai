/**
 * @ngdoc overview
 * @name Member controller
 * @description 
 *
 */
(function() {     
    var myApp = angular.module('app')
    myApp.controller('PlaceDetailSearchController', ['$scope', '$uibModal', 'commonService', '$location', 'HttpService', 'API_ROUTES', 'ORDER_PLACE',
    function ($scope, $uibModal, commonService, $location, httpService, apiRoutes, ORDER_PLACE) {
        // Declare variable
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;
        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 0, pageSize: 0, currentItem: '0-20', totalItems: 0 };
            $scope.filter = { orderId: ORDER_PLACE.idDesc, name: "" };
        }

        resetFilter();

        function filterPlaces() {
            console.log("a");
            httpService.sendGet(apiRoutes.placeFilter, $scope.filter, $scope.setContentLoading).then(function (response) {
                console.log(response);
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.paging = commonService.preparePagination(response);
                }
            });
        }

        filterPlaces();
        $scope.removedPlace = {};

        $scope.sortBy = function (type) {
            switch (type) {
                case "title":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.titleAsc ? ORDER_PLACE.titleDesc : ORDER_PLACE.titleAsc;
                        break;
                    }
                case "id":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.idAsc ? ORDER_PLACE.idDesc : ORDER_PLACE.idAsc;
                        break;
                    }
                case "description":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.descriptionAsc ? ORDER_PLACE.descriptionDesc : ORDER_PLACE.descriptionAsc;
                        break;
                    }
                case "rating":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.ratingAsc ? ORDER_PLACE.ratingDesc : ORDER_PLACE.ratingAsc;
                        break;
                    }
                case "likenumber":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.likenumberAsc ? ORDER_PLACE.likenumberDesc : ORDER_PLACE.likenumberAsc;
                        break;
                    }
                case "placecategory":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.placecategoryAsc ? ORDER_PLACE.placecategoryDesc : ORDER_PLACE.placecategoryAsc;
                        break;
                    }
                case "createdat":
                    {
                        $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACE.createdatAsc ? ORDER_PLACE.createdatDesc : ORDER_PLACE.createdatAsc;
                        break;
                    }               
            }
            filterPlaces();
        }

        $scope.getSortClass = function (type) {
            switch (type) {
                case "title":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.titleAsc ? "sorting_asc" : ($scope.filter.orderId === ORDER_PLACE.titleDesc ? "sorting_desc" : "sorting");
                    }
                case "id":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.idAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACE.idDesc ? "sorting_desc" : "sorting");
                    }
                case "description":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.descriptionAsc ? "sorting_asc" : ($scope.filter.orderId === ORDER_PLACE.descriptionDesc ? "sorting_desc" : "sorting");
                    }
                case "rating":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.ratingAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACE.ratingDesc ? "sorting_desc" : "sorting");
                    }
                case "likenumber":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.likenumberAsc ? "sorting_asc" : ($scope.filter.orderId === ORDER_PLACE.likenumberDesc ? "sorting_desc" : "sorting");
                    }
                case "placecategory":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.placecategoryAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACE.placecategoryDesc ? "sorting_desc" : "sorting");
                    }
                case "createdat":
                    {
                        return $scope.filter.orderId === ORDER_PLACE.createdatAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACE.createdatDesc ? "sorting_desc" : "sorting");
                    }              
                default:
                    return '';
            }
        }

        $scope.searchPageChange = function (flag, pageNumber) {
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1;
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterPlaces();
        };

        $scope.searchClick = function () {
            //$scope.filter.pageNumber = 1;
            filterPlaces();
        }

        $scope.showPopup = function (place) {
            commonService.openModal("placedetailmodal.html", "PlaceDetailModificationController", place, filterPlaces);
        }

        $scope.removeItem = function (item) {
            $scope.removedPlace = item;
        }

        $scope.deletePlace = function () {
            console.log("Delete place with Name:" + $scope.removedPlace.Name);

            httpService.sendPost(apiRoutes.placeCategoryDelete, $scope.removedPlace, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    filterPlaces();
                }
            });
            return true;
        }
    }]);
})();
