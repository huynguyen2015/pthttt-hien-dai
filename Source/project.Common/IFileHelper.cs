﻿using System.IO;

namespace project.Common
{
    public interface IFileHelper
    {
        FileStream Open(string path, FileMode mode);
    }
}
