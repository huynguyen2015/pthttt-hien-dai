namespace Click.Data.Entities
{
    using System;

    public  class CommentModel
    {
        public long CommentId { get; set; }

        public string Message { get; set; }

        public long MemberId { get; set; }

        public short StatusId { get; set; }

        public long ParentId { get; set; }

        public short ParentTypeId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        

        

        

        

        
    }
}
