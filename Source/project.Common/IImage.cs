﻿using System.Drawing;
using System.IO;

namespace project.Common
{
    public interface IImage
    {
        Size ProportionSize(Size sizeOriginal, Size sizeChanged);

        Bitmap GetThumbnailBitmap(Stream stream, Size sizeChanged);

        Stream GetThumbnailImageStream(Stream stream, Size sizeChanged);
        Stream ResizeStream(Stream stream, Size sizeChanged);
    }
}
