﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Data.Entities.StoreModel
{
    public class EmailModel : BaseEntity
    {
        public long Id { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string FromName { get; set; }
        public string ToName { get; set; }
        public long ParentId { get; set; }
        public DateTime CreateAt { get; set; }
        public string StrCreateAt { get; set; }
    }

    public class EmailModelReturn
    {
        public long id { get; set; }
        public string subject { get; set; }
        public string from { get; set; }
        public string avatar { get; set; }
        public ToEmailModel to { get; set; }
        public string content { get; set; }
        public string date { get; set; }
        public string label { get; set; }
        public string fold
        {
            get;
            set;
        }

        public EmailModelReturn()
        {
            label = string.Empty;
            fold = string.Empty;
            avatar = "../../Content/logo.png";
        }
    }

    public class ToEmailModel
    {
        public string name { get; set; }
        public string email { get; set; }
    }
}