﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using project.Web.Common.ActionHelper;

namespace project.Web.Common.ErrorHandling
{
    public class SimpleErrorResult : IHttpActionResult
    {
        private readonly string _errorMessage;
        private readonly HttpRequestMessage _requestMessage;
        private readonly int _statusCode;

        public SimpleErrorResult(HttpRequestMessage requestMessage, int statusCode,
            string errorMessage)
        {
            _requestMessage = requestMessage;
            _statusCode = statusCode;
            _errorMessage = errorMessage;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            //return Task.FromResult(_requestMessage.CreateErrorResponse(_statusCode, _errorMessage));
            return Task.FromResult(_requestMessage.CreateResponse(
                HttpStatusCode.OK,
                new ActionResult
                {
                    ErrorCode = _statusCode,
                    Success = false,
                    Message = _errorMessage
                }));

        }
    }
}