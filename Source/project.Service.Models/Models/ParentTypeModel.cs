namespace Click.Data.Entities
{
    using System;


    public class ParentTypeModel
    {
        public short ParentTypeId { get; set; }

        public string ParentName { get; set; }
        
    }
}