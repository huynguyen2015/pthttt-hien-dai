var translationsEN = {
    app_name: 'Admin Managerment',
    menu: {
        dashboard: 'Dashboard',
        member: {
            member: 'Member',
            profile: 'Profile',
            role: 'Role',
            permission: 'Permission'
        },
        post: {
            post: 'Post'
        },
        place: {
            place: 'Place'
        },
        /* TRAN TUNG - Vehicle menu*/
        vehicle: {
            vehicle: 'Vehicle',
            details: 'Details',
            category: 'Category',
            company: 'Company'
        }
        /* TRAN TUNG - Vehicle menu*/
    },
    page: {
        dashboard: {
            title: 'Dashboard'
        },
        member: {
            detail: {
                tabname: {
                    dashboard: 'Dashboard',
                    linked_account: 'Linked Account',
                    account: 'Account',
                    purchase: 'Purchase',
                    photos_video: 'Photos & Video',
                    posts: 'Posts',
                    comments: 'Comments',
                    communication_with_admin: 'Communication with admin',
                    reported: 'Reported'
                }
            },
            search: {
                title: "Search",
                username: "Username",
                placeholderusernameoremail: "Who",
                joinedfrom: "Joined from",
                to: "to",
                searchresults: "Search results: ",
                shortednumber: "No.",
                fullname: "Full name",
                id: "Id",
                age: "Age",
                location: "Location",
                id: "Id",
                status: "Status",
                joined: "Joined",
                lastlogin: "Last login",
                role: "Role",
                checked: "Checked",
                sendmessage: "Send message",
                /*Khiet Tran: Added - 20/9/2016 */
                email: 'Email',
                FailedLoginCount: 'Failed Count',
                createBy: 'Create By',
                createAt: 'Create At',
                updateBy: 'Update By',
                updateAt: 'Update At',
                imageUrl: 'Image URL',
                birthday: 'Birthday',
                ssn: 'Social Security Number',
                ssn_short: 'SSN',
                phoneNumber: 'Phone Number',
                address: 'Address',
                lastOnline: 'Last Online',
                delete: 'Delete',
                update: 'Update',
                addorupdate: 'ADD NEW OR UPDATE ACCOUNT',
                refresh: 'Refresh',
                avatar: 'Photo'
                /*Khiet Tran: Added - 20/9/2016 */
            }
        },
        post: {
            queries: {
                title: "Queries",
                username: "Username",
                shortednumber: "No.",
                type: "Type",
                role: "Role",
                daterecieved: "Date Recieved",
                content: "Content",
                nooftask: "No of Tasks",
                reply: "Reply",
                addtask: "Add tasks",
                completeclose: "Complete & Close"
            }
        }, 
        role: {
            title: "Search",
            name: "Name",
            placeholdername: "Name",
            createdfrom: "Created from",
            to: "to",
            shortednumber: "No.",
            createdat: "Created at",
            updatedat: "Updated at",
            searchresults: "Search results: ",
            status: "Status",
            actions: "Actions",
            addroletitle: "Add Role",
            permissions: "Permissions",
        },
        permission: {
            title: "Search",
            name: "Name",
            placeholdername: "Name",
            createdfrom: "Created from",
            to: "to",
            shortednumber: "No.",
            createdat: "Created at",
            updatedat: "Updated at",
            searchresults: "Search results: ",
            status: "Status",
            actions: "Actions",
        },
        place: {
            search: {
                title: "Search",
                name: "Name",
                placeholdername: "Name",
                searchresults: "Search results: ",
                shortednumber: "No.",
                id: "Id",
                icon: "Icon base 64",
                createdat: "Created at",
                createdby: "Created by",
                updatedat: "Updated at",
                updatedby: "Updated by",
                action: "Action",
                addorupdate: 'ADD NEW OR UPDATE PLACE CATEGORY'
            }
        },
        /* TRAN TUNG - Vehicle menu*/ 
        vehicle: {
            
        },
        /* TRAN TUNG - Vehicle menu*/
        paging: {
            itemperpage: "showing {{pagesize}} of {{total}} items"
        }
    },
    btn: {
        search: "Search",
        cancel: "Cancel",
        save: "Save",
        ok: "ok",
        addrole: "Add Role",
        addpermission: "Add Permission",
        addaccount: 'Add Account',
        addprofile: 'Add Profile'
    },
    tour: {
        tour: 'Tour',
        title: 'Title',
        dayscount: 'Day count',
        description: 'Description',
        likenumber: 'Like Number'
    }
};