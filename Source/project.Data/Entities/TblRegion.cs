namespace project.Data.Entities
{

    public class TblRegion : BaseEntity
    {
        public virtual int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }       
    }
}
