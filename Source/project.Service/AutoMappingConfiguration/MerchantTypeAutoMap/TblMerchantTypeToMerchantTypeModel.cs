﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models.Merchant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.AutoMappingConfiguration
{
    public class TblMerchantTypeToMerchantTypeModel : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<TblMerchantType, MerchantTypeModel>()
                .ForMember(dst => dst.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dst => dst.MerchantTypeName, x => x.MapFrom(src => src.MerchantTypeName));
        }
    }
}