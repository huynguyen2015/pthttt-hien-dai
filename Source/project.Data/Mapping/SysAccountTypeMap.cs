﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class SysAccountTypeMap : BaseEntityMap<SysAccountType>
    {
        public SysAccountTypeMap(){
            ToTable("SYS_ACCOUNT_TYPE");
            HasKey(o => o.Id);
            Property(o => o.TypeName);
            Property(o => o.Role);
        }
    }
}