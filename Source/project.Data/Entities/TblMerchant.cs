using System;
using System.Collections.Generic;

namespace project.Data.Entities
{

    public class TblMerchant : BaseEntity
    {
        public virtual long Id { get; set; }
        public string MerchantNumber { get; set; }
        public long BackendProcessor { get; set; }
        public string MerchantName { get; set; }
        public int Status { get; set; }
        public string Owner { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }        
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string BankCardDBA { get; set; }
        public DateTime FistActiveDate { get; set; }
        public DateTime? LastActiveDate { get; set; }
        public long AgentId { get; set; }
        public int MerchantTypeId { get; set; }
        public int RegionId { get; set; }

        public virtual TblMerchantType MerchantType { get; set; }
        public virtual TblRegion Region { get; set; }
        public virtual TblAgent Agent { get; set; }
    }
}
