﻿using project.Service.Models.Agent;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using System.Net.Http;
using System.Web.Http;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("agent")]
    [UserAudit]
    public class AgentController : ApiControllerBase<IAgentControllerDependencyBlock>
    {
       public AgentController(IAgentControllerDependencyBlock agentControllerDependency)
            : base(agentControllerDependency)
        {
        }

        [Route("filter", Name = "FilterAgentRoute")]
        [HttpGet]
        public IHttpActionResult FilterAgent(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateAgentDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.AgentService.FilterAgent(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);

            //var result = _controllerDependency.PlaceService.FilterPlace(new PlaceDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("filternoparams", Name = "FilterAgentNoParamsRoute")]
        [HttpGet]
        public IHttpActionResult FilterAgent_NoParams(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateAgentNoParamsDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.AgentService.FilterAgent_NoParams(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);

            //var result = _controllerDependency.PlaceService.FilterPlace(new PlaceDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("add", Name = "AddAgentRoute")]
        [HttpPost]
        public IHttpActionResult AddAgent(HttpRequestMessage requestMessage, [FromBody] AgentModel agent)
        {
            var result = _controllerDependency.AgentService.Insert(agent);           
            return WrapApiActionResult(requestMessage, true, result);

        }

        [Route("update", Name = "UpdateAgentRoute")]
        [HttpPost]
        public IHttpActionResult UpdateAgent(HttpRequestMessage requestMessage, [FromBody] AgentModel agent)
        {
            var result = _controllerDependency.AgentService.Update(agent);
            return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("{agentId}/delete", Name = "DeleteAgentRoute")]
        [HttpPost]
        public IHttpActionResult DeleteAgent(HttpRequestMessage requestMessage, long agentId)
        {
            var result = _controllerDependency.AgentService.Delete(agentId);            
            return WrapApiActionResult(requestMessage, true, result);
        }        
    }
}