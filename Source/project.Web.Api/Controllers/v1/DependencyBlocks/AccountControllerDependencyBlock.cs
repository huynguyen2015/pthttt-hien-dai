﻿using project.Common.Exceptions;
using project.Common.Security;
using project.Common.Serialization;
using project.Service.Services.Interfaces;
using project.Web.Api.Commons.Helper;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common;
using project.Web.Common.ActionHelper;
using project.Web.Common.Security;

namespace project.Web.Api.Controllers.v1.DependencyBlocks
{
    public class AccountControllerDependencyBlock : ControllerDependencyBlockBase, IAccountControllerDependencyBlock
    {
        public AccountControllerDependencyBlock(
            IMembershipInfoProvider membershipInfoProvider,
            IAccountService memberService,            
            IParseRequestParams parseRequestParams,
            IHttpHelper httpHelper,
            IActionResultHelper actionResultHelper,
            ISerialization serialization, 
            IUserSession userSession,
            IExceptionHelper exceptionHelper)
        {
            MembershipAdapter = membershipInfoProvider;
            MemberService = memberService;          
            HttpHelper = httpHelper;
            ParseRequestParams = parseRequestParams;
            ActionResultHelper = actionResultHelper;
            Serialization = serialization;
            UserSession = userSession;
            ExceptionHelper = exceptionHelper;
        }

        public IMembershipInfoProvider MembershipAdapter { get; private set; }
        public IAccountService MemberService { get; private set; }    
        public ISerialization Serialization { get; private set; }
        public IParseRequestParams ParseRequestParams { get; private set; }
    }
}