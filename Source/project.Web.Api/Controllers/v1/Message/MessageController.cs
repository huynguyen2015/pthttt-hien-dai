﻿using project.Service.Models.Agent;
using project.Service.Models.Message;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using System.Net.Http;
using System.Web.Http;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("message")]
    [UserAudit]
    public class MessageController : ApiControllerBase<IMessageControllerDependencyBlock>
    {
        public MessageController(IMessageControllerDependencyBlock messageControllerDependency)
            : base(messageControllerDependency)
        {
        }

        [Route("filter", Name = "FilterMessageRoute")]
        [HttpGet]
        public IHttpActionResult FilterAgent(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateMessageDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.MessageService.FilterMessage(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);

            //var result = _controllerDependency.PlaceService.FilterPlace(new PlaceDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("filternoparams", Name = "FilterMessageNoParamsRoute")]
        [HttpGet]
        public IHttpActionResult FilterAgent_NoParams(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateMessageNoParamsDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.MessageService.FilterMessage_NoParams(pagedDataRequest);
            return WrapApiActionResult(requestMessage, true, result);

            //var result = _controllerDependency.PlaceService.FilterPlace(new PlaceDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("list", Name = "ListMessageRoute")]
        [HttpPost]
        public IHttpActionResult ListMessage(HttpRequestMessage requestMessage, [FromBody] MessageModel message)
        {
            var result = _controllerDependency.MessageService.GetListEmail(message);
            return WrapApiActionResult(requestMessage, true, result);

        }

        [Route("add", Name = "AddMessageRoute")]
        [HttpPost]
        public IHttpActionResult AddMessage(HttpRequestMessage requestMessage, [FromBody] MessageModel message)
        {
            var result = _controllerDependency.MessageService.InsertMessageService(message);           
            return WrapApiActionResult(requestMessage, true, result);

        }

        //[Route("update", Name = "UpdateAgentRoute")]
        //[HttpPost]
        //public IHttpActionResult UpdateAgent(HttpRequestMessage requestMessage, [FromBody] AgentModel agent)
        //{
        //    var result = _controllerDependency.AgentService.Update(agent);
        //    return WrapApiActionResult(requestMessage, true, result);
        //}

        [Route("delete", Name = "DeleteMessageRoute")]
        [HttpPost]
        public IHttpActionResult DeleteMessage(HttpRequestMessage requestMessage, [FromBody] MessageModel message)
        {
            var result = _controllerDependency.MessageService.DeleteMessageService(message);            
            return WrapApiActionResult(requestMessage, true, result);
        }        
    }
}