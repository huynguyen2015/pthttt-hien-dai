﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class TblRegionMap : BaseEntityMap<TblRegion>
    {
        public TblRegionMap() {
            ToTable("TBL_REGION");
            HasKey(o => o.Id);
            Property(o => o.Name);            
            Property(o => o.Description);
            
            //HasRequired(s => s.Account).WithMany(o => o.SessionTokens).HasForeignKey(o => o.AccountId);
        }
    }
}