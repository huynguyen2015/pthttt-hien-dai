﻿using project.Common.Logging;
using project.Common;
using project.Data.Entities;
using project.Repository.Intefaces;
using project.Data;
using project.Data.PagedDataRequest;
using System;
using System.Linq;
using project.Common.Exceptions;
using System.Linq.Dynamic;
using System.Data.Entity;

namespace project.Repository.Repositories
{
    public class AccountRepository : EfRepository<SysAccount>, IAccountRepository
    {
        IFileHelper _fileHelper;
        public AccountRepository(TravelContext context, IDateTime datetime, ILogManager logManager, IFileHelper fileHeplper)
            : base(context, datetime, logManager)
        {
            _fileHelper = fileHeplper;
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-18, Filter member
        /// </summary>
        /// <param name="pagedDataRequest"></param>        
        /// <returns></returns>
        public QueryResult<SysAccount> FilterAccount(PagedDataRequest PagedDataRequest)
        {
            try
            {
                // Todo: defined Error code
                var startIndex = ResultsPagingUtility.CalculateStartIndex(
                PagedDataRequest.PageNumber, PagedDataRequest.PageSize);

                //Parameter for get list
                var query = Table.AsQueryable();
                if (null != PagedDataRequest.Keyword && !string.IsNullOrEmpty(PagedDataRequest.Keyword))
                    query = query.Where(obj => obj.Email.Contains(PagedDataRequest.Keyword));

                //select all account except deleted accounts
                query = query.Where(a => a.StatusId != Constants.StatusIds.Deleted);

                //parameter for get total count               
                var totalCount = query.Any() ? query.Count() : 0;

                //query = query.Where(c => c.StatusId != Constants.StatusIds.Deleted);
                if (!string.IsNullOrEmpty(PagedDataRequest.NameToSort))
                {
                    query = query.OrderBy<SysAccount>(PagedDataRequest.NameToSort + " " + PagedDataRequest.OrderType);
                    var results = query.Skip(startIndex).Take(PagedDataRequest.PageSize).ToList();
                    return new QueryResult<SysAccount>(results, totalCount, PagedDataRequest.PageSize);
                }
                else
                {
                    //sort default
                    var results = query.OrderBy<SysAccount>("Id ASC").Skip(startIndex).Take(PagedDataRequest.PageSize).ToList();
                    return new QueryResult<SysAccount>(results, totalCount, PagedDataRequest.PageSize);
                } 
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not filter member. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, "MemberRepository:" + ex.Message);
            }
        }
 
        /// <summary>
        /// Huy Nguyen, 2016-10-26, Get member by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public SysAccount GetAccountByEmail(string email)
        {
            try
            {
                var query = Table.AsQueryable();              
                return query.FirstOrDefault(obj => obj.Email == email && obj.StatusId == Constants.StatusIds.Active);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not  get member by email. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, "Failed to get member by email.");
            }
        }

        /// <summary>
        /// Get account by infor
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public SysAccount GetAccountByKeyword(string keyword)
        {
            try
            {
                var query = Table.AsQueryable();
                return query.FirstOrDefault(obj => (obj.Email == keyword || obj.Username == keyword) && obj.StatusId == Constants.StatusIds.Active);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not  get member by info. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, "Failed to get member by email.");
            }
        }

    }
}