﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models.Agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace project.Service.AutoMappingConfiguration
{
    public class AgentModelToTblAgent : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<AgentModel, TblAgent>()
                .ForMember(dst => dst.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dst => dst.AgentCode, x => x.MapFrom(src => src.AgentCode))
                .ForMember(dst => dst.AgentName, x => x.MapFrom(src => src.AgentName))
                .ForMember(dst => dst.StatusId, x => x.MapFrom(src => src.StatusId));
        }
    }
}