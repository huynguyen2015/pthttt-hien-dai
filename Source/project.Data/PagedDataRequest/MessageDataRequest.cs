﻿using System;

namespace project.Data.PagedDataRequest
{
    public class MessageDataRequest : PagedDataRequest
    {
        public MessageDataRequest()         
        {

        }
        public MessageDataRequest(int pageNumber, int pageSize)
            : base(pageNumber, pageSize)
        {
        
        }

        public MessageDataRequest(int pageNumber, int pageSize, DateTime lastTime)
            : base(pageNumber, pageSize, lastTime)
        {
        
        }


        public MessageDataRequest(int pageNumber, int pageSize, int orderId)
            : base(pageNumber, pageSize, orderId)
        {
           
        }
    }
}