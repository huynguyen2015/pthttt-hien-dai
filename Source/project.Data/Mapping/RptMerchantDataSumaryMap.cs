﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class RptMerchantDataSumaryMap : BaseEntityMap<RptMerchantDataSumary>
    {
        public RptMerchantDataSumaryMap() {
            ToTable("RPT_MERCHANT_DATASUMARY");
            HasKey(o => o.Id);
            Property(o => o.DateType);
            Property(o => o.TotalAmount);
            Property(o => o.TotalCount);            
            Property(o => o.MerchantId);
            Property(o => o.DateReport);
            Property(o => o.PayType);

            HasRequired(o => o.Merchant).WithMany().WillCascadeOnDelete(false);
        }
    }
}