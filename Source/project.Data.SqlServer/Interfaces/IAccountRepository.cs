﻿using project.Common;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;

namespace project.Repository.Intefaces
{
    public interface IAccountRepository : IRepository<SysAccount>
    {
        /// <summary>
        /// Khiet Tran, 2016-10-24, filter member
        /// </summary>
        /// <param name="pagedDataRequest"></param>        
        /// <returns></returns>
        QueryResult<SysAccount> FilterAccount(PagedDataRequest pagedDataRequest);       

        /// <summary>
        /// Huy Nguyen, 2016-10-26, Get member by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        SysAccount GetAccountByEmail(string email);

        /// <summary>
        /// GetAccountInfo
        /// </summary>
        /// <param name="keyword"></param>
        /// <returns></returns>
        SysAccount GetAccountByKeyword(string keyword);
    }
}