namespace Click.Data.Entities
{
    using System;
    public  class SocialTypeModel
    {
       
        public short SocialTypeId { get; set; }

        public string SocialName { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

    }
}
