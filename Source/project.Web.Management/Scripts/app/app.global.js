﻿ApiRootURL = "http://localhost:8085/";
//ApiRootURL = "http://localhost:8011/";
Authorization = "";
ApplicationTokenId = "";
DeviceTypeIds = { Web: 4 };

angular.module("app").constant("APP_ROUTES", {
    dashBoard: "app/dashboard",
    MemberSignin: "access/signin"
});

angular.module("app").constant("API_ROUTES", {
    memberLogin: "api/v1/access/loginadmin",
    memberLogout: "api/v1/access/logout",
    memberFilter: "api/v1/members/filter",
    memberAdd: "api/v1/members/add",
    memberUpload: "api/v1/members/upload",
    memberUpdate: "api/v1/members/update",
    memberDelete: "api/v1/members/delete",
    memberSearch: "api/v1/members/search",
    roleFilter: "api/v1/role/filter",
    roleAdd: "api/v1/role/add",
    roleUpdate: "api/v1/role/update",
    roleRemove: "api/v1/role/remove",
    permissionFilter: "api/v1/adminpermission/filter",    
    permissionAdd: "api/v1/adminpermission/add",
    permissionUpdate: "api/v1/adminpermission/update",
    permissionRemove: "api/v1/adminpermission/remove",

    //place category
    placeCategoryFilter: "api/v1/merchant/filter",

    // Agent
    managementAgentFilter: "api/v1/agent/filter",
    managementAgentAdd: "api/v1/agent/add",
    managementAgentUpdate: "api/v1/agent/update",
    managementAgentDelete: "api/v1/agent/{agentId}/delete",
    
});
    
angular.module("app").constant("ORDER_TYPE", {
    ASC: "ASC",
    DESC: "DESC"
});

angular.module("app").constant("ORDER_MEMBER", {
    fullnameAsc: 1,
    fullnameDesc: 2,
    idAsc: 3,
    idDesc: 4,
    ageAsc: 5,
    ageDesc: 6,
    locationAsc: 7,
    locationDesc: 8,
    statusAsc: 9,
    statusDesc: 10,
    joinedAsc: 11,
    joinedDesc: 12,
    lastLoginAsc: 13,
    lastLoginDesc: 14,
    roleAsc: 15,
    roleDesc: 16,
    emailAsc: 17,
    emailDesc: 18,
    failedCountAsc: 19,
    failedCountDesc: 20
});

angular.module("app").constant("ORDER_ROLE", {
    nameAsc: 1,
    nameDesc: 2,
    statusAsc: 3,
    statusDesc: 4,
    createdatAsc: 5,
    createdatDesc: 6,
    updatedatAsc: 7,
    updatedatDesc: 8    
});

angular.module("app").constant("ORDER_PERMISSION", {
    nameAsc: 1,
    nameDesc: 2,
    statusAsc: 3,
    statusDesc: 4,
    createdatAsc: 5,
    createdatDesc: 6,
    updatedatAsc: 7,
    updatedatDesc: 8
});

//PlaceCategory
angular.module("app").constant("ORDER_PLACECATEGORY", {
    idAsc: 1,
    idDesc: 2,
    numberAsc: 3,
    numberDesc: 4,
    nameAsc: 5,
    nameDesc: 6,
    phoneAsc: 7,
    phoneDesc: 8,
    typeAsc: 9,
    typeDesc: 10,
    regionAsc: 11,
    regionDesc: 12
});

//Place
angular.module("app").constant("ORDER_PLACE", {
    idAsc: 1,
    idDesc: 2,
    titleAsc: 3,
    titleDesc: 4,
    descriptionAsc: 5,
    descriptionDesc: 6,
    ratingAsc: 7,
    ratingDesc: 8,
    likenumberAsc: 9,
    likenumberDesc: 10,
    placecategoryAsc: 11,
    placecategoryDesc: 12,
    createdatAsc: 13,
    createdatDesc: 14,
    updatedatAsc: 15,
    updatedatDesc: 16
});

//PlaceImage
angular.module("app").constant("ORDER_PLACEIMAGE", {
    idAsc: 1,
    idDesc: 2,
    placeAsc: 3,
    placeDesc: 4,
    createdatAsc: 5,
    createdatDesc: 6,
    updatedatAsc: 7,
    updatedatDesc: 8
});

var Role = [
	{ id: 1, name: 'Administrator' },
	{ id: 2, name: 'User' },
	{ id: 3, name: 'Guest' },
];

var Status = [
	{ id: 1, name: 'Active' },
	{ id: 2, name: 'Approved' },
	{ id: 3, name: 'Rejected' },
	{ id: 4, name: 'Deleted' },
	{ id: 5, name: 'Locked' },
	{ id: 6, name: 'AdminRemoved' },
	{ id: 7, name: 'SelfRemoved' }
];