﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models.Merchant;

namespace project.Service.AutoMappingConfiguration.MerchantAutoMap
{
    public class MerchantModelToTblMerchant : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<MerchantModel, TblMerchant>()
                .ForMember(dst => dst.Id, x => x.MapFrom(src => src.Id))
                .ForMember(dst => dst.Address, x => x.MapFrom(src => src.Address))
                .ForMember(dst => dst.Agent, x => x.MapFrom(src => src.Agent))
                .ForMember(dst => dst.AgentId, x => x.MapFrom(src => src.AgentId))
                .ForMember(dst => dst.ApprovalDate, x => x.MapFrom(src => src.ApprovalDate))
                .ForMember(dst => dst.BankCardDBA, x => x.MapFrom(src => src.BankCardDBA))
                .ForMember(dst => dst.City, x => x.MapFrom(src => src.City))
                .ForMember(dst => dst.CloseDate, x => x.MapFrom(src => src.CloseDate))
                .ForMember(dst => dst.Email, x => x.MapFrom(src => src.Email))
                .ForMember(dst => dst.Fax, x => x.MapFrom(src => src.Fax))
                .ForMember(dst => dst.FistActiveDate, x => x.MapFrom(src => src.FistActiveDate))
                .ForMember(dst => dst.LastActiveDate, x => x.MapFrom(src => src.LastActiveDate))
                .ForMember(dst => dst.MerchantName, x => x.MapFrom(src => src.MerchantName))
                .ForMember(dst => dst.MerchantNumber, x => x.MapFrom(src => src.MerchantNumber))
                .ForMember(dst => dst.MerchantType, x => x.MapFrom(src => src.MerchantType))
                .ForMember(dst => dst.MerchantTypeId, x => x.MapFrom(src => src.MerchantTypeId))
                .ForMember(dst => dst.Owner, x => x.MapFrom(src => src.Owner))
                .ForMember(dst => dst.Phone, x => x.MapFrom(src => src.Phone))
                .ForMember(dst => dst.Region, x => x.MapFrom(src => src.Region))
                .ForMember(dst => dst.RegionId, x => x.MapFrom(src => src.RegionId))
                .ForMember(dst => dst.State, x => x.MapFrom(src => src.State))
                .ForMember(dst => dst.Status, x => x.MapFrom(src => src.Status))
                .ForMember(dst => dst.StatusId, x => x.MapFrom(src => src.StatusId))
                .ForMember(dst => dst.Zip, x => x.MapFrom(src => src.Zip))
                .ForMember(dst => dst.CreatedAt, x => x.MapFrom(src => src.CreatedAt))
                .ForMember(dst => dst.CreatedUser, x => x.MapFrom(src => src.CreatedUser))
                .ForMember(dst => dst.UpdatedAt, x => x.MapFrom(src => src.UpdatedAt))
                .ForMember(dst => dst.UpdatedUser, x => x.MapFrom(src => src.UpdatedUser));
        }
    }
}