using System;

namespace Click.Data.Entities
{
    public class EducationModel
    {
        public short EducationId { get; set; }

        public string Name { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short AboutMeCategoryId { get; set; }

        
    }
}