﻿using project.Data.PagedDataRequest;
using project.Service.Models;
using project.Service.Models.Merchant;

namespace project.Service.Services.Interfaces
{
    public interface IMerchantTypeService
    {
        MerchantTypeModel GetById(int memberId);
        bool Insert(MerchantTypeModel model);
        bool Update(MerchantTypeModel model);
        bool Delete(MerchantTypeModel model);
        PagedDataInquiryResponse<MerchantTypeModel> FilterMerchantType(MerchantTypeDataRequest dataRequest);
    }
}