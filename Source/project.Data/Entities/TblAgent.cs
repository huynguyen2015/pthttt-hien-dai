using System;
using System.Collections.Generic;

namespace project.Data.Entities
{

    public class TblAgent : BaseEntity
    {
        public virtual long Id { get; set; }
        public string AgentCode { get; set; }
        public string AgentName { get; set; }       
    }
}
