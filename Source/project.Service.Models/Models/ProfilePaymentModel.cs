using System;

namespace Click.Data.Entities
{
    public class ProfilePaymentModel
    {
        public int ProfilePaymentId { get; set; }

        public string ProfileId { get; set; }

        public long MemberId { get; set; }

        public short PaymentPackageId { get; set; }

        public decimal Amount { get; set; }

        public bool IsInAppPurchase { get; set; }

        public DateTimeOffset ExpireAt { get; set; }

        public short[] PayerId { get; set; }

        public string ExpressToken { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public long UpdatedBy { get; set; }

        public long CreatedBy { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public DateTimeOffset? CreatedAt { get; set; }

        public short CurrencyId { get; set; }

        public short PaymentStatusId { get; set; }

        public short RecurringStatusId { get; set; }

    }
}