﻿using AutoMapper;
using project.Common.TypeMapping;
using project.Data.Entities;
using project.Service.Models;

namespace okFree.Service.AutoMappingConfiguration.TourAutoMap
{
    public class SysAccountToAccountModel : IAutoMapperTypeConfigurator
    {
        public void Configure()
        {
            Mapper.CreateMap<SysAccount, AccountModel>()                
                .ForMember(dst => dst.SessionToken, x => x.Ignore());
        }
    }
}