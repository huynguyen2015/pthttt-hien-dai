﻿using project.Data.Entities;
using project.Data;
using project.Common;
using project.Data.PagedDataRequest;

namespace project.Repository.Intefaces
{
    public interface IInfMessageRepository : IRepository<InfMessage>
    {
        QueryResult<InfMessage> FilterMessage(MessageDataRequest dataRequest);
        QueryResult<InfMessage> FilterMessage_NoParams(MessageDataRequest dataRequest);
    }
}