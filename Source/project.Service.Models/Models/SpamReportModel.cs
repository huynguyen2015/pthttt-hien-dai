namespace Click.Data.Entities
{
    using System;

    public  class SpamReportModel
    {
        public long SpamReportId { get; set; }

        public long MemberId { get; set; }

        public long ParentId { get; set; }

        public short ParentTypeId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

    }
}
