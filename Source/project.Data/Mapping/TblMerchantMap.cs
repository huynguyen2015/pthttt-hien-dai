﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class TblMerchantMap : BaseEntityMap<TblMerchant>
    {
        public TblMerchantMap() {
            ToTable("TBL_MERCHANT");
            HasKey(o => o.Id);
            Property(o => o.MerchantNumber);            
            Property(o => o.BackendProcessor);
            Property(o => o.MerchantName);
            Property(o => o.Status);
            Property(o => o.Owner);
            Property(o => o.Address);
            Property(o => o.City);
            Property(o => o.State);
            Property(o => o.Zip);
            Property(o => o.Phone);
            Property(o => o.Fax);
            Property(o => o.Email);
            Property(o => o.ApprovalDate);
            Property(o => o.CloseDate).IsOptional();
            Property(o => o.BankCardDBA);
            Property(o => o.FistActiveDate);
            Property(o => o.LastActiveDate).IsOptional();
            Property(o => o.AgentId);
            Property(o => o.MerchantTypeId);
            Property(o => o.RegionId);

            HasRequired(o => o.MerchantType).WithMany().WillCascadeOnDelete(false);
            HasRequired(o => o.Region).WithMany().WillCascadeOnDelete(false);
            HasRequired(o => o.Agent).WithMany().WillCascadeOnDelete(false);
        }
    }
}