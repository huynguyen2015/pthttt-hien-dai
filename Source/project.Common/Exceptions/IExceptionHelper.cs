﻿using System.Resources;

namespace project.Common.Exceptions
{
    public interface IExceptionHelper
    {
        ExceptionMessage ExceptionMessage(ResourceManager resourceManager, string name);
        ExceptionMessage ResourceToExceptionMessage(ResourceManager resourceManager, string name, string prefix = "Msg");
        ExceptionMessage ExceptionMessageCustomize(int errorCode, string errorMessage);
    }
}
