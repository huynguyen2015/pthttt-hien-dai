namespace project.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.INF_MESSAGE",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Subject = c.String(),
                        Content = c.String(),
                        IsRead = c.Boolean(nullable: false),
                        FromAccountId = c.Long(nullable: false),
                        ToAccountId = c.Long(nullable: false),
                        ParentId = c.Long(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SYS_ACCOUNT", t => t.FromAccountId)
                .ForeignKey("dbo.SYS_ACCOUNT", t => t.ToAccountId)
                .Index(t => t.FromAccountId)
                .Index(t => t.ToAccountId);
            
            CreateTable(
                "dbo.SYS_ACCOUNT",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AccountTypeId = c.Int(nullable: false),
                        AccountId = c.Long(nullable: false),
                        Username = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Salt = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        IsHash = c.Boolean(nullable: false),
                        RoleId = c.Short(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SYS_ACCOUNT_TYPE", t => t.AccountTypeId)
                .Index(t => t.AccountTypeId);
            
            CreateTable(
                "dbo.SYS_ACCOUNT_TYPE",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                        Role = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RPT_MERCHANT_DATASUMARY",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DateType = c.Int(nullable: false),
                        TotalAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalCount = c.Long(nullable: false),
                        MerchantId = c.Long(nullable: false),
                        DateReport = c.DateTime(nullable: false),
                        PayType = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TBL_MERCHANT", t => t.MerchantId)
                .Index(t => t.MerchantId);
            
            CreateTable(
                "dbo.TBL_MERCHANT",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MerchantNumber = c.String(),
                        BackendProcessor = c.Long(nullable: false),
                        MerchantName = c.String(),
                        Status = c.Int(nullable: false),
                        Owner = c.String(),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                        Phone = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        ApprovalDate = c.DateTime(nullable: false),
                        CloseDate = c.DateTime(nullable: false),
                        BankCardDBA = c.String(),
                        FistActiveDate = c.DateTime(nullable: false),
                        LastActiveDate = c.DateTime(nullable: false),
                        AgentId = c.Long(nullable: false),
                        MerchantTypeId = c.Int(nullable: false),
                        RegionId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TBL_AGENT", t => t.AgentId)
                .ForeignKey("dbo.TBL_MERCHANT_TYPE", t => t.MerchantTypeId)
                .ForeignKey("dbo.TBL_REGION", t => t.RegionId)
                .Index(t => t.AgentId)
                .Index(t => t.MerchantTypeId)
                .Index(t => t.RegionId);
            
            CreateTable(
                "dbo.TBL_AGENT",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AgentCode = c.String(),
                        AgentName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TBL_MERCHANT_TYPE",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MerchantTypeName = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TBL_REGION",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RPT_MERCHANT_SUMARY",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReportDate = c.DateTime(nullable: false),
                        MerchantId = c.Long(nullable: false),
                        SaleAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SaleCount = c.Long(nullable: false),
                        ReturnAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ReturnCount = c.Long(nullable: false),
                        TransactionAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionCount = c.Long(nullable: false),
                        KeyAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        KeyedCount = c.Long(nullable: false),
                        ForeignCardAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ForeignCardCount = c.Long(nullable: false),
                        DebitAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DebitCount = c.Long(nullable: false),
                        VisaNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        VisaTransactionCount = c.Long(nullable: false),
                        MasterCardNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MasterCardTransactionCount = c.Long(nullable: false),
                        AmericanExpressNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AmericanExpressTransactionCount = c.Long(nullable: false),
                        DiscoverNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DiscoverNetCount = c.Long(nullable: false),
                        DebitCardNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DebitCardTransactionCount = c.Long(nullable: false),
                        OtherNetAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtherTransactionCount = c.Long(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TBL_MERCHANT", t => t.MerchantId)
                .Index(t => t.MerchantId);
            
            CreateTable(
                "dbo.RPT_TRANSACTION_DETAIL",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MerchantId = c.Long(nullable: false),
                        ReportDate = c.DateTime(nullable: false),
                        FileSource = c.String(),
                        BatchNumber = c.Long(nullable: false),
                        TerminalNumber = c.Int(nullable: false),
                        ExpirationDate = c.DateTime(nullable: false),
                        TransactionCode = c.String(),
                        CardTypeCode = c.String(),
                        TransactionAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionDate = c.DateTime(nullable: false),
                        TransactionTime = c.Time(nullable: false, precision: 7),
                        KeyedEntry = c.Boolean(nullable: false),
                        AuthorizationNumber = c.String(),
                        ReportTime = c.DateTime(nullable: false),
                        Description = c.String(),
                        AccountNumber = c.String(),
                        FirstTwelveAccountNumber = c.String(),
                        CountryId = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TBL_MERCHANT", t => t.MerchantId)
                .Index(t => t.MerchantId);
            
            CreateTable(
                "dbo.SYS_ACCOUNT_SESSION",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        SessionToken = c.String(),
                        ExpirationAt = c.DateTime(nullable: false),
                        AccountId = c.Long(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SYS_ACCOUNT", t => t.AccountId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.SYS_LOG",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FileName = c.String(),
                        ReadDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Version = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TBL_ATTRIBUTE",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Entity = c.Int(nullable: false),
                        KeyNumber = c.Long(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                        IsPrimary = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        CreatedUser = c.Long(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                        UpdatedUser = c.Long(nullable: false),
                        StatusId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SYS_ACCOUNT_SESSION", "AccountId", "dbo.SYS_ACCOUNT");
            DropForeignKey("dbo.RPT_TRANSACTION_DETAIL", "MerchantId", "dbo.TBL_MERCHANT");
            DropForeignKey("dbo.RPT_MERCHANT_SUMARY", "MerchantId", "dbo.TBL_MERCHANT");
            DropForeignKey("dbo.RPT_MERCHANT_DATASUMARY", "MerchantId", "dbo.TBL_MERCHANT");
            DropForeignKey("dbo.TBL_MERCHANT", "RegionId", "dbo.TBL_REGION");
            DropForeignKey("dbo.TBL_MERCHANT", "MerchantTypeId", "dbo.TBL_MERCHANT_TYPE");
            DropForeignKey("dbo.TBL_MERCHANT", "AgentId", "dbo.TBL_AGENT");
            DropForeignKey("dbo.INF_MESSAGE", "ToAccountId", "dbo.SYS_ACCOUNT");
            DropForeignKey("dbo.INF_MESSAGE", "FromAccountId", "dbo.SYS_ACCOUNT");
            DropForeignKey("dbo.SYS_ACCOUNT", "AccountTypeId", "dbo.SYS_ACCOUNT_TYPE");
            DropIndex("dbo.SYS_ACCOUNT_SESSION", new[] { "AccountId" });
            DropIndex("dbo.RPT_TRANSACTION_DETAIL", new[] { "MerchantId" });
            DropIndex("dbo.RPT_MERCHANT_SUMARY", new[] { "MerchantId" });
            DropIndex("dbo.TBL_MERCHANT", new[] { "RegionId" });
            DropIndex("dbo.TBL_MERCHANT", new[] { "MerchantTypeId" });
            DropIndex("dbo.TBL_MERCHANT", new[] { "AgentId" });
            DropIndex("dbo.RPT_MERCHANT_DATASUMARY", new[] { "MerchantId" });
            DropIndex("dbo.SYS_ACCOUNT", new[] { "AccountTypeId" });
            DropIndex("dbo.INF_MESSAGE", new[] { "ToAccountId" });
            DropIndex("dbo.INF_MESSAGE", new[] { "FromAccountId" });
            DropTable("dbo.TBL_ATTRIBUTE");
            DropTable("dbo.SYS_LOG");
            DropTable("dbo.SYS_ACCOUNT_SESSION");
            DropTable("dbo.RPT_TRANSACTION_DETAIL");
            DropTable("dbo.RPT_MERCHANT_SUMARY");
            DropTable("dbo.TBL_REGION");
            DropTable("dbo.TBL_MERCHANT_TYPE");
            DropTable("dbo.TBL_AGENT");
            DropTable("dbo.TBL_MERCHANT");
            DropTable("dbo.RPT_MERCHANT_DATASUMARY");
            DropTable("dbo.SYS_ACCOUNT_TYPE");
            DropTable("dbo.SYS_ACCOUNT");
            DropTable("dbo.INF_MESSAGE");
        }
    }
}
