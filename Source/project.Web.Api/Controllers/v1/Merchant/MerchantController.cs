﻿using project.Service.Models.Merchant;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.Filter;
using project.Web.Common.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using project.Data.PagedDataRequest;
using project.Common;

namespace project.Web.Api.Controllers.v1
{
    [ApiVersion1RoutePrefix("merchant")]
    [UserAudit]
    public class MerchantController : ApiControllerBase<IMerchantControllerDependencyBlock>
    {
        public MerchantController(IMerchantControllerDependencyBlock merchantControllerDependency)
            : base(merchantControllerDependency)
        {
        }

        [Route("filter", Name = "FilterMerchantRoute")]
        [HttpGet]
        public IHttpActionResult FilterMerchant(HttpRequestMessage requestMessage)
        {
            var pagedDataRequest = _controllerDependency.ParseRequestParams.CreateMerchantDataRequest(requestMessage.RequestUri);
            var result = _controllerDependency.MerchantService.FilterMerchant(pagedDataRequest);
            //for (int i = 0; i < result.Items.Count; i++)
            //{
            //    string date = result.Items[i].ApprovalDate.ToString("dd/MM/yyyy");
            //    result.Items[i].ApprovalDate = DateTime.Parse(date);
            //}
            return WrapApiActionResult(requestMessage, true, result);
           
            //var result = _controllerDependency.MerchantService.FilterMerchant(new MerchantDataRequest());
            //return WrapApiActionResult(requestMessage, true, result);
        }

        [Route("add", Name = "AddMerchantRoute")]
        [HttpPost]
        public HttpResponseMessage AddMerchant([FromBody] MerchantModel Merchant)
        {
            var result = _controllerDependency.MerchantService.Insert(Merchant);
            var returnObj = new
            {
                Merchant = Merchant,
                Success = result
            };
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("update", Name = "UpdateMerchantRoute")]
        [HttpPost]
        public HttpResponseMessage UpdateMerchant([FromBody] MerchantModel Merchant)
        {
            var result = _controllerDependency.MerchantService.Update(Merchant);
            var returnObj = new
            {
                Merchant = Merchant,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

        [Route("delete", Name = "DeleteMerchantRoute")]
        [HttpPost]
        public HttpResponseMessage DeleteMerchant([FromBody] MerchantModel removedMerchant)
        {
            var result = _controllerDependency.MerchantService.Delete(removedMerchant);
            var returnObj = new
            {
                Merchant = removedMerchant,
                Success = result
            };
            //create a success response for request
            return this.Request.CreateResponse(System.Net.HttpStatusCode.OK, returnObj);
        }

    }
}