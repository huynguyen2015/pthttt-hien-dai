USE [master]
GO
/****** Object:  Database [CardProcessing]    Script Date: 11/22/2016 9:53:26 PM ******/
CREATE DATABASE [CardProcessing] ON  PRIMARY 
GO
USE [CardProcessing]
GO
/****** Object:  Table [dbo].[INF_NOTIFYCATION]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INF_NOTIFYCATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](1024) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[From] [bigint] NULL,
	[To] [bigint] NULL,
	[Status] [bit] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_INF_NOTIFYCATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_MERCHANT_DATASUMARY]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPT_MERCHANT_DATASUMARY](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DateType] [int] NOT NULL,
	[TotalAmount] [decimal](18, 4) NOT NULL,
	[TotalCount] [decimal](18, 4) NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[DateReport] [datetime] NULL,
	[PayType] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MERCHANT_DATASUMARY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_MERCHANT_SUMARY]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPT_MERCHANT_SUMARY](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[SaleAmount] [decimal](18, 4) NULL,
	[SaleCount] [decimal](18, 4) NULL,
	[ReturnAmount] [decimal](18, 4) NULL,
	[ReturnCount] [decimal](18, 4) NULL,
	[TransactionCount] [decimal](18, 4) NULL,
	[KeyAmount] [decimal](18, 4) NULL,
	[KeyedCount] [decimal](18, 4) NULL,
	[ForeignCardAmount] [decimal](18, 4) NULL,
	[ForeignCardCount] [decimal](18, 4) NULL,
	[DebitAmount] [decimal](18, 4) NULL,
	[DebitCount] [decimal](18, 4) NULL,
	[VisaNetAmount] [decimal](18, 4) NULL,
	[VisaTransactionCount] [decimal](18, 4) NULL,
	[MasterCardNetAmount] [decimal](18, 4) NULL,
	[MasterCardTransactionCount] [decimal](18, 4) NULL,
	[AmericanExpressNetAmount] [decimal](18, 4) NULL,
	[AmericanExpressTransactionCount] [decimal](18, 4) NULL,
	[DiscoverNetAmount] [decimal](18, 4) NULL,
	[DebitCardNetAmount] [decimal](18, 4) NULL,
	[DebitCardTransactionCount] [decimal](18, 4) NULL,
	[OtherNetAmount] [decimal](18, 4) NULL,
	[OtherTransactionCount] [decimal](18, 4) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MerchantSummary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_TRANSACTION_DETAIL]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TRANSACTION_DETAIL](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumberId] [bigint] NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[FileSource] [nvarchar](200) NULL,
	[BatchNumber] [bigint] NOT NULL,
	[TerminalNumber] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[TransactionCode] [char](30) NOT NULL,
	[CardTypeCode] [char](30) NULL,
	[TransactionAmount] [bigint] NULL,
	[TransactionDate] [date] NULL,
	[TransactionTime] [time](7) NULL,
	[KeyedEntry] [bit] NOT NULL,
	[AuthorizationNumber] [char](30) NULL,
	[ReportTime] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
	[AccountNumber] [char](30) NULL,
	[FirstTwelveAccountNumber] [nchar](16) NULL,
	[CountryId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_TransactionDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_ACCOUNT]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountTypeId] [int] NULL,
	[UserType] [int] NOT NULL,
	[Username] [char](100) NULL,
	[Password] [nvarchar](200) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Account] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_ACCOUNT_TYPE]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](200) NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Account_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ROLE]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ROLE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](512) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ROLE_DETAIL]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ROLE_DETAIL](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleDetailName] [nvarchar](200) NOT NULL,
	[RoleId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_SYS_Role_Detail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_AGENT]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_AGENT](
	[Id] [bigint] NOT NULL,
	[AgentCode] [char](30) NOT NULL,
	[AgentName] [nvarchar](100) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Agent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MERCHANT]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MERCHANT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumber] [char](30) NOT NULL,
	[BackendProcessor] [bigint] NULL,
	[MerchantName] [nvarchar](512) NOT NULL,
	[Status] [int] NOT NULL,
	[Owner] [nchar](10) NOT NULL,
	[Address1] [nvarchar](512) NULL,
	[Address2] [nvarchar](512) NULL,
	[Address3] [nvarchar](512) NULL,
	[City] [nvarchar](512) NULL,
	[State] [nvarchar](512) NULL,
	[Zip] [char](20) NULL,
	[Phone] [char](20) NULL,
	[Fax] [char](20) NULL,
	[Email] [char](50) NULL,
	[ApprovalDate] [datetime] NULL,
	[CloseDate] [datetime] NULL,
	[BankCardDBA] [char](50) NOT NULL,
	[FistActiveDate] [datetime] NULL,
	[LastActiveDate] [datetime] NULL,
	[AgentId] [bigint] NULL,
	[MerchantTypeId] [bigint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_Mechant] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MERCHANT_TYPE]    Script Date: 11/22/2016 9:53:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MERCHANT_TYPE](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantTypeName] [nvarchar](200) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdatedUser] [int] NULL,
	[DeleteDate] [datetime] NULL,
	[DeletedUser] [int] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_TBL_MerchantType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[INF_NOTIFYCATION] ADD  CONSTRAINT [DF_INF_NOTIFYCATION_To]  DEFAULT ((0)) FOR [To]
GO
ALTER TABLE [dbo].[INF_NOTIFYCATION] ADD  CONSTRAINT [DF_INF_NOTIFYCATION_Status]  DEFAULT ((1)) FOR [Status]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] ADD  CONSTRAINT [DF_TBL_MERCHANT_DATASUMARY_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] ADD  CONSTRAINT [DF_TBL_MERCHANT_DATASUMARY_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] ADD  CONSTRAINT [DF_TBL_MerchantSummary_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] ADD  CONSTRAINT [DF_TBL_MerchantSummary_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_TBL_TransactionDetail_KeyedEntry]  DEFAULT ((0)) FOR [KeyedEntry]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_TBL_TransactionDetail_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_TBL_TransactionDetail_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] ADD  CONSTRAINT [DF_TBL_Account_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] ADD  CONSTRAINT [DF_TBL_Account_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_TYPE] ADD  CONSTRAINT [DF_SYS_Account_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_TYPE] ADD  CONSTRAINT [DF_SYS_Account_Type_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_AGENT] ADD  CONSTRAINT [DF_TBL_Agent_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TBL_AGENT] ADD  CONSTRAINT [DF_TBL_Agent_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_AGENT] ADD  CONSTRAINT [DF_TBL_Agent_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_MERCHANT] ADD  CONSTRAINT [DF_TBL_Merchant_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[TBL_MERCHANT] ADD  CONSTRAINT [DF_TBL_Merchant_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_MERCHANT] ADD  CONSTRAINT [DF_TBL_Merchant_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TBL_MERCHANT_TYPE] ADD  CONSTRAINT [DF_TBL_MerchantType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TBL_MERCHANT_TYPE] ADD  CONSTRAINT [DF_TBL_MerchantType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY]  WITH CHECK ADD  CONSTRAINT [FK_TBL_MERCHANT_DATASUMARY_TBL_Merchant] FOREIGN KEY([MerchantNumberId])
REFERENCES [dbo].[TBL_MERCHANT] ([Id])
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] CHECK CONSTRAINT [FK_TBL_MERCHANT_DATASUMARY_TBL_Merchant]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY]  WITH CHECK ADD  CONSTRAINT [FK_TBL_MERCHANT_SUMARY_TBL_Merchant] FOREIGN KEY([MerchantNumberId])
REFERENCES [dbo].[TBL_MERCHANT] ([Id])
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] CHECK CONSTRAINT [FK_TBL_MERCHANT_SUMARY_TBL_Merchant]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_TBL_TRANSACTION_DETAIL_TBL_Merchant] FOREIGN KEY([MerchantNumberId])
REFERENCES [dbo].[TBL_MERCHANT] ([Id])
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] CHECK CONSTRAINT [FK_TBL_TRANSACTION_DETAIL_TBL_Merchant]
GO
ALTER TABLE [dbo].[SYS_ROLE_DETAIL]  WITH CHECK ADD  CONSTRAINT [FK_SYS_ROLE_DETAIL_SYS_ROLE] FOREIGN KEY([RoleId])
REFERENCES [dbo].[SYS_ROLE] ([Id])
GO
ALTER TABLE [dbo].[SYS_ROLE_DETAIL] CHECK CONSTRAINT [FK_SYS_ROLE_DETAIL_SYS_ROLE]
GO
ALTER TABLE [dbo].[TBL_MERCHANT]  WITH CHECK ADD  CONSTRAINT [FK_TBL_Merchant_TBL_MERCHANT_TYPE] FOREIGN KEY([MerchantTypeId])
REFERENCES [dbo].[TBL_MERCHANT_TYPE] ([Id])
GO
ALTER TABLE [dbo].[TBL_MERCHANT] CHECK CONSTRAINT [FK_TBL_Merchant_TBL_MERCHANT_TYPE]
GO
USE [master]
GO
ALTER DATABASE [CardProcessing] SET  READ_WRITE 
GO
