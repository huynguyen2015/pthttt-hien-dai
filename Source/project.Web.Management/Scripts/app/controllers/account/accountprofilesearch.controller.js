/**
 * @ngdoc overview
 * @name Member controller
 * @description 
 *
 */
(function() {     
    var myApp = angular.module('app')
    myApp.controller('AccountProfileSearchController', ['$scope', '$uibModal', 'commonService', '$location', 'HttpService', 'API_ROUTES', 'ORDER_PLACECATEGORY',
    function ($scope, $uibModal, commonService, $location, httpService, apiRoutes, ORDER_PLACECATEGORY) {
        // Declare variable
        commonService.datetimePicker($scope);
        $scope.commonService = commonService;
        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 0, pageSize: 0, currentItem: '0-20', totalItems: 0 };
            $scope.filter = { orderId: ORDER_PLACECATEGORY.nameDesc, total: "", regionId: -1, merchantTypeId: -1 };
        }

        $scope.options = {
            height: 150
        };

        resetFilter();

        //function filterMerchants() {
        //    console.log("a");
        //    httpService.sendGet(apiRoutes.placeCategoryFilter, $scope.filter, $scope.setContentLoading).then(function (response) {
        //        console.log(response);
        //        if (response && response.Success === true) {
        //            $scope.datas = response.Data ? response.Data.Items : [];
        //            $scope.paging = commonService.preparePagination(response);
        //        }
        //    });
        //}

        //filterMerchants();
        //$scope.removedPlace = {};

        //$scope.sortBy = function (type) {
        //    switch (type) {
        //        case "name":
        //            {
        //                $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACECATEGORY.nameAsc ? ORDER_PLACECATEGORY.nameDesc : ORDER_PLACECATEGORY.nameAsc;
        //                break;
        //            }
        //        case "id":
        //            {
        //                $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACECATEGORY.idAsc ? ORDER_PLACECATEGORY.idDesc : ORDER_PLACECATEGORY.idAsc;
        //                break;
        //            }
        //        case "number":
        //            {
        //                $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACECATEGORY.numberAsc ? ORDER_PLACECATEGORY.numberDesc : ORDER_PLACECATEGORY.numberAsc;
        //                break;
        //            }
        //        case "phone":
        //            {
        //                $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACECATEGORY.phoneAsc ? ORDER_PLACECATEGORY.phoneDesc : ORDER_PLACECATEGORY.phoneAsc;
        //                break;
        //            }
        //        case "type":
        //            {
        //                $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACECATEGORY.typeAsc ? ORDER_PLACECATEGORY.typeDesc : ORDER_PLACECATEGORY.typeAsc;
        //                break;
        //            }
        //        case "region":
        //            {
        //                $scope.filter.orderId = $scope.filter.orderId === ORDER_PLACECATEGORY.regionAsc ? ORDER_PLACECATEGORY.regionDesc : ORDER_PLACECATEGORY.regionAsc;
        //                break;
        //            }
        //    }
        //    filterMerchants();
        //}
        //$scope.getSortClass = function (type) {
        //    switch (type) {
        //        case "name":
        //            {
        //                return $scope.filter.orderId === ORDER_PLACECATEGORY.nameAsc ? "sorting_asc" : ($scope.filter.orderId === ORDER_PLACECATEGORY.nameDesc ? "sorting_desc" : "sorting");
        //            }
        //        case "id":
        //            {
        //                return $scope.filter.orderId === ORDER_PLACECATEGORY.idAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACECATEGORY.idDesc ? "sorting_desc" : "sorting");
        //            }
        //        case "number":
        //            {
        //                return $scope.filter.orderId === ORDER_PLACECATEGORY.numberAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACECATEGORY.numberDesc ? "sorting_desc" : "sorting");
        //            }
        //        case "phone":
        //            {
        //                return $scope.filter.orderId === ORDER_PLACECATEGORY.phoneAsc ? "sorting_asc" : ($scope.filter.orderId === ORDER_PLACECATEGORY.phoneDesc ? "sorting_desc" : "sorting");
        //            }
        //        case "type":
        //            {
        //                return $scope.filter.orderId === ORDER_PLACECATEGORY.typeAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACECATEGORY.typeDesc ? "sorting_desc" : "sorting");
        //            }
        //        case "region":
        //            {
        //                return $scope.filter.orderId === ORDER_PLACECATEGORY.regionAsc ? 'sorting_asc' : ($scope.filter.orderId === ORDER_PLACECATEGORY.regionDesc ? "sorting_desc" : "sorting");
        //            }
        //        default:
        //            return '';
        //    }
        //}

        //$scope.searchPageChange = function (flag, pageNumber) {
        //    if (flag === 'pre') {
        //        if ($scope.paging.currentPage - 1 > 0)
        //            $scope.paging.currentPage -= 1;
        //    } else if (flag === 'nxt') {
        //        if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
        //            $scope.paging.currentPage += 1;
        //    } else {
        //        $scope.paging.currentPage = pageNumber;
        //    }
        //    $scope.filter.pageNumber = $scope.paging.currentPage;
        //    filterMerchants();
        //};

        //$scope.searchClick = function () {
        //    //$scope.filter.pageNumber = 1;
        //    filterMerchants();
        //}

        //$scope.showPopup = function (place) {
        //    commonService.openModal("placemodal.html", "PlaceModificationController", place, filterMerchants);
        //}

        //$scope.removeItem = function (item) {
        //    $scope.removedPlace = item;
        //}

        //$scope.deletePlace = function () {
        //    console.log("Delete place with Name:" + $scope.removedPlace.Name);

        //    httpService.sendPost(apiRoutes.placeCategoryDelete, $scope.removedPlace, $scope.setContentLoading).then(function (response) {
        //        if (response && response.Success === true) {
        //            filterMerchants();
        //        }
        //    });
        //    return true;
        //}
    }]);
})();
