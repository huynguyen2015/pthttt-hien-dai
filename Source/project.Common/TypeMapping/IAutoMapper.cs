﻿namespace project.Common.TypeMapping
{
    public interface IAutoMapper
    {
        T Map<T>(object objectToMap);

        TDestination Map<TSource, TDestination>(TSource source, TDestination destination);

    }
}
