﻿using project.Common;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;

namespace project.Repository.Interfaces
{
    public interface IRegionRepository : IRepository<TblRegion>
    {
        QueryResult<TblRegion> FilterRegion(RegionDataRequest dataRequest);
    }
}