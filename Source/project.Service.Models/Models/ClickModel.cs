namespace Click.Data.Entities
{
    using System;

    public  class ClickModel
    {
       
        public long ClickId { get; set; }

        public string Name { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        

        public short StatusId { get; set; }

        

    }
}
