﻿using project.Common;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;

namespace project.Repository.Interfaces
{
    public interface IAgentRepository : IRepository<TblAgent>
    {
        QueryResult<TblAgent> FilterAgent(AgentDataRequest dataRequest);
        QueryResult<TblAgent> FilterAgent_NoParams(AgentDataRequest dataRequest);
    }
}