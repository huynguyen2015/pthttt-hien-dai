using System;

namespace Click.Data.Entities
{
    public class PhotoModel
    {
        public long PhotoId { get; set; }

        public short ParentTypeId { get; set; }

        public long ParentId { get; set; }

        public string PhotoName { get; set; }

        public string UuId { get; set; }

        public short StatusId { get; set; }

        public short SortOrder { get; set; }

        public short PhotoTypeId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public bool IsMainPhoto { get; set; }

        public bool IsAvatar { get; set; }
    }
}