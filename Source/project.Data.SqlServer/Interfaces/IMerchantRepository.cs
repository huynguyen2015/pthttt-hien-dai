﻿using project.Common;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;

namespace project.Repository.Interfaces
{
    public interface IMerchantRepository : IRepository<TblMerchant>
    {
        QueryResult<TblMerchant> FilterMerchant(MerchantDataRequest dataRequest);
    }
}