﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using project.Web.Common.ActionHelper;

namespace project.Web.Common.Validation
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override bool AllowMultiple
        {
            get { return false; }
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ModelState.IsValid == false)
            {                
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.OK,
                                                                                new ActionResult
                                                                                {
                                                                                    ErrorCode = 500,
                                                                                    Success = false,
                                                                                    Message = "Bad Request"
                                                                                });
            }
        }
    }
}