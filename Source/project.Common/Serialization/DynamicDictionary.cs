﻿using System.Collections.Generic;
using System.Dynamic;

namespace project.Common.Serialization
{
    public class DynamicDictionary : DynamicObject
    {
        readonly Dictionary<string, object> _dictionary
            = new Dictionary<string, object>();
        public int Count
        {
            get
            {
                return _dictionary.Count;
            }
        }

        public Dictionary<string, object> Parse()
        {
            return _dictionary;
        }

        public override bool TryGetMember(
            GetMemberBinder binder, out object result)
        {
            var name = binder.Name;
            return _dictionary.TryGetValue(name, out result);
        }
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            _dictionary[binder.Name] = value;
            return true;
        }
    }
}
