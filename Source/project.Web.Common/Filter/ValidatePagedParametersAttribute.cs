﻿using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using project.Common;
using project.Common.Exceptions;
using project.Common.Logging;
using log4net;

namespace project.Web.Common.Filter
{
    public class ValidatePagedParametersAttribute : ActionFilterAttribute
    {
        protected readonly ILog _log;
        public ValidatePagedParametersAttribute()
            : this(WebContainerManager.Get<ILogManager>())
        {
        }
        public ValidatePagedParametersAttribute(ILogManager logManager)
        {
            _log = logManager.GetLog(typeof(ValidatePagedParametersAttribute));
        }
        public override bool AllowMultiple
        {
            get { return false; }
        }
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            ValueCollection = actionContext.Request.RequestUri.ParseQueryString();
            int? pageNumber;
            int? pageSize;

            try
            {
                pageNumber = PrimitiveTypeParser.Parse<int?>(ValueCollection[Constants.CommonParameterNames.PageNumber]);
                pageSize = PrimitiveTypeParser.Parse<int?>(ValueCollection[Constants.CommonParameterNames.PageSize]);
            }
            catch (Exception ex)
            {
                _log.Error("Error parsing pageNumber or pageSize from URL query string ", ex);
                throw new BusinessException(2612, "Invalid PageNumber or PageSize");
            }
        }
        protected NameValueCollection ValueCollection { get; private set; }
    }
}