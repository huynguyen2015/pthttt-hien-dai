﻿
namespace project.Data.PagedDataRequest
{
    public class FilterParameter : BaseFilterParameter
    {        
        public string Keyword { get; set; }
        public short OrderId { get; set; }
    }
}