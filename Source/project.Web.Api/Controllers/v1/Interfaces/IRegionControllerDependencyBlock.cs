﻿using project.Service.Services.Interfaces;
using project.Web.Api.Commons.Helper;

namespace project.Web.Api.Controllers.v1.Interfaces
{
    public interface IRegionControllerDependencyBlock : IControllerDependencyBlock
    {
        IRegionService RegionService { get; }
        IParseRequestParams ParseRequestParams { get; }
    }
}