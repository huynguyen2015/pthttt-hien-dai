﻿using System.ComponentModel;
using System.Globalization;

namespace project.Common
{
    public class PrimitiveTypeParser
    {
        public static T Parse<T>(string valueAsString)
        {           
            var converter = TypeDescriptor.GetConverter(typeof(T));            
            var result = converter.ConvertFromString(valueAsString);
            return (T)result;
        }

        public static T ParseWithEnCulture<T>(string valueAsString)
        {
            var culture = new CultureInfo("en");

            var converter = TypeDescriptor.GetConverter(typeof(T));
            var result = converter.ConvertFromString(null, culture, valueAsString);
            return (T)result;
        }
    }
}