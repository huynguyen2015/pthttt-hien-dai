using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;
using System.Web.Http;
using System.Web.WebPages;
using project.Common;
using project.Common.Security;
using project.Common.Serialization;
using project.Common.TypeMapping;
using project.Service.Models;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.ActionHelper;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web.Http.Controllers;
using System.Threading;
using travel.Service.Models;

namespace project.Web.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        protected IActionResultHelper _actionResultHelper;        
        protected IAutoMapper _autoMapper;
        protected IUserSession _userSession;

    }

    public abstract class ApiControllerBase<T> : ApiController where T : IControllerDependencyBlock
    {
        protected T _controllerDependency;        

        public ApiControllerBase(T controllerDependency)
        {
            _controllerDependency = controllerDependency;
        }
        protected ApiActionResult WrapApiActionResult<TOut>(HttpRequestMessage requestMessage
            , bool isSuccess
            , TOut data
            , string message = "")
        {
            var actionResult = _controllerDependency.ActionResultHelper
                .WrapActionResult(isSuccess, message, data);
            return new ApiActionResult(requestMessage, actionResult);
        }

        protected ApiActionResult WrapApiActionResult<TOut>(HttpResponseMessage responseMessage
            , bool isSuccess
            , TOut data
            , string message = "")
        {
            var actionResult = _controllerDependency.ActionResultHelper
                .WrapActionResult(isSuccess, message, data);
            return new ApiActionResult(responseMessage, actionResult);
        }

        protected ApiActionResult WrapApiActionResult<TOut>(HttpRequestMessage requestMessage
            , bool isSuccess
            , PagedDataInquiryResponse<TOut> paging
            , Action<dynamic> customize
            , string message = "")
        {
            if (paging == null)
                throw new NullReferenceException("Paging agrument have not null");
            if (customize == null)
                throw new NullReferenceException("Customize agrument have not null");
            dynamic data = new DynamicDictionary();
            data.MaxId = paging.MaxId;
            data.TotalCount = paging.TotalItemCount;
            data.PageCount = paging.PageCount;
            data.PageSize = paging.PageSize;
            data.PageNumber = paging.PageNumber;
            customize(data);
            var actionResult = _controllerDependency.ActionResultHelper
                .WrapActionResult(isSuccess, message, data.Parse());
            return new ApiActionResult(requestMessage, actionResult);
        }


        protected short GetClassifyId(HttpRequestMessage requestMessage)
        {
            //get device Type
            var deviceType = _controllerDependency.HttpHelper.GetFirstHeaderValue(requestMessage, Constants.HttpHeaderKeyNames.ClassifyId);
            if (!deviceType.IsInt()) return 1;
            var deviceTypeId = PrimitiveTypeParser.Parse<short>(deviceType);
            return deviceTypeId > 0 ? deviceTypeId : (short)1;
        }

        protected string ConvertStreamToBase64(Stream fileStream)
        {
            var img = Bitmap.FromStream(fileStream);
            using (MemoryStream ms = new MemoryStream())
            {
                //Convert Image to byte
                img.Save(ms, ImageFormat.Png);
                byte[] imgBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imgBytes);
                return base64String;
            }
        }

        protected async Task<List<UploadFileStream>> parseFilesFromRequestMessage(HttpRequestMessage requestMessage)
        {
            SerializationHelper _serialization = new SerializationHelper();
            var provider = new MultipartMemoryStreamProvider();
            await requestMessage.Content.ReadAsMultipartAsync(provider);
            var files = new List<UploadFileStream>();
            Type type = typeof(Constants.MediaTypeNames);
            var unsupportedFile = new List<string>();

            string mediaType = string.Empty;
            foreach (var content in provider.Contents)
            {
                mediaType = content.Headers.ContentType.MediaType;
                foreach (var p in type.GetFields(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic))
                {
                    if (mediaType.Equals(p.GetValue(null)))
                    {
                        var fileName = content.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                        files.Add(new UploadFileStream
                        {
                            FileStream = await content.ReadAsStreamAsync(),
                            FileName = fileName,
                            ContentType = content.Headers.ContentType.MediaType,
                            Extention = Path.GetExtension(fileName)
                        });

                        continue;
                    }
                    unsupportedFile.Add(mediaType);
                }
            }

            if (files.Count > 0)
                return files;

            throw new UnsupportedMediaTypeException(string.Join(",", unsupportedFile.Distinct().ToArray().Select(s => s).ToArray())
                + " are not supported!", new MediaTypeHeaderValue(string.Empty));
        }

        protected bool IsSupportedMediaType(string mediaType)
        {
            Type type = typeof(Constants.MediaTypeNames);
            var fields = type.GetFields();
            foreach (var p in fields)
            {
                if (p.GetValue(null).ToString().Contains(mediaType))
                {
                    return true;
                }
            }

            return false;
        }

        public string SaveUploadedFile(UploadFileStream file, string folder)
        {
            var apiPath = System.Web.HttpContext.Current.Server.MapPath("/");
            var dinfo = new DirectoryInfo(apiPath);
            var drectoryInfo = dinfo.Parent.GetDirectories().Where(f => f.Name.Contains("project.Web.MvcManagement")).ToList()[0];
            var desPath = drectoryInfo.FullName + "/Content/Upload/" + folder + "/" + file.FileName;
            var savedFile = File.Open(desPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            //var savedFile = File.Create(desPath, );
            file.FileStream.Seek(0, SeekOrigin.Begin);
            file.FileStream.CopyTo(savedFile);
            savedFile.Close();

            var index = desPath.IndexOf("/Content");

            var resultPath = desPath.Substring(index);
            return resultPath;
        }

        public string SaveUploadedFile(UploadFileStream file, string folder, string optionFileName)
        {
            var apiPath = System.Web.HttpContext.Current.Server.MapPath("/");
            var dinfo = new DirectoryInfo(apiPath);
            var drectoryInfo = dinfo.Parent.GetDirectories().Where(f => f.Name.Contains("project.Web.MvcManagement")).ToList()[0];
            var desPath = drectoryInfo.FullName + "/Content/Upload/" + folder + "/" + optionFileName + file.Extention;
            var savedFile = File.Open(desPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            //var savedFile = File.Create(desPath, );
            file.FileStream.Seek(0, SeekOrigin.Begin);
            file.FileStream.CopyTo(savedFile);
            savedFile.Close();

            var index = desPath.IndexOf("/Content");

            var resultPath = desPath.Substring(index);
            return resultPath;
        }

        protected async Task<T> GetModel<T>(HttpRequestMessage request, ICollection<UploadFileStream> files)
        {
            var model = default(T);
            var provider = new MultipartMemoryStreamProvider();
            await request.Content.ReadAsMultipartAsync(provider);

            foreach (var content in provider.Contents)
            {
                if (content.Headers.ContentType == null ||
                    (!content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.ImageJpeg)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.ImageJpg)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.ImagePng)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.VideoMp4)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.VideoMov)
                    && !content.Headers.ContentType.MediaType.Equals(Constants.MediaTypeNames.VideoQuickTime)
                    && !content.Headers.ContentType.MediaType.Equals(
                         Constants.MediaTypeNames.ApplicationJson)))
                    throw new UnsupportedMediaTypeException(
                        "Only support image/jpeg, image/jpg, image/png, video/mp4, video/mov, video/quicktime, application/json",
                        new MediaTypeHeaderValue("multipart/form-data"));

                if (content.Headers.ContentType.MediaType == Constants.MediaTypeNames.ImageJpeg
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.ImageJpg
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.ImagePng
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.VideoMp4
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.VideoMov
                    || content.Headers.ContentType.MediaType == Constants.MediaTypeNames.VideoQuickTime)
                {
                    var fileName = content.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    files.Add(new UploadFileStream
                    {
                        FileStream = await content.ReadAsStreamAsync(),
                        FileName = fileName,
                        ContentType = content.Headers.ContentType.MediaType,
                        Extention = Path.GetExtension(fileName)
                    });
                    continue;
                }
                model = _controllerDependency.Serialization.DeserializeJsonToObject<T>(
                        content.ReadAsStringAsync().Result);
            }
            return model;
        }
    }
}