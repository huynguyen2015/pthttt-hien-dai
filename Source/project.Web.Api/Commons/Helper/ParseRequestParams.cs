﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using project.Common;
using project.Common.Extensions;
using project.Common.Logging;
using project.Data.PagedDataRequest;

namespace project.Web.Api.Commons.Helper
{
    public class ParseRequestParams : IParseRequestParams
    {
        private readonly ILog _log;
        private readonly IDateTime _dateTime;

        public ParseRequestParams(ILogManager logManager, IDateTime dateTime)
        {
            _log = logManager.GetLog(typeof(ParseRequestParams));
            _dateTime = dateTime;
        }
        public PagedDataRequest CreatePagedDataRequest(Uri requestUri)
        {
            var pagedDataRequest = GetPagedDataRequest<PagedDataRequest>(requestUri);
            return pagedDataRequest;
        }      
        
        public PageFilterDataRequest CreateFilterDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var filterParameter = new FilterParameter()
            {                
                OrderId = GetParam<short>(Constants.CommonParameterNames.OrderId, valueCollection),
                Keyword = GetKeyword(valueCollection)
            };
            return new PageFilterDataRequest(GetPageNumber(valueCollection),
                GetPageSize(valueCollection), GetLastTimeToPaging(valueCollection, GetPageNumber(valueCollection)), filterParameter);
        }
        
        ///<summary>
        /// Khiet Tran, 2016-10-24
        /// Create paged request data for member
        ///</summary>
        ///<param name="requestUri"></param>
        public PagedDataRequest CreateMemberFilterDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var filter = GetParam<bool?>("filter", valueCollection);

            var memberReq = new PagedDataRequest();
            if (filter != null && filter == true)
            {
                memberReq.OrderType = GetParam<string>("orderType", valueCollection);
                memberReq.NameToSort = GetParam<string>("name2Sort", valueCollection);
            }

            memberReq.PageNumber = GetPageNumber(valueCollection);
            memberReq.PageSize = GetPageSize(valueCollection);

            return memberReq;
        }

        
        /// <summary>
        /// Khiet Tran, 2016-10-26
        /// searching a member
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public PagedDataRequest CreateMemberSeachingDataRequest(Uri uri)
        {
            var paramCollection = uri.ParseQueryString();
            var PagedDataRequest = new PagedDataRequest();

            return PagedDataRequest;
        }

        /// <summary>
        /// The Hoa, 2017-02-15
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public MerchantDataRequest CreateMerchantDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new MerchantDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                Keyword = GetKeyword(valueCollection),
                RegionId = GetParam<int>("regionId", valueCollection),
                MerchantTypeId = GetParam<int>("merchantTypeId", valueCollection),
                AgentId = GetParam<int>("agentId", valueCollection),
                Name = GetParam<string>("name", valueCollection),
                PageNumber = GetPageNumber(valueCollection)
            };
            return result;
        }

        /// <summary>
        /// The Hoa, 2017-02-15
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public MerchantTypeDataRequest CreateMerchantTypeDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new MerchantTypeDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                Keyword = GetKeyword(valueCollection),
            };
            return result;
        }

        /// <summary>
        /// The Hoa, 2017-02-15
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public RegionDataRequest CreateRegionDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new RegionDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                Keyword = GetKeyword(valueCollection),
            };
            return result;
        }

        /// <summary>
        /// The Hoa, 2017-02-15
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public AgentDataRequest CreateAgentDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new AgentDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                AgentName = GetParam<string>("agentName", valueCollection),
                AgentCode = GetParam<string>("agentCode", valueCollection),                
                Keyword = GetKeyword(valueCollection),
                CreatedFrom = GetFromDate(valueCollection),
                CreatedTo  = GetToDate(valueCollection),
                PageNumber = GetPageNumber(valueCollection),
                PageSize = GetPageSize(valueCollection),
            };
            return result;
        }

        /// <summary>
        /// The Hoa, 2017-02-15
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public AgentDataRequest CreateAgentNoParamsDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new AgentDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                Keyword = GetKeyword(valueCollection),
            };
            return result;
        }

        /// <summary>
        /// ThoaiND - 17/2/2017
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public MessageDataRequest CreateMessageDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new MessageDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                Keyword = GetKeyword(valueCollection),
            };
            return result;
        }

        /// <summary>
        /// ThoaiND - 17/2/2017
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public MessageDataRequest CreateMessageNoParamsDataRequest(Uri requestUri)
        {
            var valueCollection = requestUri.ParseQueryString();
            var result = new MessageDataRequest()
            {
                orderId = GetParam<int>("orderId", valueCollection),
                Keyword = GetKeyword(valueCollection),
            };
            return result;
        }
        #region PRIVATE MENTHOD
        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get last time to paging
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private DateTimeOffset GetLastTimeToPaging(NameValueCollection valueCollection, int pageNumber)
        {
            var maxId = GetMaxId(valueCollection) ?? 0;
            if (pageNumber == Constants.Paging.MinPageNumber || maxId == 0)
                return _dateTime.UtcNow;
            return _dateTime.DateTimeOffsetFromUnixTimestampMillis(maxId);
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get max id
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private long? GetMaxId(NameValueCollection collection)
        {
            return PrimitiveTypeParser.Parse<long?>(collection[Constants.CommonParameterNames.MaxId]);
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get collection of param
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private IList<T> GetCollectionParam<T>(NameValueCollection collection, string paramName)
        {
            var paramValue = collection[paramName];
            if (string.IsNullOrWhiteSpace(paramValue))
                return new List<T>();

            const string pattern = @"[{}()]";
            var rgx = new Regex(pattern);
            paramValue = rgx.Replace(paramValue, string.Empty);

            return paramValue.Split(',').Select(PrimitiveTypeParser.Parse<T>).ToList();

        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get lat
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private decimal? GetLat(NameValueCollection collection)
        {
            return PrimitiveTypeParser.ParseWithEnCulture<decimal?>(collection[Constants.CommonParameterNames.Lat]);
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get lng
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private decimal? GetLng(NameValueCollection collection)
        {
            return PrimitiveTypeParser.ParseWithEnCulture<decimal?>(collection[Constants.CommonParameterNames.Lng]);
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get keyword
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private string GetKeyword(NameValueCollection collection)
        {
            if (collection[Constants.CommonParameterNames.Keyword] == null)
                return string.Empty;
            return collection[Constants.CommonParameterNames.Keyword].ToLowerInvariant();
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get page number
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private int GetPageNumber(NameValueCollection collection)
        {
            int? pageNumber;
            pageNumber = PrimitiveTypeParser.Parse<int?>(collection[Constants.CommonParameterNames.PageNumber]);
            pageNumber = pageNumber.GetBoundedValue(
                Constants.Paging.DefaultPageNumber, Constants.Paging.MinPageNumber);
            return pageNumber.Value;
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get page size
        /// </summary>        
        /// <param name="collection"></param>
        /// <returns></returns>
        private int GetPageSize(NameValueCollection collection)
        {
            int? pageSize;
            pageSize = PrimitiveTypeParser.Parse<int?>(collection[Constants.CommonParameterNames.PageSize]);
            pageSize = pageSize.GetBoundedValue(
                Constants.Paging.DefaultPageSize, Constants.Paging.MinPageSize, Constants.Paging.MaxPageSize);
            return pageSize.Value;
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get page data request
        /// </summary>        
        /// <param name="requestUri"></param>
        /// <returns></returns>
        protected T GetPagedDataRequest<T>(Uri requestUri) where T : PagedDataRequest
        {
            var valueCollection = requestUri.ParseQueryString();
            var pageNumber = GetPageNumber(valueCollection);
            var pageSize = GetPageSize(valueCollection);
            var lastTime = GetLastTimeToPaging(valueCollection, pageNumber);
            var keyword = GetParam<string>(Constants.CommonParameterNames.Keyword, valueCollection);
            var pagedDataRequest = Activator.CreateInstance(typeof(T), pageNumber, pageSize, lastTime, keyword);
            return (T)pagedDataRequest;
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get from date
        /// </summary>        
        /// <param name="requestUri"></param>
        /// <returns></returns>
        private DateTime GetFromDate(NameValueCollection valueCollection)
        {
            var fromDate = valueCollection[Constants.CommonParameterNames.From];

            return string.IsNullOrEmpty(fromDate) ? DateTime.MinValue : GetParam<DateTime>(Constants.CommonParameterNames.From, valueCollection);
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get to date
        /// </summary>        
        /// <param name="requestUri"></param>
        /// <returns></returns>
        private DateTime GetToDate(NameValueCollection valueCollection)
        {
            var toDate = valueCollection[Constants.CommonParameterNames.To];

            return string.IsNullOrEmpty(toDate) ? DateTime.MinValue : GetParam<DateTime>(Constants.CommonParameterNames.To, valueCollection);
        }

        /// <summary>
        /// Huy Nguyen, 2016-08-27
        /// Get common param
        /// </summary>        
        /// <param name="requestUri"></param>
        /// <returns></returns>
        private T GetParam<T>(string name, NameValueCollection collection)
        {
            var obj = collection[name];
            return PrimitiveTypeParser.Parse<T>(obj);
        }
        /// <summary>
        /// Khiet Tran, get Datetime params
        /// </summary>
        /// <param name="valueCollection"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private DateTime GetParamDateTime(NameValueCollection valueCollection, string name)
        {
            var result = valueCollection[name];
            return string.IsNullOrEmpty(result) ? DateTime.MinValue : GetParam<DateTime>(name, valueCollection);
        }

        //private T GetParams<T>(NameValueCollection valueCollection, string name)
        //{
        //    var result = valueCollection[name];
        //    if (result == null)
        //        return null;
        //}
        #endregion
    }
}