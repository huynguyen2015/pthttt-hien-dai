﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class RptTransactionDetailMap : BaseEntityMap<RptTransactionDetail>
    {
        public RptTransactionDetailMap() {
            ToTable("RPT_TRANSACTION_DETAIL");
            HasKey(o => o.Id);
            Property(o => o.MerchantId);
            Property(o => o.ReportDate);
            Property(o => o.FileSource);            
            Property(o => o.BatchNumber);
            Property(o => o.TerminalNumber);
            Property(o => o.ExpirationDate);
            Property(o => o.TransactionCode);
            Property(o => o.CardTypeCode);
            Property(o => o.TransactionAmount);
            Property(o => o.TransactionDate);
            Property(o => o.TransactionTime);
            Property(o => o.KeyedEntry);
            Property(o => o.AuthorizationNumber);
            Property(o => o.ReportTime);
            Property(o => o.Description);
            Property(o => o.AccountNumber);
            Property(o => o.FirstTwelveAccountNumber);
            Property(o => o.CountryId);


            HasRequired(o => o.Merchant).WithMany().WillCascadeOnDelete(false);
        }
    }
}