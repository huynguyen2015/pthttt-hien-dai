﻿using System.Net.Http;

namespace project.Web.Common
{
    public interface IHttpHelper
    {
        string GetFirstHeaderValue(HttpRequestMessage requestMessage, string keyName);
    }
}
