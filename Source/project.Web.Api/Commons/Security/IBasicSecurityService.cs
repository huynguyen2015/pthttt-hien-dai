﻿
namespace project.Web.Api.Security
{
    public interface IBasicSecurityService
    {
        bool SetPrincipal(string email, string password);
        bool IsValidSessionToken(long sessionTokenId);
    }
}
