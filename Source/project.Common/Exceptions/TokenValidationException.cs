﻿using System;

namespace project.Common.Exceptions
{
    [Serializable]
    public class TokenValidationException : Exception
    {
        public TokenValidationException(string message)
            : base(message)
        {
        }
    }
}