﻿using travel.Data.Entities;

/// FILE TEMP FOR TEST FIRST DEMO
namespace travel.Data.Mapping
{
    public class MemberMap : BaseEntityMap<Member>
    {
        public MemberMap()
        {
            ToTable("Member");
            HasKey(o => o.Id);
            Property(o => o.Email).HasMaxLength(100);
            Property(o => o.PasswordSalt);            
            Property(o => o.HashedPassword);
            Property(o => o.FailedPasswordCount);
            Property(o => o.StatusId).IsRequired();

            Ignore(s => s.Role);
        }

    }
}