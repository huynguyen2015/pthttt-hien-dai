/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function() {
    function asignMerchantToAgentController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params) {
        $scope.commonService = commonService;
        function resetFilter() {
            $scope.paging = { currentPage: 1, totalPage: 6, pageSize: 4, currentItem: '0-20', totalItems: 120 };
            $scope.filter = { orderId: 1, keyword: "", pageSize: 4 };
        }
        resetFilter();

        $scope.closeClick = function() {
            $uibModalInstance.dismiss("cancel");            
        }
        
        function filterAgents() {
            httpService.sendGet(apiRoutes.managementAgentFilter, $scope.filter, $scope.setContentLoading).then(function (response) {
                if (response && response.Success === true) {
                    $scope.datas = response.Data ? response.Data.Items : [];
                    $scope.paging = commonService.preparePagination(response);
                    console.log(response);
                }
            });
        }

        filterAgents();

        $scope.searchPageChange = function (flag, pageNumber) {
            if (flag === 'pre') {
                if ($scope.paging.currentPage - 1 > 0)
                    $scope.paging.currentPage -= 1;
            } else if (flag === 'nxt') {
                if ($scope.paging.currentPage + 1 <= $scope.paging.totalPage)
                    $scope.paging.currentPage += 1;
            } else {
                $scope.paging.currentPage = pageNumber;
            }
            $scope.filter.pageNumber = $scope.paging.currentPage;
            filterAgents();
        };

        $scope.searchAgentClick = function () {
            filterAgents();
        };
   

        $scope.saveClick = function () {
            var result = validateDataBeforeSubmit();
            
            if (!isEmpty(result)) {
                alertify.logPosition("top right").error(result);                
                return;
            }
            var url = params ? apiRoutes.placeUpdate : apiRoutes.placeAdd;

            if (params) {
                Updated();
            } else {
                Inserted();
            }         

            httpService.sendPost(url, $scope.data).then(function (response) {
                if (response && response.Success === true) {
                    $uibModalInstance.close("success");
                }
            });
        }

    }

    angular.module('app').controller('AsignMerchantToAgentController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify', 'params', asignMerchantToAgentController]);
})();
