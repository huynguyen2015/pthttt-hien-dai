var express = require('express')
  , cors = require('cors')
  , app = express();

app.use(cors());

app.get('/api/v1/members/filter', function(req, res){
  
  res.status(200).send([{
    Id: 1,
  	Email: 'admin@gmail.com',
  	ErrorCount: 10,
  	Role: 1,
  	Status: 'Active',
  	CreateBy: 'Admin',
  	CreateAt: '',
  	UpdateBy: 'Admin',
    UpdateAt: ''
  },
  {
    Id: 2,
  	Email: 'admin1@gmail.com',
  	ErrorCount: 11,
  	Role: 2,
  	Status: 'Inactive',
  	CreateBy: 'Admin',
  	CreateAt: '',
  	UpdateBy: 'Admin',
    UpdateAt: ''
  }]);
});

app.listen(8081, function (){
	console.log('listening on port: 8081');
});