﻿using System;
using System.IO;

namespace travel.Service.Models
{
    public class UploadFileStream
    {        
        public UploadFileStream()
        {
            LocalFileName = string.Empty;
            FileName = string.Empty;
            ContentType = string.Empty;
            Extention = string.Empty;
            Name = string.Empty;
        }

        public UploadFileStream(Stream fileStream, string localFileName, string fileName, string name, string contentType, string extension)
        {
            FileStream = fileStream;
            LocalFileName = localFileName;
            FileName = fileName;
            ContentType = contentType;
            Extention = extension;
            Name = name;
        }

        public Stream FileStream { get; set; }
        public string LocalFileName { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public string Extention { get; set; }
        public string Name { get; set; }    
    }
}