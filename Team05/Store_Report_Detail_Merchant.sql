-- Sale
IF OBJECT_ID('sp_ReportSaleMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportSaleMerchants
GO

CREATE PROC sp_ReportSaleMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.[SaleAmount]) as [TotalAmount],
	       SUM(mcSummary.[SaleCount]) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END


-- Return
IF OBJECT_ID('sp_ReportReturnMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportReturnMerchants
GO

CREATE PROC sp_ReportReturnMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.[ReturnAmount]) as [TotalAmount],
	       SUM(mcSummary.[ReturnCount]) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END


-- Transaction
IF OBJECT_ID('sp_ReportTransactionMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportTransactionMerchants
GO

CREATE PROC sp_ReportTransactionMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.[TransactionAmount]) as [TotalAmount],
	       SUM(mcSummary.[TransactionCount]) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END



-- Key amount
IF OBJECT_ID('sp_ReportKeyMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportKeyMerchants
GO

CREATE PROC sp_ReportKeyMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.[KeyAmount]) as [TotalAmount],
	       SUM(mcSummary.[KeyedCount]) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END


-- ForeignCard
IF OBJECT_ID('sp_ReportForeignCardMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportForeignCardMerchants
GO

CREATE PROC sp_ReportForeignCardMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.ForeignCardAmount) as [TotalAmount],
	       SUM(mcSummary.ForeignCardCount) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END



-- Debit
IF OBJECT_ID('sp_ReportDebitMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportDebitMerchants
GO

CREATE PROC sp_ReportDebitMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.DebitAmount) as [TotalAmount],
	       SUM(mcSummary.DebitCount) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END



-- Visa
IF OBJECT_ID('sp_ReportVisaMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportVisaMerchants
GO

CREATE PROC sp_ReportVisaMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.VisaNetAmount) as [TotalAmount],
	       SUM(mcSummary.VisaTransactionCount) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END


-- Master
IF OBJECT_ID('sp_ReportMasterMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportMasterMerchants
GO

CREATE PROC sp_ReportMasterMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.MasterCardNetAmount) as [TotalAmount],
	       SUM(mcSummary.MasterCardTransactionCount) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END


-- Master
IF OBJECT_ID('sp_ReportOtherMerchants', 'P') IS NOT NULL
DROP PROC sp_ReportOtherMerchants
GO

CREATE PROC sp_ReportOtherMerchants
	@agentId bigint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SELECT mc.[MerchantNumber] as [MerchantNumber],
	       mc.[BackendProcessor] as [BackendProcessor],
	       mc.[MerchantName] as [MerchantName],
	       agent.[AgentName] as [AgentName],
	       mcType.[MerchantTypeName] as [MerchantTypeName],
	       region.[Name] as [Name],
	       SUM(mcSummary.OtherNetAmount) as [TotalAmount],
	       SUM(mcSummary.OtherTransactionCount) as [TotalCount]
	FROM dbo.TBL_MERCHANT mc
		 LEFT OUTER JOIN dbo.RPT_MERCHANT_SUMARY mcSummary on mcSummary.MerchantNumberId = mc.MerchantNumber
		 INNER JOIN dbo.TBL_MERCHANT_TYPE mcType ON mcType.Id = mc.MerchantTypeId
		 LEFT OUTER JOIN dbo.TBL_REGION region ON region.Id = mc.RegionId
		 LEFT OUTER JOIN dbo.TBL_AGENT agent ON agent.Id = mc.AgentId
	WHERE (CASE WHEN @agentId > 0 THEN @agentId ELSE mc.AgentId END) = mc.AgentId
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) >= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateFrom))
		 AND DATEADD(dd, 0, DATEDIFF(dd, 0, mcSummary.ReportDate)) <= DATEADD(dd, 0, DATEDIFF(dd, 0, @dateTo))
	GROUP BY mc.[MerchantNumber],
	         mc.[BackendProcessor],
	         mc.[MerchantName],
	         agent.[AgentName],
	         mcType.[MerchantTypeName],
	         region.[Name]
	ORDER by region.Name, mc.[MerchantName], mcType.[MerchantTypeName]		 
END