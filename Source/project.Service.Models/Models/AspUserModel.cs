namespace Click.Data.Entities
{
    using System;

    public class AspUserModel
    {
       
        public long AspUserId { get; set; }

        public long MemberId { get; set; }

        public short MaxConcurrentSession { get; set; }

        public DateTimeOffset LastLoginAt { get; set; }

        public DateTimeOffset LastActivityAt { get; set; }

        public int VisitCount { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        


    }
}
