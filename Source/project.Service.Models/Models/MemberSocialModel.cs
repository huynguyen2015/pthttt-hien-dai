using System;

namespace Click.Data.Entities
{
    public class MemberSocialModel
    {
        public long MemberId { get; set; }

        public short SocialTypeId { get; set; }

        public string AccessToken { get; set; }

        public string DisplayName { get; set; }

        public string SocialEmail { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }
        

        public long MemberSocialId { get; set; }

    }
}