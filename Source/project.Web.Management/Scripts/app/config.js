// put your config code here
(function() {
    'use strict';
    angular.module("app").config(['$translateProvider', function ($translateProvider) {
        $translateProvider
            .translations('en', translationsVI)
            .preferredLanguage('en')
            .useSanitizeValueStrategy('sanitize');
    }]);

    angular.module("app").config(function (blockUIConfig) {

        // Change the default overlay message
        blockUIConfig.message = 'Processing !';

        // Change the default delay to 100ms before the blocking is visible
        blockUIConfig.delay = 100;

        // Turn off auto block
        blockUIConfig.autoBlock = false;

    });

    //angular.module('app', ['ngMaterial']).config(function ($mdThemingProvider) {
    //    $mdThemingProvider.theme('default').primaryPalette('pink').accentPalette('orange');
    //});
})();