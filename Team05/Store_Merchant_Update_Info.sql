/*
Ngay			Phien ban		Tac gia						Mo ta
27/12/2016		1.0				Phan Thi Thu Thuy			Viet store cap nhat thong tin Merchant
29/12/2016		1.1				Phan Thi Thu Thuy			Chinh sua input parameter
*/

CREATE PROC sp_ActivateMerchant
	@merchantNumber CHAR(30),
	@merchantName [nvarchar](512),
	@status [int],
	@owner [nchar](10),
	@address [nvarchar](512),
	@city [nvarchar](512),
	@state [nvarchar](512),
	@zip [char](20),
	@phone [char](20),
	@fax [char](20),
	@email [char](50),
	@bankCardDBA [char](50),
	@agentId [bigint],
	@merchantTypeId [int],
	@regionId [int]
AS
BEGIN
	BEGIN TRAN

	DECLARE @count int
	DECLARE @success BIT
	SELECT @count = COUNT(Id) FROM [dbo].[TBL_MERCHANT] WHERE [dbo].[TBL_MERCHANT].[MerchantNumber] = @merchantNumber

	--@count = 0 => Khong ton tai Merchant co Id la @merchantId => Loi
	IF(@count > 0)
		BEGIN
			BEGIN TRY
				UPDATE [dbo].[TBL_MERCHANT]
				   SET [MerchantName] = @merchantName
					  ,[Status] = @status
					  ,[Owner] = @owner
					  ,[Address] = @address
					  ,[City] = @city
					  ,[State] = @state
					  ,[Zip] = @zip
					  ,[Phone] = @phone
					  ,[Fax] = @fax
					  ,[Email] = @email
					  ,[BankCardDBA] = @bankCardDBA
					  ,[AgentId] = @agentId
					  ,[MerchantTypeId] = @merchantTypeId
					  ,[RegionId] = @regionId
					  ,[UpdatedDate] = GETDATE()
				 WHERE [dbo].[TBL_MERCHANT].[MerchantNumber] = @merchantNumber

				SET @success = 1
			END TRY

			BEGIN CATCH
				--SELECT ERROR_NUMBER() AS ErrorNumber
				--	  ,ERROR_MESSAGE() AS ErrorMessage;  
				ROLLBACK TRAN
				SET @success = 0
			END CATCH;
		END
	ELSE
		SET @success = 0

	IF(@success = 1)
		COMMIT TRAN
END