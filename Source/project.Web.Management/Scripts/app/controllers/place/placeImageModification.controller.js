/**
 * @ngdoc overview
 * @name Role controller
 * @description 
 *
 */
(function() {
    function placeImageModificationController($scope, $http, $uibModalInstance, commonService, httpService, apiRoutes, $filter, alertify, params) {        
        $scope.data = {model :{
                Id: -1,
                Url: '',
                PlaceId: '',
                CreatedBy: '',
                CreatedAt: '',
                UpdatedBy:'',
                UpdatedAt: '',
                PlaceTitle: ''
            }, files:[]
        }               

        $scope.place = {};

        function GetPlace() {
            var filter = { orderId: 1, name: "" };
            httpService.sendGet(apiRoutes.placeFilter, filter).then(function (response) {
                console.log(response);
                if (response && response.Success === true) {
                    $scope.place = response.Data ? response.Data.Items : [];
                }
            });
        };

        var oldPlaceId = -1;
        $scope.init = function () {            
            GetPlace();
            if (!params) {
                return;
            }
          
            $scope.data.model = params;
            oldPlaceId = $scope.data.model.PlaceId;
            console.log($scope.data.model);

        }
        

        $scope.closeClick = function() {
            $uibModalInstance.dismiss("cancel");
            $scope.data.model.PlaceId = oldPlaceId;
        }

        $scope.saveClick = function () {
            var result = validateDataBeforeSubmit();
            
            if (!isEmpty(result)) {
                alertify.logPosition("top right").error(result);                
                return;
            }
            var url = params ? apiRoutes.placeImageUpdateWithMedia : apiRoutes.placeImageAddWithMedia; // sao phai chia ra nhu vay?

            if (params) {
                Updated();
            } else {
                Inserted();
            }         

            console.log($scope.data);
            httpService.sendPostMedia(url, $scope.data).then(function (response) {
                if (response && response.Success === true) {
                    $uibModalInstance.close("success");
                }
            });
        }

        function validateDataBeforeSubmit() {
            var errorMessage = '';
            var placeimage = $scope.data.model;
            if (placeimage.PlaceId == "")
                errorMessage = "Place is not empty";         
            return errorMessage;
        }

        function Updated() {
            $scope.data.model.UpdatedAt = new Date();
            $scope.data.model.UpdatedBy = 1;
        }

        function Inserted() {
            $scope.data.model.CreatedAt = $scope.data.model.UpdatedAt = new Date();
            ////TODO: change logged in account id
            $scope.data.model.CreatedBy = $scope.data.model.UpdatedBy = 1;
        }
    }

    angular.module('app').controller('PlaceImageModificationController', ['$scope', '$http', '$uibModalInstance', 'commonService', 'HttpService', 'API_ROUTES', '$filter', 'alertify', 'params', placeImageModificationController]);
})();
