﻿using project.Common;
using project.Common.Exceptions;
using project.Common.Logging;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Interfaces;
using System;
using System.Linq;

namespace project.Repository.Repositories
{
    public class MerchantTypeRepository : EfRepository<TblMerchantType>, IMerchantTypeRepository
    {
        public MerchantTypeRepository(TravelContext context, IDateTime datetime, ILogManager logManager)
            : base(context, datetime, logManager)
        {
        }

        public QueryResult<TblMerchantType> FilterMerchantType(MerchantTypeDataRequest dataRequest)
        {
            try
            {
                // Todo: defined Error code
                var startIndex = ResultsPagingUtility.CalculateStartIndex(
                dataRequest.PageNumber, dataRequest.PageSize);

                //Parameter for get list
                var query = Table;
                //query = query.Where(obj => obj.MerchantTypeTypeId == dataRequest.MerchantTypeTypeId);
                
                if (dataRequest.orderId == 1)
                    query = query.OrderBy(c => c.Id);
                else if (dataRequest.orderId == 2)
                    query = query.OrderByDescending(c => c.Id);
                //else if (dataRequest.orderId == 3)
                //    query = query.OrderBy(c => c.MerchantTypeNumber);
                //else if (dataRequest.orderId == 4)
                //    query = query.OrderByDescending(c => c.MerchantTypeNumber);
                //else if (dataRequest.orderId == 5)
                //    query = query.OrderBy(c => c.MerchantTypeName);
                //else if (dataRequest.orderId == 6)
                //    query = query.OrderByDescending(c => c.MerchantTypeName);
                //else if (dataRequest.orderId == 7)
                //    query = query.OrderBy(c => c.Phone);
                //else if (dataRequest.orderId == 8)
                //    query = query.OrderByDescending(c => c.Phone);
                //else if (dataRequest.orderId == 9)
                //    query = query.OrderBy(c => c.MerchantTypeType.MerchantTypeTypeName);
                //else if (dataRequest.orderId == 10)
                //    query = query.OrderByDescending(c => c.MerchantTypeType.MerchantTypeTypeName);
                //else if (dataRequest.orderId == 11)
                //    query = query.OrderBy(c => c.MerchantType.Name);
                //else if (dataRequest.orderId == 12)
                //    query = query.OrderByDescending(c => c.MerchantType.Name);
                //else if (dataRequest.orderId == 13)
                //    query = query.OrderBy(c => c.CreatedAt);
                //else if (dataRequest.orderId == 14)
                //    query = query.OrderByDescending(c => c.CreatedAt);
                //parameter for get total count               
                var totalCount = query.Any() ? query.Count() : 0;
                var results = query.Skip(startIndex).Take(dataRequest.PageSize).ToList();
                return new QueryResult<TblMerchantType>(results, totalCount, dataRequest.PageSize);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("System could not filter member. Environment: {0}. Detail: {1}", Environment.NewLine, ex);
                throw new InternalException(1111, ex.ToString());
            }
        }
    }
}