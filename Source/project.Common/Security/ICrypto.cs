﻿using System;

namespace project.Common.Security
{
    public interface ICrypto
    {
        string GenerateSalt();
        string EncryptPassword(string password, string salt);
        Guid GenerateGuid();
    }
}
