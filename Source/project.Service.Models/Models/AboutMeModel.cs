namespace Click.Data.Entities
{

    public  class AboutMeModel
    {
        public long AboutMeId { get; set; }

        public short? OutGoingId { get; set; }

        public short? ChildrenId { get; set; }

        public short? PetId { get; set; }

        public short? EducationId { get; set; }

        public short? EmploymentId { get; set; }

        public short? Industry { get; set; }

        public short? HairTypeId { get; set; }

        public short? EyeTypeId { get; set; }

        public short? BodyTypeId { get; set; }

        public short? DrinkId { get; set; }

        public short? ExcersiseId { get; set; }

        public short? SmokeId { get; set; }

        public short? HeightId { get; set; }
    }
}
