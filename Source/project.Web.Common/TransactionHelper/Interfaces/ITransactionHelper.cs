﻿using System.Web.Http.Filters;

namespace project.Web.Common.TransactionHelper
{
    public interface ITransactionHelper
    {
        void Commit();
        void Rollback();
        void Dispose();
    }
}
