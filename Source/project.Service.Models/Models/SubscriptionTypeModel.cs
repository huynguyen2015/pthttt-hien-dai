using System;

namespace Click.Data.Entities
{
    public class SubscriptionTypeModel
    {
       
        public short SubscriptionTypeId { get; set; }

        public string Name { get; set; }

        public int NumberOfDay { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        
    }
}
