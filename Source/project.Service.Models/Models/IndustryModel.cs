using System;

namespace Click.Data.Entities
{
    public class IndustryModel
    {
        public short IndustryId { get; set; }

        public string Name { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short AboutMeCategoryId { get; set; }

        
    }
}