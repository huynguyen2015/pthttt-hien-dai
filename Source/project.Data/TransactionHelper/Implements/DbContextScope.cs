﻿using System.Linq;

namespace project.Data.TransactionHelper
{
    public class DbContextScope : IDbContextScope
    {
        private readonly TravelContext _context;
        
        public DbContextScope(TravelContext context)
        {
            _context = context;
        }


        public void Commit()
        {
            _context.SaveChanges();
        }
        public void Rollback()
        {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }
    }
}
    