using System;

namespace Click.Data.Entities
{
    public class MemberOptionModel
    {
        public short NotifyOptionId { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public bool IsPush { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        
    }
}