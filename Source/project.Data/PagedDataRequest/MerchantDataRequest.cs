﻿using System;

namespace project.Data.PagedDataRequest
{
    public class MerchantDataRequest : PagedDataRequest
    {
        public MerchantDataRequest()         
        {

        }
        public MerchantDataRequest(int pageNumber, int pageSize)
            : base(pageNumber, pageSize)
        {
        
        }

        public MerchantDataRequest(int pageNumber, int pageSize, DateTime lastTime)
            : base(pageNumber, pageSize, lastTime)
        {
        
        }


        public MerchantDataRequest(int pageNumber, int pageSize, int orderId)
            : base(pageNumber, pageSize, orderId)
        {
           
        }

        public int OrderId { get; set; }
        public int MerchantTypeId { get; set; }
        public int RegionId { get; set; }
        public int AgentId { get; set; }
        public string Name { get; set; }
    }
}