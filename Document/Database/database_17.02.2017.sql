USE [master]
GO
/****** Object:  Database [CardProcessing]    Script Date: 17/02/2017 7:21:18 CH ******/
CREATE DATABASE [CardProcessing]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CardProcessing', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CardProcessing.mdf' , SIZE = 7232KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CardProcessing_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\CardProcessing_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CardProcessing] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CardProcessing].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CardProcessing] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CardProcessing] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CardProcessing] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CardProcessing] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CardProcessing] SET ARITHABORT OFF 
GO
ALTER DATABASE [CardProcessing] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CardProcessing] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CardProcessing] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CardProcessing] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CardProcessing] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CardProcessing] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CardProcessing] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CardProcessing] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CardProcessing] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CardProcessing] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CardProcessing] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CardProcessing] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CardProcessing] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CardProcessing] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CardProcessing] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CardProcessing] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CardProcessing] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CardProcessing] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CardProcessing] SET RECOVERY FULL 
GO
ALTER DATABASE [CardProcessing] SET  MULTI_USER 
GO
ALTER DATABASE [CardProcessing] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CardProcessing] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CardProcessing] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CardProcessing] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CardProcessing', N'ON'
GO
USE [CardProcessing]
GO
/****** Object:  StoredProcedure [dbo].[Ins_INF_MESSAGE_CountEmail]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ins_INF_MESSAGE_CountEmail] @createdUser INT, @count INT OUTPUT 
AS
BEGIN
	SELECT @count = COUNT(1) 
	FROM INF_MESSAGE WITH (NOLOCK)
	WHERE CreatedUser = @createdUser
	AND IsRead = 0 --EMAIL CHUA DOC 
	AND FromAccountId <> @createdUser
END 

GO
/****** Object:  StoredProcedure [dbo].[Ins_INF_MESSAGE_Delete]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ins_INF_MESSAGE_Delete] @id bigint, @createdUser INT
AS
BEGIN
	DELETE INF_MESSAGE WHERE ID = @Id AND (FromAccountId = @CreatedUser OR ToAccountId = @CreatedUser)
END 
GO
/****** Object:  StoredProcedure [dbo].[Ins_INF_MESSAGE_GetListEmail]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ins_INF_MESSAGE_GetListEmail] @creatUser INT, @type INT
AS 
BEGIN
	IF @TYPE = 1 --EMAIL CHUA DOC 
	BEGIN
		SELECT [Subject], Content, FromAccountId, ToAccountId, ParentId, CreatedAt
		FROM dbo.INF_MESSAGE WITH(NOLOCK)
		WHERE IsRead = 0 --EMAIL CHUA DOC
		ORDER BY CreatedAt
	END 
	ELSE IF @TYPE = 2 --EMAIL DA DOC 
	BEGIN
		SELECT [Subject], Content, FromAccountId, ToAccountId, ParentId, CreatedAt
		FROM dbo.INF_MESSAGE WITH(NOLOCK)
		WHERE IsRead = 1 --EMAIL DA DOC 
		ORDER BY CreatedAt
	END
END

GO
/****** Object:  StoredProcedure [dbo].[Ins_INF_MESSAGE_Insert]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ins_INF_MESSAGE_Insert] @Subject NVARCHAR(1024), @Content NVARCHAR(MAX), @FromAccountId BIGINT, 
						   @ToAccountId BIGINT, @ParentId INT, @CreatedUser INT
AS
BEGIN
	SET DATEFORMAT DMY
	INSERT INTO INF_MESSAGE([Subject],Content,IsRead,FromAccountId,ToAccountId,ParentId,CreatedAt,CreatedUser,UpdatedAt, UpdatedUser, StatusId)
	VALUES(@Subject, @Content, 0, @FromAccountId, @ToAccountId, @ParentId, GETDATE(), @CreatedUser, GETDATE(), @CreatedUser, 0)
END

GO
/****** Object:  StoredProcedure [dbo].[Ins_INF_MESSAGE_UpdateStatus]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	4. Lấy số lượng mail chưa đọc của 1 userid
*/

/*
	6. Cập nhật trạng thái mail của user 
*/
CREATE PROCEDURE [dbo].[Ins_INF_MESSAGE_UpdateStatus] @id bigint , @createdUser INT
AS
BEGIN
	UPDATE INF_MESSAGE SET IsRead = 1 --Email Da doc
	WHERE Id = @id 
	AND StatusId = 0 --EMAIL DANG HOAT DONG
	AND CreatedUser = @createdUser
END

GO
/****** Object:  StoredProcedure [dbo].[Ins_TransactionDetail_GetAmount]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	Author: LoanHT
	Date: 16/2/2017
	Des: Tổng hợp dữ liệu từ bảng transactiondetail --> data summary
	Test:
	Ins_TransactionDetail_GetAmount

	SELECT *FROM dbo.RPT_MERCHANT_SUMARY
*/


CREATE PROCEDURE [dbo].[Ins_TransactionDetail_GetAmount]
AS
    BEGIN
		SET NOCOUNT ON

		DECLARE @reportDate DATETIME = GETDATE()
		DECLARE @merchantId INT
        DECLARE @payType INT ,
            @sumSale MONEY ,
			@sumSaleCount INT ,

            @sumReturn MONEY ,		
			@sumReturnCount INT ,	

            @sumTransaction MONEY ,
			@sumTransactionCount INT ,

            @sumKey MONEY ,
			@sumKeyCount INT ,

            @sumForeign MONEY ,
			@sumForeignCount INT ,

            @sumDebit MONEY ,
			@sumDebitCount INT ,

            @sumVisa MONEY ,
			@sumVisaCount INT ,

            @sumMaster MONEY ,
			@sumMasterCount INT ,

            @sumOther MONEY,
			@sumOtherCount INT

		DECLARE @tblSales TABLE(MerchantId BIGINT, Amount MONEY, Total INT, PayType INT)
		--DECLARE @tblReturns TABLE(RowNumber INT, MerchantId BIGINT, Amount MONEY, Total INT, PayType INT)
		DECLARE @tblMerchants TABLE(RowNumber INT, MerchantId BIGINT)

		-- thong ke cho bang sales
		INSERT INTO @tblSales
		SELECT t.MerchantId, SUM(t.TransactionAmount), COUNT(1) SaleCount, t.PayTypeId
		FROM dbo.RPT_TRANSACTION_DETAIL t WITH(NOLOCK)
		GROUP BY t.MerchantId, t.PayTypeId
		ORDER BY t.MerchantId DESC

		INSERT INTO @tblMerchants
		SELECT ROW_NUMBER() OVER(ORDER BY t.MerchantId), t.MerchantId FROM (SELECT DISTINCT MerchantId
		FROM @tblSales) t

		--SELECT *FROM @tblMerchants

		-- thong ke cho bang returns
		--INSERT INTO @tblReturns
		--SELECT ROW_NUMBER() OVER(ORDER BY t.MerchantId DESC), t.MerchantId, SUM(t.TransactionAmount), COUNT(1) SaleCount, t.PayTypeId
		--FROM dbo.RPT_TRANSACTION_DETAIL t WITH(NOLOCK)
		--WHERE t.TransactionCode = 'R' 
		--GROUP BY t.MerchantId, t.PayTypeId
		
		--tao bien de lap
		DECLARE @i INT = 1
		DECLARE @merchantCount INT
		SELECT @merchantCount = COUNT(1) FROM @tblMerchants

		WHILE @i <= @merchantCount
		BEGIN
			--set bang 0 de qua vong lap khac khong giu lai gia tri cu
			SET @sumSale = 0
			SET @sumSaleCount = 0
			SET @sumReturn = 0
			SET @sumReturnCount = 0
			SET @sumTransaction = 0
			SET @sumTransactionCount = 0
			SET @sumForeign = 0
			SET @sumForeignCount = 0
			SET @sumDebit = 0
			SET @sumDebitCount = 0
			SET @sumVisa = 0
			SET @sumVisaCount = 0
			SET @sumMaster = 0
			SET @sumMasterCount = 0
			SET @sumKey = 0
			SET @sumKeyCount = 0
			SET @sumOther = 0
			SET @sumOtherCount = 0

			SELECT @merchantId = MerchantId FROM @tblMerchants
			WHERE RowNumber = @i

		    --lay tong tien, so luong cua sale
			SET @payType = 1
			SELECT @sumSale = Amount, @sumSaleCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--return
			SET @payType = 2
			SELECT @sumReturn = Amount, @sumReturnCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--transaction
			SET @payType = 3
			SELECT @sumTransaction = Amount, @sumTransactionCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--transaction
			SET @payType = 4
			SELECT @sumKey = Amount, @sumKeyCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--transaction
			SET @payType = 5
			SELECT @sumForeign = Amount, @sumForeignCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--debit
			SET @payType = 6
			SELECT @sumDebit = Amount, @sumDebitCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--visa
			SET @payType = 7
			SELECT @sumVisa = Amount, @sumVisaCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--master
			SET @payType = 8
			SELECT @sumMaster = Amount, @sumMasterCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--master
			SET @payType = 9
			SELECT @sumOther = Amount, @sumOtherCount = Total
			FROM @tblSales
			WHERE MerchantId = @merchantId AND PayType = @payType

			--set # null
			SET @sumSale = ISNULL(@sumSale, 0)
			SET @sumSaleCount = ISNULL(@sumSaleCount, 0)
			SET @sumReturn = ISNULL(@sumReturn, 0)
			SET @sumReturnCount = ISNULL(@sumReturnCount, 0)
			SET @sumTransaction = ISNULL(@sumTransaction, 0)
			SET @sumTransactionCount = ISNULL(@sumTransactionCount, 0)
			SET @sumForeign = ISNULL(@sumForeign, 0)
			SET @sumForeignCount = ISNULL(@sumForeignCount, 0)
			SET @sumDebit = ISNULL(@sumDebit, 0)
			SET @sumDebitCount = ISNULL(@sumDebitCount, 0)
			SET @sumVisa = ISNULL(@sumVisa, 0)
			SET @sumVisaCount = ISNULL(@sumVisaCount, 0)
			SET @sumMaster = ISNULL(@sumMaster, 0)
			SET @sumMasterCount = ISNULL(@sumMasterCount, 0)
			SET @sumKey = ISNULL(@sumKey, 0)
			SET @sumKeyCount = ISNULL(@sumKeyCount, 0)
			SET @sumOther = ISNULL(@sumOther, 0)
			SET @sumOtherCount = ISNULL(@sumOtherCount, 0)

			INSERT INTO dbo.RPT_MERCHANT_SUMARY
			        ( ReportDate ,
			          MerchantId ,
			          SaleAmount ,
			          SaleCount ,
			          ReturnAmount ,
			          ReturnCount ,
			          TransactionAmount ,
			          TransactionCount ,
			          KeyAmount ,
			          KeyedCount ,
			          ForeignCardAmount ,
			          ForeignCardCount ,
			          DebitAmount ,
			          DebitCount ,
			          VisaNetAmount ,
			          VisaTransactionCount ,
			          MasterCardNetAmount ,
			          MasterCardTransactionCount ,
			          AmericanExpressNetAmount ,
			          AmericanExpressTransactionCount ,
			          DiscoverNetAmount ,
			          DiscoverNetCount ,
			          DebitCardNetAmount ,
			          DebitCardTransactionCount ,
			          OtherNetAmount ,
			          OtherTransactionCount ,
			          CreatedAt ,
			          CreatedUser ,
			          UpdatedAt ,
			          UpdatedUser ,
			          StatusId
			        )
			VALUES  ( @reportDate , -- ReportDate - datetime
			          @merchantId , -- MerchantId - bigint
			          @sumSale , -- SaleAmount - decimal
			          @sumSaleCount , -- SaleCount - bigint
			          @sumReturn , -- ReturnAmount - decimal
			          @sumReturnCount , -- ReturnCount - bigint
			          @sumTransaction , -- TransactionAmount - decimal
			          @sumTransactionCount , -- TransactionCount - bigint
			          @sumKey , -- KeyAmount - decimal
			          @sumKeyCount , -- KeyedCount - bigint
			          @sumForeign , -- ForeignCardAmount - decimal
			          @sumForeignCount , -- ForeignCardCount - bigint
			          @sumDebit , -- DebitAmount - decimal
			          @sumDebitCount , -- DebitCount - bigint
			          @sumVisa , -- VisaNetAmount - decimal
			          @sumVisaCount , -- VisaTransactionCount - bigint
			          @sumMaster , -- MasterCardNetAmount - decimal
			          @sumMasterCount , -- MasterCardTransactionCount - bigint
			          0 , -- AmericanExpressNetAmount - decimal
			          0 , -- AmericanExpressTransactionCount - bigint
			          0 , -- DiscoverNetAmount - decimal
			          0 , -- DiscoverNetCount - bigint
			          0 , -- DebitCardNetAmount - decimal
			          0 , -- DebitCardTransactionCount - bigint
			          @sumOther , -- OtherNetAmount - decimal
			          @sumOtherCount , -- OtherTransactionCount - bigint
			          @reportDate , -- CreatedAt - datetime
			          1 , -- CreatedUser - bigint
			          @reportDate , -- UpdatedAt - datetime
			          1 , -- UpdatedUser - bigint
			          1  -- StatusId - smallint
			        )

			SET @i = @i + 1
		END

		
		
     
    END;

GO
/****** Object:  Table [dbo].[INF_MESSAGE]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INF_MESSAGE](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Subject] [nvarchar](max) NULL,
	[Content] [nvarchar](max) NULL,
	[IsRead] [bit] NOT NULL,
	[FromAccountId] [bigint] NOT NULL,
	[ToAccountId] [bigint] NOT NULL,
	[ParentId] [bigint] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.INF_MESSAGE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRT_MERCHANT_PAYTYPE]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRT_MERCHANT_PAYTYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DataText] [nvarchar](50) NULL,
 CONSTRAINT [PK_PRT_MERCHANT_DATATYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_MERCHANT_DATASUMARY]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPT_MERCHANT_DATASUMARY](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DateType] [int] NOT NULL,
	[TotalAmount] [decimal](18, 2) NOT NULL,
	[TotalCount] [bigint] NOT NULL,
	[MerchantId] [bigint] NOT NULL,
	[DateReport] [datetime] NOT NULL,
	[PayType] [int] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.RPT_MERCHANT_DATASUMARY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_MERCHANT_SUMARY]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RPT_MERCHANT_SUMARY](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[MerchantId] [bigint] NOT NULL,
	[SaleAmount] [decimal](18, 2) NOT NULL,
	[SaleCount] [bigint] NOT NULL,
	[ReturnAmount] [decimal](18, 2) NOT NULL,
	[ReturnCount] [bigint] NOT NULL,
	[TransactionAmount] [decimal](18, 2) NOT NULL,
	[TransactionCount] [bigint] NOT NULL,
	[KeyAmount] [decimal](18, 2) NOT NULL,
	[KeyedCount] [bigint] NOT NULL,
	[ForeignCardAmount] [decimal](18, 2) NOT NULL,
	[ForeignCardCount] [bigint] NOT NULL,
	[DebitAmount] [decimal](18, 2) NOT NULL,
	[DebitCount] [bigint] NOT NULL,
	[VisaNetAmount] [decimal](18, 2) NOT NULL,
	[VisaTransactionCount] [bigint] NOT NULL,
	[MasterCardNetAmount] [decimal](18, 2) NOT NULL,
	[MasterCardTransactionCount] [bigint] NOT NULL,
	[AmericanExpressNetAmount] [decimal](18, 2) NOT NULL,
	[AmericanExpressTransactionCount] [bigint] NOT NULL,
	[DiscoverNetAmount] [decimal](18, 2) NOT NULL,
	[DiscoverNetCount] [bigint] NOT NULL,
	[DebitCardNetAmount] [decimal](18, 2) NOT NULL,
	[DebitCardTransactionCount] [bigint] NOT NULL,
	[OtherNetAmount] [decimal](18, 2) NOT NULL,
	[OtherTransactionCount] [bigint] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.RPT_MERCHANT_SUMARY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RPT_TRANSACTION_DETAIL]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TRANSACTION_DETAIL](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantId] [bigint] NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[FileSource] [varchar](500) NULL,
	[TerminalNumber] [varchar](50) NOT NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[TransactionCode] [varchar](50) NULL,
	[CardTypeCode] [varchar](50) NULL,
	[TransactionAmount] [decimal](18, 2) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionTime] [time](7) NOT NULL,
	[KeyedEntry] [bit] NOT NULL,
	[AuthorizationNumber] [varchar](500) NULL,
	[ReportTime] [datetime] NOT NULL,
	[Description] [varchar](500) NULL,
	[AccountNumber] [varchar](50) NULL,
	[FirstTwelveAccountNumber] [varchar](50) NULL,
	[CountryId] [int] NOT NULL,
	[PayTypeId] [int] NOT NULL,
	[CreatedAt] [datetime] NULL,
	[CreatedUser] [bigint] NULL,
	[UpdatedAt] [datetime] NULL,
	[UpdatedUser] [bigint] NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.RPT_TRANSACTION_DETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RPT_TRANSACTION_DETAIL_ERROR]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RPT_TRANSACTION_DETAIL_ERROR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RowNumber] [int] NULL,
	[FileSource] [varchar](500) NULL,
	[ErrorCode] [int] NULL,
	[ErrorColumn] [int] NULL,
 CONSTRAINT [PK_RPT_TRANSACTION_DETAIL_ERROR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYS_ACCOUNT]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountTypeId] [int] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[Username] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[Salt] [nvarchar](max) NULL,
	[IsActive] [bit] NOT NULL,
	[IsHash] [bit] NOT NULL,
	[RoleId] [smallint] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.SYS_ACCOUNT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ACCOUNT_SESSION]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT_SESSION](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SessionToken] [nvarchar](max) NULL,
	[ExpirationAt] [datetime] NOT NULL,
	[AccountId] [bigint] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.SYS_ACCOUNT_SESSION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_ACCOUNT_TYPE]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_ACCOUNT_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[Role] [int] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.SYS_ACCOUNT_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SYS_LOG]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_LOG](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](max) NULL,
	[ReadDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[Version] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.SYS_LOG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_AGENT]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_AGENT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AgentCode] [nvarchar](max) NULL,
	[AgentName] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.TBL_AGENT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_ATTRIBUTE]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_ATTRIBUTE](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Entity] [int] NOT NULL,
	[KeyNumber] [bigint] NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
	[IsPrimary] [bit] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.TBL_ATTRIBUTE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_MERCHANT]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MERCHANT](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[MerchantNumber] [nvarchar](max) NULL,
	[BackendProcessor] [bigint] NOT NULL,
	[MerchantName] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[Owner] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](max) NULL,
	[State] [nvarchar](max) NULL,
	[Zip] [nvarchar](max) NULL,
	[Phone] [nvarchar](max) NULL,
	[Fax] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[ApprovalDate] [datetime] NOT NULL,
	[CloseDate] [datetime] NULL,
	[BankCardDBA] [nvarchar](max) NULL,
	[FistActiveDate] [datetime] NOT NULL,
	[LastActiveDate] [datetime] NULL,
	[AgentId] [bigint] NOT NULL,
	[MerchantTypeId] [int] NOT NULL,
	[RegionId] [int] NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.TBL_MERCHANT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_MERCHANT_TYPE]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MERCHANT_TYPE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MerchantTypeName] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.TBL_MERCHANT_TYPE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_REGION]    Script Date: 17/02/2017 7:21:18 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_REGION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[CreatedUser] [bigint] NOT NULL,
	[UpdatedAt] [datetime] NOT NULL,
	[UpdatedUser] [bigint] NOT NULL,
	[StatusId] [smallint] NOT NULL,
 CONSTRAINT [PK_dbo.TBL_REGION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ON 

INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (1, N'Sale')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (2, N'Return')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (3, N'Transaction')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (4, N'Key')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (5, N'Foreign')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (6, N'Debit')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (7, N'Visa')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (8, N'Master')
INSERT [dbo].[PRT_MERCHANT_PAYTYPE] ([Id], [DataText]) VALUES (9, N'Other')
SET IDENTITY_INSERT [dbo].[PRT_MERCHANT_PAYTYPE] OFF
SET IDENTITY_INSERT [dbo].[RPT_MERCHANT_SUMARY] ON 

INSERT [dbo].[RPT_MERCHANT_SUMARY] ([Id], [ReportDate], [MerchantId], [SaleAmount], [SaleCount], [ReturnAmount], [ReturnCount], [TransactionAmount], [TransactionCount], [KeyAmount], [KeyedCount], [ForeignCardAmount], [ForeignCardCount], [DebitAmount], [DebitCount], [VisaNetAmount], [VisaTransactionCount], [MasterCardNetAmount], [MasterCardTransactionCount], [AmericanExpressNetAmount], [AmericanExpressTransactionCount], [DiscoverNetAmount], [DiscoverNetCount], [DebitCardNetAmount], [DebitCardTransactionCount], [OtherNetAmount], [OtherTransactionCount], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (1, CAST(0x0000A71D013ADCEB AS DateTime), 1, CAST(20000.00 AS Decimal(18, 2)), 1, CAST(18000.00 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), 0, CAST(5060500.00 AS Decimal(18, 2)), 8, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0x0000A71D013ADCEB AS DateTime), 1, CAST(0x0000A71D013ADCEB AS DateTime), 1, 1)
INSERT [dbo].[RPT_MERCHANT_SUMARY] ([Id], [ReportDate], [MerchantId], [SaleAmount], [SaleCount], [ReturnAmount], [ReturnCount], [TransactionAmount], [TransactionCount], [KeyAmount], [KeyedCount], [ForeignCardAmount], [ForeignCardCount], [DebitAmount], [DebitCount], [VisaNetAmount], [VisaTransactionCount], [MasterCardNetAmount], [MasterCardTransactionCount], [AmericanExpressNetAmount], [AmericanExpressTransactionCount], [DiscoverNetAmount], [DiscoverNetCount], [DebitCardNetAmount], [DebitCardTransactionCount], [OtherNetAmount], [OtherTransactionCount], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (2, CAST(0x0000A71D013ADCEB AS DateTime), 2, CAST(22550000.00 AS Decimal(18, 2)), 1, CAST(0.00 AS Decimal(18, 2)), 0, CAST(15000.00 AS Decimal(18, 2)), 1, CAST(2863770.00 AS Decimal(18, 2)), 8, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0x0000A71D013ADCEB AS DateTime), 1, CAST(0x0000A71D013ADCEB AS DateTime), 1, 1)
INSERT [dbo].[RPT_MERCHANT_SUMARY] ([Id], [ReportDate], [MerchantId], [SaleAmount], [SaleCount], [ReturnAmount], [ReturnCount], [TransactionAmount], [TransactionCount], [KeyAmount], [KeyedCount], [ForeignCardAmount], [ForeignCardCount], [DebitAmount], [DebitCount], [VisaNetAmount], [VisaTransactionCount], [MasterCardNetAmount], [MasterCardTransactionCount], [AmericanExpressNetAmount], [AmericanExpressTransactionCount], [DiscoverNetAmount], [DiscoverNetCount], [DebitCardNetAmount], [DebitCardTransactionCount], [OtherNetAmount], [OtherTransactionCount], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (3, CAST(0x0000A71D013ADCEB AS DateTime), 3, CAST(0.00 AS Decimal(18, 2)), 0, CAST(320000.00 AS Decimal(18, 2)), 1, CAST(18000.00 AS Decimal(18, 2)), 1, CAST(4462500.00 AS Decimal(18, 2)), 8, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0.00 AS Decimal(18, 2)), 0, CAST(0x0000A71D013ADCEB AS DateTime), 1, CAST(0x0000A71D013ADCEB AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[RPT_MERCHANT_SUMARY] OFF
SET IDENTITY_INSERT [dbo].[RPT_TRANSACTION_DETAIL] ON 

INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (1, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1234', CAST(0x0000AB21009D6110 AS DateTime), N'S', N'VS', CAST(20000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700A41F5D480000 AS Time), 0, N'DEWF', CAST(0x0000A70F008C1360 AS DateTime), N'', N'sfgfvsvds', N'bfqwertyuiop', 84, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (2, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'2345', CAST(0x0000AB71008CA000 AS DateTime), N'S', N'DB', CAST(155000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07006850E84F0000 AS Time), 0, N'RBSFBSF', CAST(0x0000A70F008C1360 AS DateTime), N'', N'gfdvfdvfd', N'asdfghjklzxc', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (3, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'3456', CAST(0x0000A86C00BAC480 AS DateTime), N'R', N'AE', CAST(320000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700B2DB1E530000 AS Time), 0, N'HAGAFS', CAST(0x0000A70F008C1360 AS DateTime), N'', N'bgrdfsfrg', N'qazwsxedcrfv', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (4, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'4567', CAST(0x0000AC3100DC0050 AS DateTime), N'S', N'VS', CAST(180000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07005A3708570000 AS Time), 0, N'SGRRET', CAST(0x0000A70F008C1360 AS DateTime), N'', N'fdbfdbfgr', N'tgbyhnujmikl', 14, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (5, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'5678', CAST(0x0000AE4100EA0240 AS DateTime), N'R', N'DB', CAST(1500500.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07003411995D0000 AS Time), 1, N'JRYJR', CAST(0x0000A70F008C1360 AS DateTime), N'', N'rwgwrsfbhrsgr', N'poiuytrewqas', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (6, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'6789', CAST(0x0000AB21009D6110 AS DateTime), N'R', N'OT', CAST(30000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07006A11903F0000 AS Time), 1, N'SGAWR', CAST(0x0000A70F008C1360 AS DateTime), N'', N'fagfvfsvsf', N'lkjhgfdsazxc', 12, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (7, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'9021', CAST(0x0000AB71008CA000 AS DateTime), N'S', N'VS', CAST(18000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700BE90754A0000 AS Time), 1, N'SGSFV', CAST(0x0000A70F008C1360 AS DateTime), N'', N'hdahtehteh', N'mnbvcxzasdfg', 84, 2, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (8, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'9987', CAST(0x0000A86C00BAC480 AS DateTime), N'S', N'DB', CAST(195000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700A8E76F4B0000 AS Time), 0, N'SFBFSB', CAST(0x0000A70F008C1360 AS DateTime), N'', N'fhkfhhhtdg', N'hjklpoiuytre', 53, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (9, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1122', CAST(0x0000AC3100DC0050 AS DateTime), N'S', N'MS', CAST(2350000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07000E4848520000 AS Time), 0, N'THAETH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'htejtghnfgn', N'wqasderfgtyh', 16, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (10, 1, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'2233', CAST(0x0000AE4100EA0240 AS DateTime), N'R', N'FR', CAST(330000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700448E02580000 AS Time), 0, N'THSTB', CAST(0x0000A70F008C1360 AS DateTime), N'', N'hethethb', N'qasertnbvcdg', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (11, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'3344', CAST(0x0000AB21009D6110 AS DateTime), N'S', N'AE', CAST(180000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700A41F5D480000 AS Time), 0, N'GWRGWR', CAST(0x0000A70F008C1360 AS DateTime), N'', N'dzbdjyrjyh', N'zaqxswcdevfr', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (12, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'4455', CAST(0x0000AB71008CA000 AS DateTime), N'S', N'DN', CAST(225000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07006850E84F0000 AS Time), 0, N'WGWRV', CAST(0x0000A70F008C1360 AS DateTime), N'', N'mfhmsrhte', N'bgrnhtmjykui', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (13, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'5566', CAST(0x0000A86C00BAC480 AS DateTime), N'R', N'VS', CAST(990000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700B2DB1E530000 AS Time), 0, N'BDGBGD', CAST(0x0000A70F008C1360 AS DateTime), N'', N'zfhtgmhf', N'lpoikjuyhgtr', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (14, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'6677', CAST(0x0000AC3100DC0050 AS DateTime), N'S', N'MS', CAST(15000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07005A3708570000 AS Time), 0, N'GHSGHG', CAST(0x0000A70F008C1360 AS DateTime), N'', N'dhtdgndz', N'fdewsaqzxcvb', 84, 3, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (15, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'7788', CAST(0x0000AE4100EA0240 AS DateTime), N'R', N'DB', CAST(13500.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07003411995D0000 AS Time), 1, N'GSHGBGD', CAST(0x0000A70F008C1360 AS DateTime), N'', N'zdhdzfntrhetde', N'mnhjklpoiuyt', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (16, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'8899', CAST(0x0000AB21009D6110 AS DateTime), N'R', N'FR', CAST(1290870.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07006A11903F0000 AS Time), 1, N'GGNFGD', CAST(0x0000A70F008C1360 AS DateTime), N'', N'dnthtndgnd', N'tgbhyuioplkh', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (17, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'9900', CAST(0x0000AB71008CA000 AS DateTime), N'S', N'VS', CAST(123500.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700BE90754A0000 AS Time), 1, N'HAEHEGE', CAST(0x0000A70F008C1360 AS DateTime), N'', N'dzhtehtn', N'mznxbcvlaksj', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (18, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1092', CAST(0x0000A86C00BAC480 AS DateTime), N'S', N'DB', CAST(23900.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700A8E76F4B0000 AS Time), 0, N'REAETH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'ethnzeae', N'djfhgpqowoei', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (19, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1093', CAST(0x0000AC3100DC0050 AS DateTime), N'S', N'AE', CAST(17000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07000E4848520000 AS Time), 0, N'DHDGGH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'fzhndgzntg', N'qqaazzxxssww', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (20, 2, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1290', CAST(0x0000AE4100EA0240 AS DateTime), N'R', N'VS', CAST(22550000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700448E02580000 AS Time), 0, N'THSHGRS', CAST(0x0000A70F008C1360 AS DateTime), N'', N'atehtdteaht', N'thuyjoikjhuy', 84, 1, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (21, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'2389', CAST(0x0000AB21009D6110 AS DateTime), N'S', N'DB', CAST(32000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700A41F5D480000 AS Time), 0, N'RSTRTRH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'dgzngdzndg', N'mmnnbbvvccxx', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (22, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'3478', CAST(0x0000AB71008CA000 AS DateTime), N'S', N'OT', CAST(20000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07006850E84F0000 AS Time), 0, N'THTEHSH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'srgtfgrh', N'zzxxccvvbbnn', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (23, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'4567', CAST(0x0000A86C00BAC480 AS DateTime), N'R', N'VS', CAST(155000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700B2DB1E530000 AS Time), 0, N'HTHGGH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'zdhthtgnj', N'aassddffgghh', 14, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (24, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'5647', CAST(0x0000AC3100DC0050 AS DateTime), N'S', N'DB', CAST(320000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07005A3708570000 AS Time), 0, N'STHTHH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'yaethaeth', N'qweadsfgzvxc', 84, 2, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (25, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1028', CAST(0x0000AE4100EA0240 AS DateTime), N'R', N'MS', CAST(180000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07003411995D0000 AS Time), 1, N'TETJYFDH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'hteghnbdz', N'qtwreutiyopa', 12, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (26, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1299', CAST(0x0000AB21009D6110 AS DateTime), N'R', N'FR', CAST(1500500.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07006A11903F0000 AS Time), 1, N'THTHDH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'dngdnd', N'kdjfhgnvmxka', 84, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (27, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1300', CAST(0x0000AB71008CA000 AS DateTime), N'S', N'AE', CAST(30000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700BE90754A0000 AS Time), 1, N'TAHHETH', CAST(0x0000A70F008C1360 AS DateTime), N'', N'eayreff', N'fwqanchdgetr', 53, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (28, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1295', CAST(0x0000A86C00BAC480 AS DateTime), N'S', N'DN', CAST(18000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700A8E76F4B0000 AS Time), 0, N'DGBDG', CAST(0x0000A70F008C1360 AS DateTime), N'', N'rahrhagb', N'uthgjcmskfhr', 16, 3, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (29, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1245', CAST(0x0000AC3100DC0050 AS DateTime), N'S', N'VS', CAST(195000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x07000E4848520000 AS Time), 0, N'DSHDGB', CAST(0x0000A70F008C1360 AS DateTime), N'', N'raegerhtgh', N'shdgfnvksirh', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
INSERT [dbo].[RPT_TRANSACTION_DETAIL] ([Id], [MerchantId], [ReportDate], [FileSource], [TerminalNumber], [ExpirationDate], [TransactionCode], [CardTypeCode], [TransactionAmount], [TransactionDate], [TransactionTime], [KeyedEntry], [AuthorizationNumber], [ReportTime], [Description], [AccountNumber], [FirstTwelveAccountNumber], [CountryId], [PayTypeId], [CreatedAt], [CreatedUser], [UpdatedAt], [UpdatedUser], [StatusId]) VALUES (30, 3, CAST(0x0000A70F008C1360 AS DateTime), N'TransactionDetail', N'1221', CAST(0x0000AE4100EA0240 AS DateTime), N'R', N'MS', CAST(2350000.00 AS Decimal(18, 2)), CAST(0x0000A70E00000000 AS DateTime), CAST(0x0700448E02580000 AS Time), 0, N'THSGT', CAST(0x0000A70F008C1360 AS DateTime), N'', N'rhrfbrrg', N'qpwoeirutylk', 0, 4, CAST(0x0000000000000000 AS DateTime), 1, CAST(0x0000A70F00AD08E0 AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[RPT_TRANSACTION_DETAIL] OFF
ALTER TABLE [dbo].[INF_MESSAGE] ADD  CONSTRAINT [DF_INF_MESSAGE_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_DATASUMARY] ADD  CONSTRAINT [DF_RPT_MERCHANT_DATASUMARY_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[RPT_MERCHANT_SUMARY] ADD  CONSTRAINT [DF_RPT_MERCHANT_SUMARY_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_RPT_TRANSACTION_DETAIL_CreatedAt]  DEFAULT ((0)) FOR [CreatedAt]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_RPT_TRANSACTION_DETAIL_UpdatedUser]  DEFAULT ((0)) FOR [UpdatedUser]
GO
ALTER TABLE [dbo].[RPT_TRANSACTION_DETAIL] ADD  CONSTRAINT [DF_RPT_TRANSACTION_DETAIL_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] ADD  CONSTRAINT [DF_SYS_ACCOUNT_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_SESSION] ADD  CONSTRAINT [DF_SYS_ACCOUNT_SESSION_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_TYPE] ADD  CONSTRAINT [DF_SYS_ACCOUNT_TYPE_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[SYS_LOG] ADD  CONSTRAINT [DF_SYS_LOG_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[TBL_AGENT] ADD  CONSTRAINT [DF_TBL_AGENT_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[TBL_ATTRIBUTE] ADD  CONSTRAINT [DF_TBL_ATTRIBUTE_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[TBL_MERCHANT] ADD  CONSTRAINT [DF_TBL_MERCHANT_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[TBL_MERCHANT_TYPE] ADD  CONSTRAINT [DF_TBL_MERCHANT_TYPE_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[TBL_REGION] ADD  CONSTRAINT [DF_TBL_REGION_StatusId]  DEFAULT ((1)) FOR [StatusId]
GO
ALTER TABLE [dbo].[INF_MESSAGE]  WITH CHECK ADD  CONSTRAINT [FK_dbo.INF_MESSAGE_dbo.SYS_ACCOUNT_FromAccountId] FOREIGN KEY([FromAccountId])
REFERENCES [dbo].[SYS_ACCOUNT] ([Id])
GO
ALTER TABLE [dbo].[INF_MESSAGE] CHECK CONSTRAINT [FK_dbo.INF_MESSAGE_dbo.SYS_ACCOUNT_FromAccountId]
GO
ALTER TABLE [dbo].[INF_MESSAGE]  WITH CHECK ADD  CONSTRAINT [FK_dbo.INF_MESSAGE_dbo.SYS_ACCOUNT_ToAccountId] FOREIGN KEY([ToAccountId])
REFERENCES [dbo].[SYS_ACCOUNT] ([Id])
GO
ALTER TABLE [dbo].[INF_MESSAGE] CHECK CONSTRAINT [FK_dbo.INF_MESSAGE_dbo.SYS_ACCOUNT_ToAccountId]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SYS_ACCOUNT_dbo.SYS_ACCOUNT_TYPE_AccountTypeId] FOREIGN KEY([AccountTypeId])
REFERENCES [dbo].[SYS_ACCOUNT_TYPE] ([Id])
GO
ALTER TABLE [dbo].[SYS_ACCOUNT] CHECK CONSTRAINT [FK_dbo.SYS_ACCOUNT_dbo.SYS_ACCOUNT_TYPE_AccountTypeId]
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_SESSION]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SYS_ACCOUNT_SESSION_dbo.SYS_ACCOUNT_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[SYS_ACCOUNT] ([Id])
GO
ALTER TABLE [dbo].[SYS_ACCOUNT_SESSION] CHECK CONSTRAINT [FK_dbo.SYS_ACCOUNT_SESSION_dbo.SYS_ACCOUNT_AccountId]
GO
ALTER TABLE [dbo].[TBL_MERCHANT]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TBL_MERCHANT_dbo.TBL_AGENT_AgentId] FOREIGN KEY([AgentId])
REFERENCES [dbo].[TBL_AGENT] ([Id])
GO
ALTER TABLE [dbo].[TBL_MERCHANT] CHECK CONSTRAINT [FK_dbo.TBL_MERCHANT_dbo.TBL_AGENT_AgentId]
GO
ALTER TABLE [dbo].[TBL_MERCHANT]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TBL_MERCHANT_dbo.TBL_MERCHANT_TYPE_MerchantTypeId] FOREIGN KEY([MerchantTypeId])
REFERENCES [dbo].[TBL_MERCHANT_TYPE] ([Id])
GO
ALTER TABLE [dbo].[TBL_MERCHANT] CHECK CONSTRAINT [FK_dbo.TBL_MERCHANT_dbo.TBL_MERCHANT_TYPE_MerchantTypeId]
GO
ALTER TABLE [dbo].[TBL_MERCHANT]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TBL_MERCHANT_dbo.TBL_REGION_RegionId] FOREIGN KEY([RegionId])
REFERENCES [dbo].[TBL_REGION] ([Id])
GO
ALTER TABLE [dbo].[TBL_MERCHANT] CHECK CONSTRAINT [FK_dbo.TBL_MERCHANT_dbo.TBL_REGION_RegionId]
GO
USE [master]
GO
ALTER DATABASE [CardProcessing] SET  READ_WRITE 
GO
