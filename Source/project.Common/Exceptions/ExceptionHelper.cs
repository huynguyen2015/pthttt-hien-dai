﻿using System;
using project.Common.Resources;
using ResourceManager = System.Resources.ResourceManager;

namespace project.Common.Exceptions
{
    public class ExceptionHelper : IExceptionHelper
    {
        private readonly IResourceManager _manager;

        public ExceptionHelper(IResourceManager manager)
        {
            _manager = manager;
        }

        public ExceptionMessage ExceptionMessage(ResourceManager resourceManager, string name)
        {
            return new ExceptionMessage { ErrorCode = Convert.ToInt32(name), ErrorMessage = _manager.GetResouce(resourceManager, name) };
        }

        public ExceptionMessage ResourceToExceptionMessage(ResourceManager resourceManager, string name, string prefix = "Msg")
        {
            return new ExceptionMessage { ErrorCode = Convert.ToInt32(name), ErrorMessage = _manager.GetResouce(resourceManager, prefix + name) };
        }

        public ExceptionMessage ExceptionMessageCustomize(int errorCode, string errorMessage)
        {
            return new ExceptionMessage { ErrorCode = errorCode, ErrorMessage = errorMessage };
        }
    }
}