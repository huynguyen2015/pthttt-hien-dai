using System;

namespace project.Data.Entities
{

    public class RptTransactionDetail : BaseEntity
    {
        public virtual long Id { get; set; }
        public long MerchantId { get; set; }
        public DateTime ReportDate { get; set; }
        public string FileSource { get; set; }
        public long BatchNumber { get; set; }
        public int TerminalNumber { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string TransactionCode { get; set; }
        public string CardTypeCode { get; set; }        
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public TimeSpan TransactionTime { get; set; }
        public bool KeyedEntry { get; set; }
        public string AuthorizationNumber { get; set; }
        public DateTime ReportTime { get; set; }
        public string Description { get; set; }
        public string AccountNumber { get; set; }
        public string FirstTwelveAccountNumber { get; set; }
        public int CountryId { get; set; }

        public TblMerchant Merchant { get; set; }
    }
}
