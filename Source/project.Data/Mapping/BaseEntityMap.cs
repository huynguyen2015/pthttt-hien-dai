using System.Data.Entity.ModelConfiguration;
using project.Data.Entities;

namespace project.Data.Mapping
{
    public abstract class BaseEntityMap<T> : EntityTypeConfiguration<T> where T : BaseEntity
    {
        protected BaseEntityMap()
        {
            PostInitialize();
        }

        /// <summary>
        /// Developers can override this method in custom partial classes
        /// in order to add some custom initialization code to constructors
        /// </summary>
        protected virtual void PostInitialize()
        {
            Property(u => u.CreatedAt).IsRequired();
            Property(u => u.UpdatedAt).IsRequired();
            Property(u => u.CreatedUser).IsRequired();
            Property(u => u.UpdatedUser).IsRequired();
            Property(u => u.StatusId);
        }
    }
}