﻿using System;

namespace project.Common.Security
{
    public interface IUserSession
    {
        long? MemberId { get; }
        string Username { get; }
        string Email { get; }
        long SessionTokenId { get; }
        bool IsInRole(string roleName);
        int LanguageId { get; }
    }
}
