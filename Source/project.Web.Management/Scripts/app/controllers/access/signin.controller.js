﻿/**
 * @ngdoc overview
 * @name Login controller
 * @description Login controller, control the user's behavie on login screen and call service to process for each behavie
 *
 */
(function() {
    function signInController($scope, $location, $window, $state, httpService, apiRoutes, appRoutes, appMessages, alertify) {
        // Declare variable
        $scope.user = {
           
        };

        // Private function
        function doSignIn() {
            var params = { Email: $scope.user.email, Password: $scope.user.password };            
            httpService.sendPost(apiRoutes.memberLogin, params, $scope.setContentLoading).then(function (response) {                
                if (response && response.Success === true) {
                    $window.localStorage.setItem("token", response.Data.SessionToken);
                    $window.localStorage.setItem("accountInfo", JSON.stringify(response.Data));
                    $scope.setAccountInfo();
                    $state.go('app.dashboard');
                } 
            });
        }

        $scope.signIn = function () {
            
            if (isEmpty($scope.user.email) || isEmpty($scope.user.password)) {
                alertify.logPosition("top right").error("Bạn chưa nhập thông tin đăng nhập");                  
                return;
            }
            if ($scope.user.password.length < 6 || $scope.user.password.length > 64) {
                alertify.logPosition("top right").error("Mật khậu phải từ 6-64 ký tự");                
                return;
            }

            doSignIn();
        };        
    }
   
    angular.module('app').controller('SignInController', ['$scope', '$location', '$window', '$state', 'HttpService', 'API_ROUTES', 'APP_ROUTES', 'APP_MESSAGES', 'alertify', signInController]);
})();