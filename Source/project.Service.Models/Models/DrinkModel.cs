namespace Click.Data.Entities
{
    using System;

    public class DrinkModel
    {
        public short DrinkId { get; set; }

        public string Name { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        public short AboutMeCategoryId { get; set; }

        
    }
}