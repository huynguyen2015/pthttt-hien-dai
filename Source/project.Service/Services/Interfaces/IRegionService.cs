﻿using project.Data.PagedDataRequest;
using project.Service.Models;
using project.Service.Models.Region;

namespace project.Service.Services.Interfaces
{
    public interface IRegionService
    {
        RegionModel GetById(int memberId);
        bool Insert(RegionModel model);
        bool Update(RegionModel model);
        bool Delete(RegionModel model);
        PagedDataInquiryResponse<RegionModel> FilterRegion(RegionDataRequest dataRequest);
    }
}