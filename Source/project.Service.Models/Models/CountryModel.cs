namespace Click.Data.Entities
{
    using System;

    public  class CountryModel
    {

        public short CountryId { get; set; }

        public string CountryName { get; set; }

        public short CurrencyId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }

        

    }
}
