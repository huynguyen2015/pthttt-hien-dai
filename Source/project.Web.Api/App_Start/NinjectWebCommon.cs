﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Http;
using project.Service.DIConfiguration;
using project.Web.Api;
using project.Web.Api.Controllers.v1.DependencyBlocks;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common.DependencyResolvers;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using project.Web.Api.Security;
using project.Web.Api.Commons.Helper;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(NinjectWebCommon), "Stop")]

namespace project.Web.Api
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            IKernel containerBuilder = null;
            Bootstrapper.Initialize(() =>
            {
                containerBuilder = CreateKernel();
                return containerBuilder;
            });

            var resolver = new NinjectDependencyResolver(containerBuilder);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }

        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel containerBuilder)
        {
            var containerConfigurator = new NinjectConfigurator();
            containerConfigurator.Configure(containerBuilder);
            RegisterSecurityServices(containerBuilder);
            ConfigureControllerDependencyBlock(containerBuilder);
        }

        private static void RegisterSecurityServices(IKernel containerBuilder)
        {
            var appConfig = ConfigurationManager.AppSettings;
            containerBuilder.Bind<IBasicSecurityService>().To<BasicSecurityService>().InSingletonScope();
        }

        private static void ConfigureControllerDependencyBlock(IKernel containerBuilder)
        {            

            containerBuilder.Bind<IParseRequestParams>().To<ParseRequestParams>();
            containerBuilder.Bind<IAccountControllerDependencyBlock>().To<AccountControllerDependencyBlock>();
            containerBuilder.Bind<IMerchantControllerDependencyBlock>().To<MerchantControllerDependencyBlock>();
            containerBuilder.Bind<IRegionControllerDependencyBlock>().To<RegionControllerDependencyBlock>();
            containerBuilder.Bind<IMerchantTypeControllerDependencyBlock>().To<MerchantTypeControllerDependencyBlock>();
            containerBuilder.Bind<IAgentControllerDependencyBlock>().To<AgentControllerDependencyBlock>();
            containerBuilder.Bind<IMessageControllerDependencyBlock>().To<MessageControllerDependencyBlock>();
            BindFilter(containerBuilder);
           
        }

        private static void BindFilter(IBindingRoot containerBuilder)
        {           
            //containerBuilder.BindHttpFilter(x => new ETagAttribute(x.Inject<IUserSession>(), x.Inject<IETagQueryProcessor>()), FilterScope.Action).WhenActionMethodHas<ETagFilter>();
        }        
    }
}