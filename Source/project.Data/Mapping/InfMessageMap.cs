﻿using project.Data.Entities;

namespace project.Data.Mapping
{
    public class InfMessageMap : BaseEntityMap<InfMessage>
    {
        public InfMessageMap() {
            ToTable("INF_MESSAGE");
            HasKey(o => o.Id);
            Property(o => o.Subject);
            Property(o => o.Content);
            Property(o => o.IsRead);            
            Property(o => o.FromAccountId);
            Property(o => o.ToAccountId);
            Property(o => o.ParentId);

            HasRequired(o => o.FromAccount).WithMany().WillCascadeOnDelete(false);
            HasRequired(o => o.ToAccount).WithMany().WillCascadeOnDelete(false);
        }
    }
}