﻿using project.Common;
using project.Common.Exceptions;
using project.Common.TypeMapping;
using project.Data;
using project.Data.Entities;
using project.Data.PagedDataRequest;
using project.Repository.Interfaces;
using project.Service.Models;
using project.Service.Models.Agent;
using project.Service.Services.Interfaces;
using System;

namespace project.Service.Services
{
    public class AgentService : BaseService, IAgentService
    {
        private IAgentRepository _agentRepository;
        public AgentService(IAutoMapper autoMapper, IDateTime dateTime, IExceptionHelper exceptionHelper,
            IAgentRepository agentRepository,
            TravelContext context)
            : base(context, autoMapper, dateTime, exceptionHelper)
        {
            _agentRepository = agentRepository;
        }

        public AgentModel GetById(int id)
        {
            var value = _agentRepository.GetById(id);
            return new AgentModel
            {
                CreatedAt = value.CreatedAt,
                UpdatedAt = value.UpdatedAt,
            };
        }

        public AgentModel Insert(AgentModel value)
        {
            var entity = AutoMapper.Map<TblAgent>(value);
            entity.StatusId = Constants.StatusIds.Active;
            _agentRepository.Insert(entity);
            _context.SaveChanges();

            var result = AutoMapper.Map<AgentModel>(entity);
            return result;            
        }

        public bool Update(AgentModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.AgentName))
                    throw WrapException<BusinessException>("Tên đại lý không được để trống");
                if (string.IsNullOrEmpty(model.AgentCode))
                    throw WrapException<BusinessException>("Mã đại lý không được để trống");

                var entity = _agentRepository.GetById(model.Id);
                if (entity == null)
                    throw WrapException<BusinessException>("Đại lý không còn tồn tại");
                entity.AgentName = model.AgentName;
                entity.AgentCode = model.AgentCode;
                _agentRepository.Update(entity);
                _context.SaveChanges();                
                return true;
            }
            catch
            {
                //TODO: change error code later
                throw WrapException<BusinessException>(204);
            }
        }

        public bool Delete(long agentId)
        {
            try
            {
                var entity = _agentRepository.GetById(agentId);
                if (entity == null)
                    throw WrapException<BusinessException>("Đại lý không còn tồn tại");

                _agentRepository.Delete(entity);
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                throw WrapException<BusinessException>(204);
            }
        }

        public bool UpdateStatus(AgentModel model)
        {
            try
            {
                var entity = _context.Set<TblAgent>().Find(model.Id);
                if (entity != null)
                {
                    entity.StatusId = model.StatusId;
                    entity.UpdatedAt = System.DateTime.Now;
                    _context.Entry(entity).CurrentValues.SetValues(entity);
                    _context.SaveChanges();

                    return true;
                }

                return false;
            }
            catch
            {
                //TODO: change error code later
                throw WrapException<BusinessException>(204);
            }
        }

        public PagedDataInquiryResponse<AgentModel> FilterAgent(AgentDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _agentRepository.FilterAgent(dataRequest);
            return WrapPagedResponse<AgentModel, TblAgent>(queryResult, dataRequest);
        }

        public PagedDataInquiryResponse<AgentModel> FilterAgent_NoParams(AgentDataRequest dataRequest)
        {
            if (dataRequest == null)
                throw WrapException<BusinessException>(3304);

            var queryResult = _agentRepository.FilterAgent_NoParams(dataRequest);
            return WrapPagedResponse<AgentModel, TblAgent>(queryResult, dataRequest);
        }
    }
}