﻿using project.Common.Exceptions;
using project.Common.Security;
using project.Common.Serialization;
using project.Service.Services.Interfaces;
using project.Web.Api.Commons.Helper;
using project.Web.Api.Controllers.v1.Interfaces;
using project.Web.Common;
using project.Web.Common.ActionHelper;

namespace project.Web.Api.Controllers.v1.DependencyBlocks
{
    public class MerchantTypeControllerDependencyBlock : ControllerDependencyBlockBase, IMerchantTypeControllerDependencyBlock
    {
        public MerchantTypeControllerDependencyBlock(
            IMerchantTypeService merchantTypeService,
            IParseRequestParams parseRequestParams,
            IHttpHelper httpHelper,
            IActionResultHelper actionResultHelper,
            ISerialization serialization, 
            IUserSession userSession,
            IExceptionHelper exceptionHelper)
        {
            MerchantTypeService = merchantTypeService;
            HttpHelper = httpHelper;
            ParseRequestParams = parseRequestParams;
            ActionResultHelper = actionResultHelper;
            Serialization = serialization;
            UserSession = userSession;
            ExceptionHelper = exceptionHelper;
        }

        public IMerchantTypeService MerchantTypeService { get; private set; }
        public ISerialization Serialization { get; private set; }
        public IParseRequestParams ParseRequestParams { get; private set; }
    }
}