﻿using System;

namespace project.Web.MvcManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {        
        private const string AdminPage = "/index.html";        
        protected void Application_BeginRequest (Object sender, EventArgs e)
        {                       
            string url = Request.Url.LocalPath;   
            if (!System.IO.File.Exists(Context.Server.MapPath(url)))
                Context.RewritePath(AdminPage);
        }        
    }
}
