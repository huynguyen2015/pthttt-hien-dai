﻿
namespace project.Service.Models.Agent
{
    public class AgentModel : BaseModel
    {
        public string AgentCode { get; set; }
        public string AgentName { get; set; } 
    }
}