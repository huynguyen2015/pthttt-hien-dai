namespace project.Data.Migrations
{
    using project.Data.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<project.Data.TravelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(project.Data.TravelContext context)
        {
            var account_type = new List<SysAccountType>()
            {
                new SysAccountType() {TypeName = "Merchant", Role = 1, StatusId = 1},
                new SysAccountType() {TypeName = "Agent", Role = 2, StatusId = 1},
                new SysAccountType() {TypeName = "Admin", Role = 3, StatusId = 1}
            };
            account_type.ForEach(s => context.SysAccountTypeSet.AddOrUpdate(p => p.TypeName, s));
            context.SaveChanges();

            //var account = new List<SysAccount>() { 
            //    new SysAccount() {Username = "admin", Email = "admin@gmail.com", 
            //        Password = "1234", AccountTypeId = 1, StatusId = 1},
            //    new SysAccount() {Username = "agent", Email = "agent@gmail.com", 
            //        Password = "1234", AccountTypeId = 2, StatusId = 1},
            //    new SysAccount() {Username = "merchant", Email = "merchant@gmail.com", 
            //        Password = "1234", AccountTypeId = 3, StatusId = 1}
            //};
            //account.ForEach(s => context.SysAccountSet.AddOrUpdate(p => p.Username, s));
            //context.SaveChanges();
        }
    }
}
