﻿/**
 * @ngdoc function
 * @name app.config:uiRouter
 * @description
 * # Config
 * Config for the router
 */
(function() {
    'use strict';    
    angular.module('app').factory('authInterceptor', function ($rootScope, $q, $window, $location, APP_ROUTES) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                
                if ($window.localStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
                    //console.log(config.headers.Authorization);
                }
                config.headers.DeviceTypeId = DeviceTypeIds.Web;
                return config;
            },
            response: function (response) {
                if (response.status === 401) {
                    // handle the case where the user is not authenticated
                    delete $window.localStorage.token;
                    $location.url(APP_ROUTES.signin);
                }
                return response || $q.when(response);
            }
        };
    });
    angular
        .module('app')
        .run(runBlock)
        .config(config);

        runBlock.$inject = ['$rootScope', '$state', '$stateParams', '$window'];
        function runBlock($rootScope, $state, $stateParams, $window) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.$on("$stateChangeStart", function (event, toState) {                                
                if (toState.isAuthenticationRequired
                    && ($window.localStorage.token == undefined || $window.localStorage.token == null || $window.localStorage.token === "")) {
                    // User isn’t authenticated                  
                    event.preventDefault();
                    $state.go("access.signin");
                }
            });
      }

      config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'MODULE_CONFIG'];
      function config($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, MODULE_CONFIG) {
        
        var layout = '../../views/layout.html',
            dashboard = '../views/dashboard/dashboard.html';
        $locationProvider.html5Mode(true).hashPrefix('!');
        $urlRouterProvider
          //.otherwise('/app/member');
          .otherwise('/access/signin');
        $stateProvider
          .state('app', {
              abstract: true,
              isAuthenticationRequired: true,
            url: '/app',
            views: {
              '': {
                templateUrl: layout
              }
            }
          })
            .state('app.dashboard', {
              url: '/dashboard',
              templateUrl: dashboard,
              isAuthenticationRequired: true,
              data: { title: 'Dashboard' }
            })
                      
            // Member router
            .state('app.member', {
              url: '/member',
              templateUrl: '/views/member/member/index.html',
              isAuthenticationRequired: true,
              controller: 'MemberSearchController',
              resolve: load(['MemberSearchController', 'Scripts/app/controllers/member/membersearch.controller.js', 'Scripts/app/services/http/http.service.js', 
                          'Scripts/app/controllers/member/accountModification.controller.js']),
              data: { title: 'Member Information' }
            })
             
            .state('app.role', {
              url: '/role',
              templateUrl: '/views/member/role/search.html',
              isAuthenticationRequired: true,
              controller: 'RoleSearchController',
              resolve: load(['RoleSearchController', 'Scripts/app/controllers/role/rolesearch.controller.js'
                  , 'Scripts/app/services/http/http.service.js'
                  , 'Scripts/app/controllers/role/roleaddpopup.controller.js']),
              data: { title: 'Role' }
            })
            .state('app.permission', {
                url: '/permission',
                templateUrl: '/views/member/permission/search.html',
                isAuthenticationRequired: true,
                controller: 'PermissionSearchController',
                resolve: load(['PermissionSearchController', 'Scripts/app/controllers/permission/permissionsearch.controller.js'
                    , 'Scripts/app/services/http/http.service.js'
                    , 'Scripts/app/controllers/permission/permissionaddpopup.controller.js']),
                data: { title: 'Permission' }
            })

            .state('app.managementagent', {
                url: '/managementagent',
                templateUrl: '/views/managementagent/index.html',
                isAuthenticationRequired: true,                
                data: { title: 'Quản lý đại lý' }
            })
            .state('app.managementagent.search', {
                url: '/search',
                templateUrl: '/views/managementagent/search.html',
                isAuthenticationRequired: true,
                controller: 'ManagementAgentController',
                resolve: load(['ManagementAgentController']),
                data: { title: 'Quản lý đại lý' }
            })

            //Place routers
            .state('app.place', {
                url: '/merchant',
                templateUrl: '/views/place/index.html',
                isAuthenticationRequired: true,
                data: { title: 'Place Search' }
            })
            .state('app.place.category', {
                url: '/category',
                templateUrl: '/views/place/place.html',
                isAuthenticationRequired: true,
                controller: 'PlaceSearchController',
                resolve: load(['PlaceSearchController', 'Scripts/app/controllers/place/placesearch.controller.js', 'Scripts/app/services/http/http.service.js',
                    'Scripts/app/controllers/place/placeModification.controller.js']),
                data: { title: 'Danh mục' }
            })
            .state('app.place.detail', {
                url: '/detail',
                templateUrl: '/views/place/placedetail.html',
                isAuthenticationRequired: true,
                controller: 'PlaceDetailSearchController',
                resolve: load(['PlaceDetailSearchController', 'Scripts/app/controllers/place/placedetailsearch.controller.js', 'Scripts/app/services/http/http.service.js',
                    'Scripts/app/controllers/place/placeDetailModification.controller.js', 'Scripts/libs/angular/angular-map/ng-map.min.js'              
                 //   'Scripts/libs/angular/angular-google-maps/angular-google-maps.min.js',
                 //   'Scripts/libs/angular/angular-google-maps/angular-simple-logger.js',
                 //'Scripts/libs/angular/angular-google-maps/lodash.js'
                 
                 ]),
                data: { title: 'Chi tiết danh mục' }
            })
            .state('app.place.image', {
                url: '/image',
                templateUrl: '/views/place/placeimage.html',
                isAuthenticationRequired: true,
                controller: 'PlaceImageSearchController',
                resolve: load(['PlaceImageSearchController', 'Scripts/app/controllers/place/placeimagesearch.controller.js', 'Scripts/app/services/http/http.service.js',
                    'Scripts/app/controllers/place/placeImageModification.controller.js']),
                data: { title: 'Hình ảnh' }
            })

            //Account routers
            .state('app.account', {
                url: '/account',
                templateUrl: '/views/account/index.html',
                isAuthenticationRequired: true,
                data: { title: 'Account Search' }
            })
            .state('app.account.profile', {
                url: '/profile',
                templateUrl: '/views/account/account.html',
                isAuthenticationRequired: true,
                controller: 'AccountProfileSearchController',
                resolve: load(['AccountProfileSearchController', 'Scripts/app/controllers/account/accountprofilesearch.controller.js', 'Scripts/app/services/http/http.service.js',
                    ]),
                data: { title: 'Tài khoản' }
            })

            .state ('app.tour', {
              url:'/tour',
              templateUrl:'/Views/tour/index.html',
              isAuthenticationRequired: true,
              controller:'TourController',
              resolve: load(['TourController', 'Scripts/app/controllers/tour/tour.controller.js', 'Scripts/app/services/http/http.service.js',
                'Scripts/app/controllers/tour/modal.controller.js']),
              data: {title: 'Quản Lý Chuyến Đi'}
            })
            .state('app.plan.tour', {
              url:'/plan',
              templateUrl: '/Views/tour/plan.html',
              isAuthenticationRequired: true,
              controller: 'PlanTourController',
              resolve: load(['PlanTourController', 'Scripts/app/controllers/tour/plan.tour.controller.js',
                'Scripts/app/services/http/http.service.js']),
              data: {title: 'Quản Lý Lịch Chuyến Đi'}
            })
            .state('app.vehicle', {
                url: '/vehicle',
                templateUrl: '/views/vehicle/index.html',
                isAuthenticationRequired: true,
                data: { title: 'Phương tiện' }
            })
            // Vehicle router 
            .state('app.vehicle.details', {
                url: '/details',
                templateUrl: '/views/vehicle/details/index.html',
                isAuthenticationRequired: true,
                controller: 'VehicleDetailsController',
                resolve: load(['VehicleDetailsController',
                    'Scripts/app/controllers/vehicle/details/vehiclesearch.controller.js',
                    'Scripts/app/services/http/http.service.js',
                    'Scripts/app/controllers/vehicle/details/vehicleModification.controller.js']),
                data: { title: 'Các Phương Tiện' }
            })
            .state('app.vehicle.category', {
                url: '/category',
                templateUrl: '/views/vehicle/category/index.html',
                isAuthenticationRequired: true,
                controller: 'VehicleCategoryController',
                resolve: load(['VehicleCategoryController',
                    'Scripts/app/controllers/vehicle/category/categorysearch.controller.js',
                    'Scripts/app/services/http/http.service.js',
                    'Scripts/app/controllers/vehicle/category/categoryModification.controller.js']),
                data: { title: 'Loại phương tiện' }
            })
            .state('app.reports', {
                url: '/reports',
                templateUrl: '/views/report/month.html',
                isAuthenticationRequired: true,
                controller: 'ReportMonthController',
                resolve: load(['ReportMonthController', 'Scripts/app/controllers/report/reportmonth.controller.js' ]),
                data: { title: 'Báo cáo' }
            })
            .state('app.reportsquarter', {
                url: '/reportsquarter',
                templateUrl: '/views/report/quarter.html',
                isAuthenticationRequired: true,
                controller: 'ReportQuarterController',
                resolve: load(['ReportQuarterController', 'Scripts/app/controllers/report/reportquarter.controller.js']),
                data: { title: 'Báo cáo' }
            })
            .state('app.reportsyear', {
                url: '/reportsyear',
                templateUrl: '/views/report/year.html',
                isAuthenticationRequired: true,
                controller: 'ReportYearController',
                resolve: load(['ReportYearController', 'Scripts/app/controllers/report/reportyear.controller.js']),
                data: { title: 'Báo cáo' }
            })
            // Managements routers
            .state('app.management', {
                url: '/management',
                templateUrl: '../../views/management/system.html',
                data: { title: 'Management' }
            })
            .state('app.management.clicks', {
                url: '/clicks',
                templateUrl: '../../views/management/click.html', 
                data: { title: 'Management' }
            })

          // Access routers
           .state('404', {
               url: '/404',
               templateUrl: '../../views/misc/404.html'
           })
            .state('505', {
                url: '/505',
                templateUrl: '../../views/misc/505.html'
            })
            .state('access', {
                url: '/access',
                template: '<div class="dark bg-auto w-full"><div ui-view class="fade-in-right-big smooth pos-rlt"></div></div>'
            })
            .state('access.signin', {
                url: '/signin',
                templateUrl: '../../views/misc/signin.html',
                controller: 'SignInController',
                resolve: load(['SignInController', 'Scripts/app/services/access/access.service.js', 'Scripts/app/controllers/access/signin.controller.js'])
            })
            .state('access.signup', {
                url: '/signup',
                templateUrl: '../../views/misc/signup.html'
            })
            .state('access.forgot-password', {
                url: '/forgot-password',
                templateUrl: '../../views/misc/forgot-password.html'
            })
            .state('access.lockme', {
                url: '/lockme',
                templateUrl: '../../views/misc/lockme.html'
            })

           // cái này nó phân cấp theo dấu chấm. nếu ko có app.mailbox nó ko tìm ra đc đâu
           //.state('app.mailboxlist', {
           //    url: '/mailbox',
           //    templateUrl: '/views/mailbox/main.html',
           //    isAuthenticationRequired: true,
           //    controller: 'MailBoxController',
           //    resolve: load(['MailBoxController', 'Scripts/app/controllers/mailbox/mailbox.controller.js', 'Scripts/app/services/http/http.service.js']),
           //    data: { title: 'Hộp thư' }
           //})

          .state('app.inbox', {
              url: '/inbox',
              templateUrl: 'views/mailbox/main.html',
              data: { title: 'Hộp thư' },
              controller: 'MainCtrl',
              resolve: load(['/views/mailbox/inbox.js', 'Scripts/libs/js/moment/moment.js'])
          })
            .state('app.inbox.list', {
                url: '/inbox/{fold}',
                templateUrl: 'views/mailbox/list.html',
                controller: 'ListCtrl',
            })
            .state('app.inbox.item', {
                url: '/{id:[0-9]{1,4}}',
                templateUrl: 'views/mailbox/item.html',
                controller: 'DetailCtrl'
            })
            .state('app.inbox.compose', {
                url: '/compose',
                templateUrl: 'views/mailbox/new.html',
                controller: 'NewCtrl',
                resolve: load(['Scripts/libs/jquery/summernote/dist/summernote.js', 'Scripts/libs/angular/angular-ui-select/dist/select.min.js', 'Scripts/libs/angular/angular-ui-select/dist/select.min.css'])
            })
          ;

        $httpProvider.interceptors.push('authInterceptor');

        // Private function
        function load(srcs, callback) {
          return {
              deps: ['$ocLazyLoad', '$q',
                function( $ocLazyLoad, $q ){
                  var deferred = $q.defer();
                  var promise  = false;
                  srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                  if(!promise){
                    promise = deferred.promise;
                  }
                  angular.forEach(srcs, function(src) {
                    promise = promise.then( function(){
                      angular.forEach(MODULE_CONFIG, function(module) {
                        if( module.name === src){
                          src = module.module ? module.name : module.files;
                        }
                      });
                      return $ocLazyLoad.load(src);
                    } );
                  });
                  deferred.resolve();
                  return callback ? promise.then(function(){ return callback(); }) : promise;
              }]
          }
        }

        function getParams(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
      }      
})();
