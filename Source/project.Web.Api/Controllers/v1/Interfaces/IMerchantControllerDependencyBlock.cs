﻿using project.Service.Services.Interfaces;
using project.Web.Api.Commons.Helper;

namespace project.Web.Api.Controllers.v1.Interfaces
{
    public interface IMerchantControllerDependencyBlock : IControllerDependencyBlock
    {
        IMerchantService MerchantService { get; }
        IParseRequestParams ParseRequestParams { get; }
    }
}